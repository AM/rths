%% Init_LinearSystem.m
% called from Run_LinearSystem.m and LinearSystem.slx

%% Parameters to change %%
% Dynamical system properties
mNUM = 9.62; %mass of numerical substructure, kg
dNUM = 200; %damping in numerical substructure, Ns/m 
kNUM = 20000;%stiffness of the numerical substructure, N/m

mEXP = 0; %mass of experimental substructure, kg
dEXP = 0;  %damping in experimental substructure, Ns/m
kEXP = 8650; %stiffness of the experimental substructure, N/m

tEnd = 20; %simulation duration, s

%% Please do not change %%
g = 9.81; %gravitational constant, m/s^2

%Actuator transfer function
GACT_Num = 1.053149793891977e+03; %numerator                         
GACT_Den = [1 55.462239946523620 0]; %denumerator
 
%parameters for inner velocity control loop (PI controller)
K_P_v  = 0.2; 	
K_I_v  = 5;

% analytic eigenfrequency of the reference system in Hz
delta = (dNUM+dEXP)/(2*(mNUM+mEXP));
f_analytical_Hz = 1/(2*pi)*sqrt((kNUM+kEXP)/(mNUM+mEXP) - delta^2);

