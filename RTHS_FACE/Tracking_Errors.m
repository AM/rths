function [TrackingErrors] = Tracking_Errors(zc,zm,Fs)
%Tracking_Errors calculates different error indicators based on the
%tracking (gap) of the actuator
% input:
%   * zc: commanded displacement
%   * zm: achieved displacement
% output:
%   * E_track_rms
%   * FEI Indizes (amplitude, delay); 

gap = zc-zm;

%% relative RMS tracking error - time
for i=1:length(zc)
    E_track_rms(i) = rms(gap(1:i))./rms(zc(1:i));
end

%% FEI Indices; d_FEI: mean delay of the actuator over the whole test and A the amplitude ratio between zm and zc
% * according to: Guo, T., Chen, C., Xu, W., and Sanchez, F. "A frequency response analysis approach for quantitative assessment
%   of actuator tracking for real-time hybrid simulation". In: Smart Materials and Structures 23.4 (2014). DOI:10.1088/0964-1726/23/4/045042
% * here: adapted version as proposed in the PhD Thesis: Insam, C. "Fundamental Methods for Real-Time Hybrid Substructuring with Contact; 
%   Enabling Testing of Prosthetic Feet", Technical University of Munich, 2022 
if size(zc,2)>size(zc,1)
    zc = zc.'; 
    zm = zm.';
end
l = 2;

%input signal
    input_sig_fft=fft(zc); %korrekt wäre /N
    input_sig_fft=input_sig_fft(1:round(end/2),:); %korrekt wäre input_sig_fft(2:end)=2*input_sig_fft(2:end)
    input_sig_fft_weighted=abs(input_sig_fft).^l; % Amplitudengewichtung

%output signal
    output_sig_fft=fft(zm); %korrekt wäre /N
    output_sig_fft=output_sig_fft(1:round(end/2),:); %korrekt wäre zusätzlich die Berechnung von: output_sig_fft(2:end)=2*output_sig_fft(2:end)
    output_sig_fft_weighted = output_sig_fft.^2;
%Frequenzen
    freq=linspace(0,Fs/2,length(input_sig_fft))'; % frequency vector
  
% % FEI: original version of implementation from Guo et al. 
%     FEI=sum( (output_sig_fft./input_sig_fft).* input_sig_fft_weighted )./sum(input_sig_fft_weighted); %hat so viele Einträge wie Freiheitsgrade; Summe ist jeweils über alle k=1:N/2 von einem Freiheitsgrad
%     A_FEI=abs(FEI);
%         phi=atan(imag(FEI)./real(FEI));
%         feq=sum(input_sig_fft_weighted.*(freq*ones(1,size(zc,2))))./sum(input_sig_fft_weighted); 
%     d_FEI=-phi./(2*pi*feq);
    
%% FEI: adapted version of implementation: just interesting frequency spectrum (PhD thesis Insam)
    start_search = find(freq>0.2,1,'first');
    freq_index = find(max(output_sig_fft_weighted(start_search:end))==output_sig_fft_weighted);
    delta_freq_index = freq_index - start_search;
    l_lim = start_search;
    u_lim = freq_index + delta_freq_index;
    
    %FEI limited
    FEI_limited = sum( (output_sig_fft(l_lim:u_lim)./input_sig_fft(l_lim:u_lim)).* input_sig_fft_weighted(l_lim:u_lim) )./sum(input_sig_fft_weighted(l_lim:u_lim)); %hat so viele Einträge wie Freiheitsgrade; Summe ist jeweils über alle k=1:N/2 von einem Freiheitsgrad
    A_FEI_limited=abs(FEI_limited);
    phi_limited=atan(imag(FEI_limited)./real(FEI_limited));
    feq_limited=sum(input_sig_fft_weighted(l_lim:u_lim).*(freq(l_lim:u_lim)))./sum(input_sig_fft_weighted(l_lim:u_lim)); 
    d_FEI_limited=-phi_limited./(2*pi*feq_limited);
   
%% Save         
    TrackingErrors.E_track_rms = E_track_rms(end); %relative RMS tracking error
    
    TrackingErrors.d_FEI_limited = d_FEI_limited; %delay value of FEI index
    TrackingErrors.A_FEI_limited = A_FEI_limited; %amplitude error of FEI index
end





