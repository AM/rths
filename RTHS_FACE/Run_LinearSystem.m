%% Run_LinearSystem.m
% This is the main script to showcase the function of the FACE (Fidelity 
% Assessment based on Convergence and Extrapolation) method. This model 
% helps to understand the method proposed in the paper "Insam, C. And 
% Rixen, D. J. "Fidelity Assessment of Real-Time Hybrid Substructuring 
% Based on Convergence and Extrapolation". In: Mechanical Systems and 
% Signal Processing 175 (2022)."
% The following Matlab/Simulink files are called:
% * LinearSystem.slx, which calls Init_LinearSystem.m
% * Tracking_Errors: calculates the relative RMS tracking error and the 
%   Frequency Evaluation Index (FEI) using the commanded and measured
%   displacement
% * Check_Implementation: to check the implementation of the FEI index and
%   whether all RTHS simulations were stable
% * mmpolyfit: polynomial fit with constraints; from Mathworks File Exchange:
%   Duane Hanselman (2021). Fit Polynomial to Data with Constraints 
%   (https://www.mathworks.com/matlabcentral/fileexchange/38926-fit-polynomial-to-data-with-constraints), 
%   MATLAB Central File Exchange. Retrieved December 7, 2021.

% Dynamical system: the numerical as well as the experimental substructure
% comprises a mass-spring-damper system
% Transfer system: the actuator is modeled using a measured transfer
% function from an electric actuator at the Chair of Applied Mechanics at
% the Technical University of Munich. The actuator is controlled using a 
% cascaded control scheme (inner velocity control loop and outer position
% control loop paramter K_P). To improve the tracking performance further, 
% a Velocity Feedforward (VFF) can be turned on.

% Application of the FACE method:
% * Error measures e: 
% * Quantities of Interest q: oscillation magnitude magn_RTHS and 
%   oscillation frequency eigenfreq_RTHS_Hz
% * deliberately alter e by: additional delay (magnitude) and change of 
%   controller parameters (frequency)

% The files were created by Christina Insam in December 2021. The research
% project was supported by the Deutsche Forschungsgemeinschaft (DFG, German
% Research Foundation), project number 450801414.

clear all
close all
clc

set(groot, 'defaultAxesTickLabelInterpreter','latex'); set(groot, 'defaultLegendInterpreter','latex');
TUMBlue = [0 0.3961 0.7412];
TUMOrange = [0.8902 0.4471 0.133];
TUMGreen = [162 173 0]/255;
lini = 1.25;

%% Part I: Initialization 
%%%%%%%%%%% Start: Parameters can be changed %%%%%%%%%%% 
delay_vec = [0:0.001:0.01]; %values of artificial delay; called tau_add in the paper 
Kp_vec    = [50:10:90]; %variation of the controller parameters of the position control loop

enable_VFF = 0; %VFF off (0) or on (1)

Init_LinearSystem; %the dynamical system properties within this script can be changed
%%%%%%%%%%% End %%%%%%%%%%% 

%% Part II: Simulate RTHS tests and extract data 
%%%%%%%%%%% Please do not change %%%%%%%%%%%
simulations_vec   = [delay_vec,Kp_vec]; %total number of simulations: 1) vary additional delay 2) vary controller parameters 
N_delay_sims      = length(delay_vec);
N_controller_sims = length(Kp_vec);

Fs = 1000; %sampling frequency, Hz (according to sampling time of Simulink model)

for i=1:(N_delay_sims + N_controller_sims)
        %set delay and controller parameter
        if i <= N_delay_sims
            current_delay = delay_vec(i)
            K_P = Kp_vec(end) %best possible controller bandwidth (Kp cannot be increased further)
        else
            current_delay = 0 %no artificial delay  tau_add
            K_P = Kp_vec(i-N_delay_sims)
        end
        
        %run simulation
        zm_Data = sim('LinearSystem');

        %extract data
         zm   = zm_Data.zm.Data;
         zref = zm_Data.zref.Data;
         zc   = zm_Data.zc.Data;
         Fext = zm_Data.Fext.Data;
         Fint = zm_Data.Fint.Data;

         startindex = 1002; %gravitational forces take effect at t = 1 s

            zm   = zm(startindex:end);
            zc   = zc(startindex:end);
            zref = zref(startindex:end);
            Fext = Fext(startindex:end);
            Fint = Fint(startindex:end);

        % calculate errors
            [TrackingErrors{i}]  = Tracking_Errors(zc,zm,Fs); 
 
        %Analyze zm (measured displacement) with Fast Fourier Transform (FFT);
        %Note: The FFT is taken over the whole RTHS duration (in the
        %      paper, only the first 0.25 s are taken). The magnitude of an FFT
        %      is only correct for steady-state oscillations. This means that the 
        %      magnitude should be interpreted with care.
        %      Furthermore, the FFT does not use a window (e.g., Hanning)
        N  = length(zm);
        Y  = fft(zm)/N;
        P2 = abs(Y); 
        P1 = P2(1:round(N/2)+1);
        P1(2:end-1) = 2*P1(2:end-1);
        freq_Hz = 0:Fs/N:Fs/2; %FFT frequency vector, Hz

            % find eigenfrequency and oscillation magnitude of the RTHS test
            start_search = find(freq_Hz>0.5,1,'first');
            freq_index_FFT      = find(max(P1(start_search:end)) == P1);
            eigenfreq_RTHS_Hz(i) = freq_Hz(freq_index_FFT);
            magn_RTHS_dB(i)      = 20*log10(P1(freq_index_FFT));
            magn_RTHS(i)         = P1(freq_index_FFT);
                        
            % get phase information from FFT              
            angle_FFT{i} = angle(Y)*180/pi;
            angle_FFT{i} = angle_FFT{i}(1:N/2+1);

        % Save
        zm_mat{i} = zm;
        zc_mat{i} = zc;
        zref_mat{i} = zref;
        Fext_mat{i} = Fext;
        Fint_mat{i} = Fint;
        
        % relative RMS tracking errors
        rel_rms_track(i) = TrackingErrors{i}.E_track_rms;
        % Frequency Evaluation Index (FEI), delay value 
        d_FEI_limited(i)      = TrackingErrors{i}.d_FEI_limited;

end

% Check whether FEI implementation outputs meaningful values and whether test remained stable  
    Check_Implementation
    
% extract eigenfrequency and magnitude of the reference solution 
    Zref  = fft(zref)/N;
    P2_ref = abs(Zref); 
    P1_ref = P2_ref(1:round(N/2)+1);
    P1_ref(2:end-1) = 2*P1_ref(2:end-1);       
    freq_index_ref = find(max(P1_ref(start_search:end))==P1_ref);
    magn_ref = P1_ref(freq_index_ref);
    
% Plot measured displacement and reference
figure;
    plot(1.001:1/Fs:tEnd,zm_mat{1},'Color',TUMBlue)
    hold on
    plot(1.001:1/Fs:tEnd,zref,'k--')
    grid on
    xlabel('Time [s]','Interpreter','Latex')
    ylabel('Displacement [m]','Interpreter','Latex')
    legend('RTHS with $e_\textup{min}$','reference','Interpreter','Latex')
    xlim([0 4])

    
%% Part III: Interpolation to find h(e) and extrapolation %% 
%%%%%%%%%%% Start: Parameters can be changed %%%%%%%%%%% 
poly_degree_magn = 1; %polynomial degree for interpolation of magnitude
max_delay_for_interp = delay_vec(end); %maximum delay value used for interpolation (magnitude), can be < delay_vec(end)
e_magn = 'tracking_error'; %select the error measure on the abscissa for the interpolation of the oscillation magnitude; 
                      %relative RMS tracking error "tracking_error" or delay value of the FEI "FEI_index"
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
poly_degree_freq = 3; %polynomial degree for interpolation of frequency
e_freq = 'inv_Kp'; %select the error measure on the abscissa for the interpolation of the oscillation frequency; 
                   %relative RMS tracking error "tracking_error" or inverse of the position gain Kp "inv_Kp"
%%%%%%%%%%% End %%%%%%%%%%% 

%% Interpolation/Extrapolation oscillation magnitude
%%%%%%%%%%% Please do not change %%%%%%%%%%%
max_index_known = find(delay_vec-max_delay_for_interp == min(abs(delay_vec-max_delay_for_interp)));

switch e_magn
    case 'FEI_index'
        e_known    = d_FEI_limited(1:max_index_known);
    case 'tracking_error'
        e_known = rel_rms_track(1:max_index_known);
end

q_known_dB = magn_RTHS_dB(1:max_index_known);  %the polynomial fit uses the magnitude values in dB
q_known    = magn_RTHS(1:max_index_known);

h_e = polyfit(e_known,q_known_dB,poly_degree_magn); %interpolate
%extrapolate
    e_extrapolation = 0:0.005:e_known(1);
    q_extrapolation_dB = polyval(h_e,e_extrapolation);
    q_extrapolation = 10.^(q_extrapolation_dB/20); %dB-->m
%evaluate using h(e)
    e_known_interpolation = e_known(1):0.002:e_known(end);
    q_known_dB_interpolation = polyval(h_e,e_known_interpolation);
    q_known_interpolation = 10.^(q_known_dB_interpolation/20);
    
%Plot
figure; 
    plot(e_known,q_known,'.','MarkerSize',12,'Color',TUMBlue) 
    hold on
    plot(e_known_interpolation,q_known_interpolation,'Color',TUMBlue,'LineWidth',lini)
    plot(e_extrapolation,q_extrapolation,'Color',TUMBlue,'LineWidth',lini','LineStyle','--')
    plot(0,magn_ref,'kx','LineWidth',lini,'MarkerSize',8)
    switch e_magn
        case 'FEI_index'
            xlabel('$d_\textup{FEI}$ [s]','Interpreter','Latex')
        case 'tracking_error'
            xlabel('relative RMS tracking error [-]')
    end
    ylabel('Peak magnitude [m]','Interpreter','Latex')
    grid on
    xlim([0,e_known(end)])
    legend('measured','interpolation','extrapolation','reference','Location','NorthWest')
    
% calculate prediction error; notation: %q_known(1)=qmin, q_extrapolation(1)=qpred, magn_ref=qhat
rel_pred_error_magn      = abs(q_extrapolation(1) - magn_ref)/magn_ref
rel_estimated_error_magn = abs(q_known(1) - q_extrapolation(1))/q_extrapolation(1) 
rel_real_error_magn      = abs(q_known(1) - magn_ref)/magn_ref

%% Interpolation/Extrapolation oscillation frequency 
%%%%%%%%%%% Please do not change %%%%%%%%%%%
clear e_known q_known e_known_interpolation q_known_interpolation e_extrapolation q_extrapolation h_e

switch e_freq
    case 'tracking_error',
        e_known = rel_rms_track(N_delay_sims+1:end);
    case 'inv_Kp'
        e_known = 1./Kp_vec;  
end

q_known              = eigenfreq_RTHS_Hz(1+length(delay_vec):end);
[e_known,sort_index] = sort(e_known);
q_known              = q_known(sort_index);

h_e = mmpolyfit(e_known,q_known,poly_degree_freq,'Slope',[0 0]); %interpolate with gradient(0) curvature(0) = 0
%extrapolate
    e_extrapolation = 0:0.001:min(e_known);
    q_extrapolation = polyval(h_e,e_extrapolation);
%evaluate using h(e)
    e_known_interpolation = e_known(1):0.0002:e_known(end);
    q_known_interpolation = polyval(h_e,e_known_interpolation);

%Plot             
figure;
    plot(e_known,q_known,'.','MarkerSize',12,'Color',TUMBlue) 
    hold on
	plot(e_known_interpolation,q_known_interpolation,'Color',TUMBlue,'LineWidth',lini)
	plot(e_extrapolation,q_extrapolation,'Color',TUMBlue,'LineWidth',lini','LineStyle','--')
	plot(0,f_analytical_Hz,'kx','LineWidth',lini,'MarkerSize',8)
    switch e_freq
        case 'tracking_error',
            xlabel('relative RMS tracking error [-]','Interpreter','Latex')
        case 'inv_Kp'
            xlabel('$K_\textup{P}^{-1}$ $\left[\frac{1}{\mathrm{s}}\right]$','Interpreter','Latex')
    end
    ylabel('Eigenfrequency [Hz]','Interpreter','Latex')
    grid on
	legend('measured','interpolation','extrapolation','reference','Location','NorthOutside')
    
% calculate prediction error; notation: %q_known(1)=qmin, q_extrapolation(1)=qpred, magn_ref=qhat
rel_pred_error_freq      = abs(q_extrapolation(1) - f_analytical_Hz)/f_analytical_Hz
rel_estimated_error_freq = abs(q_known(1) - q_extrapolation(1))/q_extrapolation(1) 
rel_real_error_freq      = abs(q_known(1) - f_analytical_Hz)/f_analytical_Hz

