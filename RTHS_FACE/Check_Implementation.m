%% Check_Implementation.m
% called by Run_LinearSystem.m to check the implementation of the FEI index
% and whether the RTHS simulation remained stable
        
figure;
    plot(d_FEI_limited(1:N_delay_sims),'Color',TUMBlue)
    hold on
    plot(d_FEI_limited(N_delay_sims+1:end),'Color',TUMOrange)
    legend('additional delay','changing controller performance')
    ylabel('dFEI [s]','Interpreter','Latex')
    xlabel('deliberate change','Interpreter','Latex')
    % Check: 
    % * the blue curve should increase monotonically
    % * the orange curve should decrease monotonically
   
% correlation between dFEI (delay calculated from FEI index) and delay_vec (added delay)
correl = corrcoef(d_FEI_limited(1:N_delay_sims),delay_vec);
correl_dFEI_delayvec = correl(1,2)
if correl_dFEI_delayvec <0.99
    disp('Attention: the correlation between dFEI and delay_vec is smaller than 99 %.')
end

% Stability of the RTHS test
for i=1:length(zm_mat)
    if unique(zm_mat{1}>0)
        disp('The simulation might be unstable')
        disp(i)
    end
end

