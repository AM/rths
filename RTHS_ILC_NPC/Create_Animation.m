%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Create_Animation.m
% - Version: November 24th 2021
% - Tutorial and files created by: Christina Insam, Mehroz Hussain, Arian 
%   Kist, Henri Schwalm and Daniel J. Rixen
% - This script is called if the user wants to animate the results of the 
%   virtual RTHS test implemented in the Simulink model
%   "vRTHS_1D_Contact.slx".
% - The animation helps to understand the benefits of using Iterative
%   Learning Control as feedforward controller and Normalized Passivity
%   Control for test stability. This actuator control scheme was proposed 
%   in "Insam C., Kist, A., Schwalm, H. and Rixen, D. J. Robust and high 
%   fidelity real-time hybrid substructuring. Mechanical Systems and Signal 
%   Processing, 157, 2021". Please do not modify this script.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                 Animation                                    %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% Plot for all components   
% animation
    v=VideoWriter('Animation.avi'); %default frame rate 30 fps
    v.Quality = 100;
    open(v);
    h = 0.2;  %spring width
    b = 2;    %wall width
    b2 = 0.1; %wall height
    b3 = 0.1; %wall diagonal lines width
    b4 = 0.4; %wall diagonal lines height
    b5 = 0.8; %spring kE diagonal lines
    b6 = 0.2; %spring Kp diagonal lines
    hd = 0.3; %distance of spring Kp and damper Kd to 0 
    r  = 0.65; %radius of the masses

%Interpolate time
    t = 0:0.001:(1/f_des);
    t_end = t(end);
    t_vec = 0:1/30:t_end;

% Reference 
    z_NUM_Reference = z_NUM_Ref_int(1,:); 

    z_EXP_Reference = z_NUM_Reference-(l0*1000); 
    for i=1:length(z_EXP_Reference) %wenn xV-xE<0, dann xE = 0
        if z_EXP_Reference(i) < 0 
            z_EXP_Reference(i) = 0;
        end
    end
    z_NUM_Reference = z_NUM_Reference/10; %mm to cm
    z_EXP_Reference = z_EXP_Reference/10;
    
% movement of the wall - desired trajectory
    z_des_First   = z_NUM_desired_real_int(1,:);
    z_des_Last   = z_NUM_desired_real_int(Iterations,:);
    z_des_First   = z_des_First/10; %mm to cm
    z_des_Last   = z_des_Last/10;

% TCP real
    z_NUM_First = z_NUM_real_int(1,:);
    z_NUM_Last = z_NUM_real_int(Iterations,:);

    z_EXP_First = z_NUM_First-(l0*1000);
    z_EXP_Last = z_NUM_Last-(l0*1000);
    
    for i=1:length(z_EXP_First) %wenn xV-xE<0, dann xE = 0
        if z_EXP_First(i) < 0 
            z_EXP_First(i) = 0;
        end
    end
    
    for i=1:length(z_EXP_Last) %wenn xV-xE<0, dann xE = 0
        if z_EXP_Last(i) < 0 
            z_EXP_Last(i) = 0;
        end
    end
    
    z_NUM_First = z_NUM_First/10; %mm to cm
    z_EXP_First = z_EXP_First/10;
    z_NUM_Last = z_NUM_Last/10;
    z_EXP_Last = z_EXP_Last/10;
    
    for idt=1:length(t_vec) %t_vec ist der Zeitvektor für den Film
        t_act = t_vec(idt);
        param(idt) = find(abs(t-t_act) <= 0.0009,1,'first');
    end
  
    z_NUM_Ref_Filmtime   = z_NUM_Reference(param(1,1:(end-1)))+r;
    z_EXP_Ref_Filmtime   = z_EXP_Reference(param(1,1:(end-1)))+r;
    z_NUM_Ref_Filmtime_2 = z_NUM_Reference(param(1,1:(end-1)));
    z_NUM_First_Filmtime = z_NUM_First(param(1,1:(end-1)))+r; 
    z_EXP_First_Filmtime = z_EXP_First(param(1,1:(end-1)))+r;
    z_NUM_Last_Filmtime = z_NUM_Last(param(1,1:(end-1)))+r;
    z_EXP_Last_Filmtime = z_EXP_Last(param(1,1:(end-1)))+r;
    z_des_First_Filmtime = z_des_First(param(1,1:(end-1)));
    z_des_Last_Filmtime = z_des_Last(param(1,1:(end-1)));
    
% change CoSy
z_NUM_Ref_Filmtime = - z_NUM_Ref_Filmtime;
z_EXP_Ref_Filmtime = -z_EXP_Ref_Filmtime;
z_NUM_Ref_Filmtime_2 = - z_NUM_Ref_Filmtime_2;
z_NUM_First_Filmtime = - z_NUM_First_Filmtime;
z_EXP_First_Filmtime = - z_EXP_First_Filmtime;
z_des_First_Filmtime = - z_des_First_Filmtime;
z_NUM_Last_Filmtime = - z_NUM_Last_Filmtime;
z_EXP_Last_Filmtime = - z_EXP_Last_Filmtime;
z_des_Last_Filmtime = - z_des_Last_Filmtime;

figure('Units','centimeters','Position',[0,0,30,20])

%% Plot 
    for idx = 1:(length(t_vec)-1)
        clf
        figure(8)
        subplot(1,2,1);     %First Iteration
        hold on
        syms x y

        plot(t_vec(1:idx),z_NUM_First_Filmtime(1:idx)+r,'Color',[0 0.4 0.74]) 
           
        % Numerical Mass circle
        draw_circle(t_vec(idx),z_NUM_First_Filmtime(idx),r,TUMBlue,1);
        axis equal

%         %Experimental Mass Circle
        draw_circle(t_vec(idx), z_EXP_First_Filmtime(idx),r,TUMGreen,1);
 
        % spring kE
        curr_distance = abs(z_NUM_First_Filmtime(idx)+r)-abs(z_EXP_First_Filmtime(idx)-r);
        patch([t_vec(idx)-h/2 t_vec(idx)+h/2 t_vec(idx)+h/2 t_vec(idx)-h/2],            [z_NUM_First_Filmtime(idx)+r z_NUM_First_Filmtime(idx)+r z_NUM_First_Filmtime(idx)+r+ 0.1*curr_distance z_NUM_First_Filmtime(idx)+r+ 0.1*curr_distance],TUMGreen,'EdgeColor','none');
        patch([t_vec(idx)+h/2 t_vec(idx)+h/2+b5 t_vec(idx)-h/2+b5 t_vec(idx)-h/2],      [z_NUM_First_Filmtime(idx)+r+0.1*curr_distance z_NUM_First_Filmtime(idx)+r+0.15*curr_distance z_NUM_First_Filmtime(idx)+r+0.15*curr_distance z_NUM_First_Filmtime(idx)+r+0.1*curr_distance],TUMGreen,'EdgeColor','none');
        patch([t_vec(idx)+h/2+b5 t_vec(idx)+h/2-b5 t_vec(idx)-h/2-b5 t_vec(idx)-h/2+b5],[z_NUM_First_Filmtime(idx)+r+0.15*curr_distance z_NUM_First_Filmtime(idx)+r+0.2*curr_distance z_NUM_First_Filmtime(idx)+r+0.2*curr_distance z_NUM_First_Filmtime(idx)+r+0.15*curr_distance],TUMGreen,'EdgeColor','none');
        patch([t_vec(idx)+h/2-b5 t_vec(idx)+h/2+b5 t_vec(idx)-h/2+b5 t_vec(idx)-h/2-b5],[z_NUM_First_Filmtime(idx)+r+0.2*curr_distance z_NUM_First_Filmtime(idx)+r+0.25*curr_distance z_NUM_First_Filmtime(idx)+r+0.25*curr_distance z_NUM_First_Filmtime(idx)+r+0.2*curr_distance],TUMGreen,'EdgeColor','none');
        patch([t_vec(idx)+h/2+b5 t_vec(idx)+h/2-b5 t_vec(idx)-h/2-b5 t_vec(idx)-h/2+b5],[z_NUM_First_Filmtime(idx)+r+0.25*curr_distance z_NUM_First_Filmtime(idx)+r+0.3*curr_distance z_NUM_First_Filmtime(idx)+r+0.3*curr_distance z_NUM_First_Filmtime(idx)+r+0.25*curr_distance],TUMGreen,'EdgeColor','none');      
        patch([t_vec(idx)+h/2-b5 t_vec(idx)+h/2+b5 t_vec(idx)-h/2+b5 t_vec(idx)-h/2-b5],[z_NUM_First_Filmtime(idx)+r+0.3*curr_distance z_NUM_First_Filmtime(idx)+r+0.35*curr_distance z_NUM_First_Filmtime(idx)+r+0.35*curr_distance z_NUM_First_Filmtime(idx)+r+0.3*curr_distance],TUMGreen,'EdgeColor','none');
        patch([t_vec(idx)+h/2+b5 t_vec(idx)+h/2-b5 t_vec(idx)-h/2-b5 t_vec(idx)-h/2+b5],[z_NUM_First_Filmtime(idx)+r+0.35*curr_distance z_NUM_First_Filmtime(idx)+r+0.4*curr_distance z_NUM_First_Filmtime(idx)+r+0.4*curr_distance z_NUM_First_Filmtime(idx)+r+0.35*curr_distance],TUMGreen,'EdgeColor','none');
        patch([t_vec(idx)+h/2-b5 t_vec(idx)+h/2+b5 t_vec(idx)-h/2+b5 t_vec(idx)-h/2-b5],[z_NUM_First_Filmtime(idx)+r+0.4*curr_distance z_NUM_First_Filmtime(idx)+r+0.45*curr_distance z_NUM_First_Filmtime(idx)+r+0.45*curr_distance z_NUM_First_Filmtime(idx)+r+0.4*curr_distance],TUMGreen,'EdgeColor','none');
        patch([t_vec(idx)+h/2+b5 t_vec(idx)+h/2-b5 t_vec(idx)-h/2-b5 t_vec(idx)-h/2+b5],[z_NUM_First_Filmtime(idx)+r+0.45*curr_distance z_NUM_First_Filmtime(idx)+r+0.5*curr_distance z_NUM_First_Filmtime(idx)+r+0.5*curr_distance z_NUM_First_Filmtime(idx)+r+0.45*curr_distance],TUMGreen,'EdgeColor','none'); 
        patch([t_vec(idx)+h/2-b5 t_vec(idx)+h/2+b5 t_vec(idx)-h/2+b5 t_vec(idx)-h/2-b5],[z_NUM_First_Filmtime(idx)+r+0.5*curr_distance z_NUM_First_Filmtime(idx)+r+0.55*curr_distance z_NUM_First_Filmtime(idx)+r+0.55*curr_distance z_NUM_First_Filmtime(idx)+r+0.5*curr_distance],TUMGreen,'EdgeColor','none');
        patch([t_vec(idx)+h/2+b5 t_vec(idx)+h/2-b5 t_vec(idx)-h/2-b5 t_vec(idx)-h/2+b5],[z_NUM_First_Filmtime(idx)+r+0.55*curr_distance z_NUM_First_Filmtime(idx)+r+0.6*curr_distance z_NUM_First_Filmtime(idx)+r+0.6*curr_distance z_NUM_First_Filmtime(idx)+r+0.55*curr_distance],TUMGreen,'EdgeColor','none');
        patch([t_vec(idx)+h/2-b5 t_vec(idx)+h/2+b5 t_vec(idx)-h/2+b5 t_vec(idx)-h/2-b5],[z_NUM_First_Filmtime(idx)+r+0.6*curr_distance z_NUM_First_Filmtime(idx)+r+0.65*curr_distance z_NUM_First_Filmtime(idx)+r+0.65*curr_distance z_NUM_First_Filmtime(idx)+r+0.6*curr_distance],TUMGreen,'EdgeColor','none');
        patch([t_vec(idx)+h/2+b5 t_vec(idx)+h/2-b5 t_vec(idx)-h/2-b5 t_vec(idx)-h/2+b5],[z_NUM_First_Filmtime(idx)+r+0.65*curr_distance z_NUM_First_Filmtime(idx)+r+0.7*curr_distance z_NUM_First_Filmtime(idx)+r+0.7*curr_distance z_NUM_First_Filmtime(idx)+r+0.65*curr_distance],TUMGreen,'EdgeColor','none');
        patch([t_vec(idx)+h/2-b5 t_vec(idx)+h/2+b5 t_vec(idx)-h/2+b5 t_vec(idx)-h/2-b5],[z_NUM_First_Filmtime(idx)+r+0.7*curr_distance z_NUM_First_Filmtime(idx)+r+0.75*curr_distance z_NUM_First_Filmtime(idx)+r+0.75*curr_distance z_NUM_First_Filmtime(idx)+r+0.7*curr_distance],TUMGreen,'EdgeColor','none');
        patch([t_vec(idx)+h/2+b5 t_vec(idx)+h/2-b5 t_vec(idx)-h/2-b5 t_vec(idx)-h/2+b5],[z_NUM_First_Filmtime(idx)+r+0.75*curr_distance z_NUM_First_Filmtime(idx)+r+0.8*curr_distance z_NUM_First_Filmtime(idx)+r+0.8*curr_distance z_NUM_First_Filmtime(idx)+r+0.75*curr_distance],TUMGreen,'EdgeColor','none');
        patch([t_vec(idx)+h/2-b5 t_vec(idx)+h/2+b5 t_vec(idx)-h/2+b5 t_vec(idx)-h/2-b5],[z_NUM_First_Filmtime(idx)+r+0.8*curr_distance z_NUM_First_Filmtime(idx)+r+0.85*curr_distance z_NUM_First_Filmtime(idx)+r+0.85*curr_distance z_NUM_First_Filmtime(idx)+r+0.8*curr_distance],TUMGreen,'EdgeColor','none');
        patch([t_vec(idx)+h/2+b5 t_vec(idx)+h/2-b5 t_vec(idx)-h/2-b5 t_vec(idx)-h/2+b5],[z_NUM_First_Filmtime(idx)+r+0.85*curr_distance z_NUM_First_Filmtime(idx)+r+0.9*curr_distance z_NUM_First_Filmtime(idx)+r+0.9*curr_distance z_NUM_First_Filmtime(idx)+r+0.85*curr_distance],TUMGreen,'EdgeColor','none');
        patch([t_vec(idx)+h/2-b5 t_vec(idx)+h/2    t_vec(idx)-h/2    t_vec(idx)-h/2-b5],[z_NUM_First_Filmtime(idx)+r+0.9*curr_distance z_NUM_First_Filmtime(idx)+r+0.95*curr_distance z_NUM_First_Filmtime(idx)+r+0.95*curr_distance z_NUM_First_Filmtime(idx)+r+0.9*curr_distance],TUMGreen,'EdgeColor','none');
        patch([t_vec(idx)+h/2    t_vec(idx)+h/2    t_vec(idx)-h/2    t_vec(idx)-h/2],   [z_NUM_First_Filmtime(idx)+r+0.95*curr_distance z_NUM_First_Filmtime(idx)+r+curr_distance z_NUM_First_Filmtime(idx)+r+curr_distance z_NUM_First_Filmtime(idx)+r+0.95*curr_distance],TUMGreen,'EdgeColor','none');
        
        %wall
        patch([t_vec(idx)+b/2 t_vec(idx)+b/2 t_vec(idx)-b/2 t_vec(idx)-b/2],[z_des_First_Filmtime(idx)-b2/2-r*4 z_des_First_Filmtime(idx)+b2/2-r*4 z_des_First_Filmtime(idx)+b2/2-r*4 z_des_First_Filmtime(idx)-b2/2-r*4],TUMBlue,'EdgeColor','none'); 
        
        %wall diagonals
        patch([t_vec(idx)+b3/2-b4+0.1 t_vec(idx)+b3/2+0.1 t_vec(idx)-b3/2+0.1 t_vec(idx)-b3/2-b4+0.1],[z_des_First_Filmtime(idx)+b2/2-r*4-b4 z_des_First_Filmtime(idx)+b2/2+b4-r*4-b4 z_des_First_Filmtime(idx)+b2/2+b4-r*4-b4 z_des_First_Filmtime(idx)+b2/2-r*4-b4],TUMBlue,'EdgeColor','none');     
        patch([t_vec(idx)+b3/2+0.3*b-b4+0.1 t_vec(idx)+b3/2+0.3*b+0.1 t_vec(idx)-b3/2+0.3*b+0.1 t_vec(idx)-b3/2+0.3*b-b4+0.1],[z_des_First_Filmtime(idx)+b2/2-r*4-b4 z_des_First_Filmtime(idx)+b2/2+b4-r*4-b4 z_des_First_Filmtime(idx)+b2/2+b4-r*4-b4 z_des_First_Filmtime(idx)+b2/2-r*4-b4],TUMBlue,'EdgeColor','none');
        patch([t_vec(idx)+b3/2-0.3*b-b4+0.1 t_vec(idx)+b3/2-0.3*b+0.1 t_vec(idx)-b3/2-0.3*b+0.1 t_vec(idx)-b3/2-0.3*b-b4+0.1],[z_des_First_Filmtime(idx)+b2/2-r*4-b4 z_des_First_Filmtime(idx)+b2/2+b4-r*4-b4 z_des_First_Filmtime(idx)+b2/2+b4-r*4-b4 z_des_First_Filmtime(idx)+b2/2-r*4-b4],TUMBlue,'EdgeColor','none');
       
        %PD-Regulator
        curr_distance_wall = abs(z_des_First_Filmtime(idx)+b2/2-r*4)-abs(z_NUM_First_Filmtime(idx)-r)-b2/2;
        curr_distance_wall_spring = curr_distance_wall + (r-r*cos(asin(hd/r))) + b2;
        
        % Damper
        patch([t_vec(idx)+hd-h/4 t_vec(idx)+hd+h/4 t_vec(idx)+hd+h/4 t_vec(idx)+hd-h/4],[z_des_First_Filmtime(idx)+b2/2-r*4 z_des_First_Filmtime(idx)+b2/2-r*4 z_des_First_Filmtime(idx)+b2/2-r*4+1/2*curr_distance_wall z_des_First_Filmtime(idx)+b2/2-r*4+1/2*curr_distance_wall],TUMBlue,'EdgeColor','none')
        damper_width = 7/8; %of hd
        inner_damper_width = 0.4;
        patch([t_vec(idx)+hd-hd*damper_width t_vec(idx)+hd+hd*damper_width t_vec(idx)+hd+hd*damper_width t_vec(idx)+hd-hd*damper_width],[z_des_First_Filmtime(idx)+b2/2-r*4+1/2*curr_distance_wall z_des_First_Filmtime(idx)+b2/2-r*4+1/2*curr_distance_wall z_des_First_Filmtime(idx)+b2/2-r*4+0.6*curr_distance_wall z_des_First_Filmtime(idx)+b2/2-r*4+0.6*curr_distance_wall],TUMBlue,'EdgeColor','none')
        patch([t_vec(idx)+hd-hd*damper_width t_vec(idx)+hd-hd*damper_width+h/2 t_vec(idx)+hd-hd*damper_width+h/2 t_vec(idx)+hd-hd*damper_width],[z_des_First_Filmtime(idx)+b2/2-r*4+0.6*curr_distance_wall z_des_First_Filmtime(idx)+b2/2-r*4+0.6*curr_distance_wall z_des_First_Filmtime(idx)+b2/2-r*4+3/4*curr_distance_wall z_des_First_Filmtime(idx)+b2/2-r*4+3/4*curr_distance_wall],TUMBlue,'EdgeColor','none')
        patch([t_vec(idx)+hd+hd*damper_width-h/2 t_vec(idx)+hd+hd*damper_width t_vec(idx)+hd+hd*damper_width t_vec(idx)+hd+hd*damper_width-h/2],[z_des_First_Filmtime(idx)+b2/2-r*4+0.6*curr_distance_wall z_des_First_Filmtime(idx)+b2/2-r*4+0.6*curr_distance_wall z_des_First_Filmtime(idx)+b2/2-r*4+3/4*curr_distance_wall z_des_First_Filmtime(idx)+b2/2-r*4+3/4*curr_distance_wall],TUMBlue,'EdgeColor','none')  
        patch([t_vec(idx)+hd-hd*inner_damper_width t_vec(idx)+hd+hd*inner_damper_width t_vec(idx)+hd+hd*inner_damper_width t_vec(idx)+hd-hd*inner_damper_width],[z_des_First_Filmtime(idx)+b2/2-r*4+2/3*curr_distance_wall z_des_First_Filmtime(idx)+b2/2-r*4+2/3*curr_distance_wall z_des_First_Filmtime(idx)+b2/2-r*4+3/4*curr_distance_wall z_des_First_Filmtime(idx)+b2/2-r*4+3/4*curr_distance_wall],TUMBlue,'EdgeColor','none')
        patch([t_vec(idx)+hd-h/4 t_vec(idx)+hd+h/4 t_vec(idx)+hd+h/4 t_vec(idx)+hd-h/4],[z_des_First_Filmtime(idx)+b2/2-r*4+3/4*curr_distance_wall z_des_First_Filmtime(idx)+b2/2-r*4+3/4*curr_distance_wall z_NUM_First_Filmtime(idx) z_NUM_First_Filmtime(idx)],TUMBlue,'EdgeColor','none')
        
        % Feder Kp
        patch([t_vec(idx)-hd-h/2 t_vec(idx)-hd+h/2 t_vec(idx)-hd+h/2 t_vec(idx)-hd-h/2],            [z_des_First_Filmtime(idx)+b2/2-4*r                               z_des_First_Filmtime(idx)+b2/2-4*r                                z_des_First_Filmtime(idx)+b2/2-4*r+0.1*curr_distance_wall_spring  z_des_First_Filmtime(idx)+b2/2-4*r+0.1*curr_distance_wall_spring],TUMBlue,'EdgeColor','none');
        patch([t_vec(idx)-hd+h/2 t_vec(idx)-hd+h/2+b6 t_vec(idx)-hd-h/2+b6 t_vec(idx)-hd-h/2],      [z_des_First_Filmtime(idx)+b2/2-4*r+0.1*curr_distance_wall_spring z_des_First_Filmtime(idx)+b2/2-4*r+0.2*curr_distance_wall_spring  z_des_First_Filmtime(idx)+b2/2-4*r+0.2*curr_distance_wall_spring  z_des_First_Filmtime(idx)+b2/2-4*r+0.1*curr_distance_wall_spring],TUMBlue,'EdgeColor','none');
        patch([t_vec(idx)-hd+h/2+b6 t_vec(idx)-hd+h/2-b6 t_vec(idx)-hd-h/2-b6 t_vec(idx)-hd-h/2+b6],[z_des_First_Filmtime(idx)+b2/2-4*r+0.2*curr_distance_wall_spring z_des_First_Filmtime(idx)+b2/2-4*r+0.4*curr_distance_wall_spring z_des_First_Filmtime(idx)+b2/2-4*r+0.4*curr_distance_wall_spring  z_des_First_Filmtime(idx)+b2/2-4*r+0.2*curr_distance_wall_spring],TUMBlue,'EdgeColor','none');
        patch([t_vec(idx)-hd+h/2-b6 t_vec(idx)-hd+h/2+b6 t_vec(idx)-hd-h/2+b6 t_vec(idx)-hd-h/2-b6],[z_des_First_Filmtime(idx)+b2/2-4*r+0.4*curr_distance_wall_spring z_des_First_Filmtime(idx)+b2/2-4*r+0.6*curr_distance_wall_spring z_des_First_Filmtime(idx)+b2/2-4*r+0.6*curr_distance_wall_spring  z_des_First_Filmtime(idx)+b2/2-4*r+0.4*curr_distance_wall_spring],TUMBlue,'EdgeColor','none');
        patch([t_vec(idx)-hd+h/2+b6 t_vec(idx)-hd+h/2-b6 t_vec(idx)-hd-h/2-b6 t_vec(idx)-hd-h/2+b6],[z_des_First_Filmtime(idx)+b2/2-4*r+0.6*curr_distance_wall_spring z_des_First_Filmtime(idx)+b2/2-4*r+0.8*curr_distance_wall_spring z_des_First_Filmtime(idx)+b2/2-4*r+0.8*curr_distance_wall_spring  z_des_First_Filmtime(idx)+b2/2-4*r+0.6*curr_distance_wall_spring],TUMBlue,'EdgeColor','none');
        patch([t_vec(idx)-hd+h/2-b6 t_vec(idx)-hd+h/2    t_vec(idx)-hd-h/2    t_vec(idx)-hd-h/2-b6],[z_des_First_Filmtime(idx)+b2/2-4*r+0.8*curr_distance_wall_spring z_des_First_Filmtime(idx)+b2/2-4*r+0.9*curr_distance_wall_spring z_des_First_Filmtime(idx)+b2/2-4*r+0.9*curr_distance_wall_spring  z_des_First_Filmtime(idx)+b2/2-4*r+0.8*curr_distance_wall_spring],TUMBlue,'EdgeColor','none');
        patch([t_vec(idx)-hd+h/2    t_vec(idx)-hd+h/2    t_vec(idx)-hd-h/2    t_vec(idx)-hd-h/2],   [z_des_First_Filmtime(idx)+b2/2-4*r+0.9*curr_distance_wall_spring z_des_First_Filmtime(idx)+b2/2-4*r+curr_distance_wall_spring     z_des_First_Filmtime(idx)+b2/2-4*r+curr_distance_wall_spring      z_des_First_Filmtime(idx)+b2/2-4*r+0.9*curr_distance_wall_spring],TUMBlue,'EdgeColor','none');

        %Ground at 0
        patch([t_vec(idx)-1.5,          t_vec(idx)+1.5,          t_vec(idx)+1.5,          t_vec(idx)-1.5],[0 0 0.15 0.15],TUMGreen,'EdgeColor','none')
        patch([t_vec(idx)-0.075-0.1     t_vec(idx)+0.075-0.1     t_vec(idx)+0.5+0.075-0.1 t_vec(idx)+0.5-0.075-0.1],[0.15 0.15 0.65 0.65],TUMGreen,'EdgeColor','none')
        patch([t_vec(idx)-0.075-0.5-0.1 t_vec(idx)+0.075-0.5-0.1 t_vec(idx)+0.075-0.1     t_vec(idx)-0.075-0.1],[0.15 0.15 0.65 0.65],TUMGreen,'EdgeColor','none')
        patch([t_vec(idx)-0.075+0.5-0.1 t_vec(idx)+0.075+0.5-0.1 t_vec(idx)+1+0.075-0.1     t_vec(idx)+1-0.075-0.1],[0.15 0.15 0.65 0.65],TUMGreen,'EdgeColor','none')
        patch([t_vec(idx)-0.075-1-0.1   t_vec(idx)+0.075-1-0.1   t_vec(idx)+0.075-0.1-0.5 t_vec(idx)-0.075-0.1-0.5],[0.15 0.15 0.65 0.65],TUMGreen,'EdgeColor','none')
        patch([t_vec(idx)-0.075+1-0.1   t_vec(idx)+0.075+1-0.1   t_vec(idx)+1.5+0.075-0.1 t_vec(idx)+1.5-0.075-0.1],[0.15 0.15 0.65 0.65],TUMGreen,'EdgeColor','none')
        
        %Stationary Ref
        transparency = 1;
        draw_circle(t_vec(1),z_NUM_Ref_Filmtime(1),r,TUMBlack,transparency);
        draw_circle(t_vec(1), z_EXP_Ref_Filmtime(1),r,TUMBlack,transparency);
            
        % spring kE
        curr_distance = abs(z_NUM_Ref_Filmtime(1)+r)-abs(z_EXP_Ref_Filmtime(1)-r);
        patch([t_vec(1)-h/2 t_vec(1)+h/2 t_vec(1)+h/2 t_vec(1)-h/2],            [z_NUM_Ref_Filmtime(1)+r z_NUM_Ref_Filmtime(1)+r z_NUM_Ref_Filmtime(1)+r+ 0.1*curr_distance z_NUM_Ref_Filmtime(1)+r+ 0.1*curr_distance],TUMBlack,'EdgeColor','none','FaceAlpha',transparency);
        patch([t_vec(1)+h/2 t_vec(1)+h/2+b5 t_vec(1)-h/2+b5 t_vec(1)-h/2],      [z_NUM_Ref_Filmtime(1)+r+0.1*curr_distance z_NUM_Ref_Filmtime(1)+r+0.15*curr_distance z_NUM_Ref_Filmtime(1)+r+0.15*curr_distance z_NUM_Ref_Filmtime(1)+r+0.1*curr_distance],TUMBlack,'EdgeColor','none','FaceAlpha',transparency);
        patch([t_vec(1)+h/2+b5 t_vec(1)+h/2-b5 t_vec(1)-h/2-b5 t_vec(1)-h/2+b5],[z_NUM_Ref_Filmtime(1)+r+0.15*curr_distance z_NUM_Ref_Filmtime(1)+r+0.2*curr_distance z_NUM_Ref_Filmtime(1)+r+0.2*curr_distance z_NUM_Ref_Filmtime(1)+r+0.15*curr_distance],TUMBlack,'EdgeColor','none','FaceAlpha',transparency);
        patch([t_vec(1)+h/2-b5 t_vec(1)+h/2+b5 t_vec(1)-h/2+b5 t_vec(1)-h/2-b5],[z_NUM_Ref_Filmtime(1)+r+0.2*curr_distance z_NUM_Ref_Filmtime(1)+r+0.25*curr_distance z_NUM_Ref_Filmtime(1)+r+0.25*curr_distance z_NUM_Ref_Filmtime(1)+r+0.2*curr_distance],TUMBlack,'EdgeColor','none','FaceAlpha',transparency);
        patch([t_vec(1)+h/2+b5 t_vec(1)+h/2-b5 t_vec(1)-h/2-b5 t_vec(1)-h/2+b5],[z_NUM_Ref_Filmtime(1)+r+0.25*curr_distance z_NUM_Ref_Filmtime(1)+r+0.3*curr_distance z_NUM_Ref_Filmtime(1)+r+0.3*curr_distance z_NUM_Ref_Filmtime(1)+r+0.25*curr_distance],TUMBlack,'EdgeColor','none','FaceAlpha',transparency);      
        patch([t_vec(1)+h/2-b5 t_vec(1)+h/2+b5 t_vec(1)-h/2+b5 t_vec(1)-h/2-b5],[z_NUM_Ref_Filmtime(1)+r+0.3*curr_distance z_NUM_Ref_Filmtime(1)+r+0.35*curr_distance z_NUM_Ref_Filmtime(1)+r+0.35*curr_distance z_NUM_Ref_Filmtime(1)+r+0.3*curr_distance],TUMBlack,'EdgeColor','none','FaceAlpha',transparency);
        patch([t_vec(1)+h/2+b5 t_vec(1)+h/2-b5 t_vec(1)-h/2-b5 t_vec(1)-h/2+b5],[z_NUM_Ref_Filmtime(1)+r+0.35*curr_distance z_NUM_Ref_Filmtime(1)+r+0.4*curr_distance z_NUM_Ref_Filmtime(1)+r+0.4*curr_distance z_NUM_Ref_Filmtime(1)+r+0.35*curr_distance],TUMBlack,'EdgeColor','none','FaceAlpha',transparency);
        patch([t_vec(1)+h/2-b5 t_vec(1)+h/2+b5 t_vec(1)-h/2+b5 t_vec(1)-h/2-b5],[z_NUM_Ref_Filmtime(1)+r+0.4*curr_distance z_NUM_Ref_Filmtime(1)+r+0.45*curr_distance z_NUM_Ref_Filmtime(1)+r+0.45*curr_distance z_NUM_Ref_Filmtime(1)+r+0.4*curr_distance],TUMBlack,'EdgeColor','none','FaceAlpha',transparency);
        patch([t_vec(1)+h/2+b5 t_vec(1)+h/2-b5 t_vec(1)-h/2-b5 t_vec(1)-h/2+b5],[z_NUM_Ref_Filmtime(1)+r+0.45*curr_distance z_NUM_Ref_Filmtime(1)+r+0.5*curr_distance z_NUM_Ref_Filmtime(1)+r+0.5*curr_distance z_NUM_Ref_Filmtime(1)+r+0.45*curr_distance],TUMBlack,'EdgeColor','none','FaceAlpha',transparency); 
        patch([t_vec(1)+h/2-b5 t_vec(1)+h/2+b5 t_vec(1)-h/2+b5 t_vec(1)-h/2-b5],[z_NUM_Ref_Filmtime(1)+r+0.5*curr_distance z_NUM_Ref_Filmtime(1)+r+0.55*curr_distance z_NUM_Ref_Filmtime(1)+r+0.55*curr_distance z_NUM_Ref_Filmtime(1)+r+0.5*curr_distance],TUMBlack,'EdgeColor','none','FaceAlpha',transparency);
        patch([t_vec(1)+h/2+b5 t_vec(1)+h/2-b5 t_vec(1)-h/2-b5 t_vec(1)-h/2+b5],[z_NUM_Ref_Filmtime(1)+r+0.55*curr_distance z_NUM_Ref_Filmtime(1)+r+0.6*curr_distance z_NUM_Ref_Filmtime(1)+r+0.6*curr_distance z_NUM_Ref_Filmtime(1)+r+0.55*curr_distance],TUMBlack,'EdgeColor','none','FaceAlpha',transparency);
        patch([t_vec(1)+h/2-b5 t_vec(1)+h/2+b5 t_vec(1)-h/2+b5 t_vec(1)-h/2-b5],[z_NUM_Ref_Filmtime(1)+r+0.6*curr_distance z_NUM_Ref_Filmtime(1)+r+0.65*curr_distance z_NUM_Ref_Filmtime(1)+r+0.65*curr_distance z_NUM_Ref_Filmtime(1)+r+0.6*curr_distance],TUMBlack,'EdgeColor','none','FaceAlpha',transparency);
        patch([t_vec(1)+h/2+b5 t_vec(1)+h/2-b5 t_vec(1)-h/2-b5 t_vec(1)-h/2+b5],[z_NUM_Ref_Filmtime(1)+r+0.65*curr_distance z_NUM_Ref_Filmtime(1)+r+0.7*curr_distance z_NUM_Ref_Filmtime(1)+r+0.7*curr_distance z_NUM_Ref_Filmtime(1)+r+0.65*curr_distance],TUMBlack,'EdgeColor','none','FaceAlpha',transparency);
        patch([t_vec(1)+h/2-b5 t_vec(1)+h/2+b5 t_vec(1)-h/2+b5 t_vec(1)-h/2-b5],[z_NUM_Ref_Filmtime(1)+r+0.7*curr_distance z_NUM_Ref_Filmtime(1)+r+0.75*curr_distance z_NUM_Ref_Filmtime(1)+r+0.75*curr_distance z_NUM_Ref_Filmtime(1)+r+0.7*curr_distance],TUMBlack,'EdgeColor','none','FaceAlpha',transparency);
        patch([t_vec(1)+h/2+b5 t_vec(1)+h/2-b5 t_vec(1)-h/2-b5 t_vec(1)-h/2+b5],[z_NUM_Ref_Filmtime(1)+r+0.75*curr_distance z_NUM_Ref_Filmtime(1)+r+0.8*curr_distance z_NUM_Ref_Filmtime(1)+r+0.8*curr_distance z_NUM_Ref_Filmtime(1)+r+0.75*curr_distance],TUMBlack,'EdgeColor','none','FaceAlpha',transparency);
        patch([t_vec(1)+h/2-b5 t_vec(1)+h/2+b5 t_vec(1)-h/2+b5 t_vec(1)-h/2-b5],[z_NUM_Ref_Filmtime(1)+r+0.8*curr_distance z_NUM_Ref_Filmtime(1)+r+0.85*curr_distance z_NUM_Ref_Filmtime(1)+r+0.85*curr_distance z_NUM_Ref_Filmtime(1)+r+0.8*curr_distance],TUMBlack,'EdgeColor','none','FaceAlpha',transparency);
        patch([t_vec(1)+h/2+b5 t_vec(1)+h/2-b5 t_vec(1)-h/2-b5 t_vec(1)-h/2+b5],[z_NUM_Ref_Filmtime(1)+r+0.85*curr_distance z_NUM_Ref_Filmtime(1)+r+0.9*curr_distance z_NUM_Ref_Filmtime(1)+r+0.9*curr_distance z_NUM_Ref_Filmtime(1)+r+0.85*curr_distance],TUMBlack,'EdgeColor','none','FaceAlpha',transparency);
        patch([t_vec(1)+h/2-b5 t_vec(1)+h/2    t_vec(1)-h/2    t_vec(1)-h/2-b5],[z_NUM_Ref_Filmtime(1)+r+0.9*curr_distance z_NUM_Ref_Filmtime(1)+r+0.95*curr_distance z_NUM_Ref_Filmtime(1)+r+0.95*curr_distance z_NUM_Ref_Filmtime(1)+r+0.9*curr_distance],TUMBlack,'EdgeColor','none','FaceAlpha',transparency);
        patch([t_vec(1)+h/2    t_vec(1)+h/2    t_vec(1)-h/2    t_vec(1)-h/2],   [z_NUM_Ref_Filmtime(1)+r+0.95*curr_distance z_NUM_Ref_Filmtime(1)+r+curr_distance z_NUM_Ref_Filmtime(1)+r+curr_distance z_NUM_Ref_Filmtime(1)+r+0.95*curr_distance],TUMBlack,'EdgeColor','none','FaceAlpha',transparency); 
        
        %wall
        patch([t_vec(1)+b/2 t_vec(1)+b/2 t_vec(1)-b/2 t_vec(1)-b/2],[z_NUM_Ref_Filmtime_2(1)-b2/2-r*4 z_NUM_Ref_Filmtime_2(1)+b2/2-r*4 z_NUM_Ref_Filmtime_2(1)+b2/2-r*4 z_NUM_Ref_Filmtime_2(1)-b2/2-r*4],TUMBlack,'EdgeColor','none','FaceAlpha',transparency); 
        
        %wall diagonals
        patch([t_vec(1)+b3/2-b4+0.1 t_vec(1)+b3/2+0.1 t_vec(1)-b3/2+0.1 t_vec(1)-b3/2-b4+0.1],[z_NUM_Ref_Filmtime_2(1)+b2/2-r*4-b4 z_NUM_Ref_Filmtime_2(1)+b2/2+b4-r*4-b4 z_NUM_Ref_Filmtime_2(1)+b2/2+b4-r*4-b4 z_NUM_Ref_Filmtime_2(1)+b2/2-r*4-b4],TUMBlack,'EdgeColor','none','FaceAlpha',transparency);     
        patch([t_vec(1)+b3/2+0.3*b-b4+0.1 t_vec(1)+b3/2+0.3*b+0.1 t_vec(1)-b3/2+0.3*b+0.1 t_vec(1)-b3/2+0.3*b-b4+0.1],[z_NUM_Ref_Filmtime_2(1)+b2/2-r*4-b4 z_NUM_Ref_Filmtime_2(1)+b2/2+b4-r*4-b4 z_NUM_Ref_Filmtime_2(1)+b2/2+b4-r*4-b4 z_NUM_Ref_Filmtime_2(1)+b2/2-r*4-b4],TUMBlack,'EdgeColor','none','FaceAlpha',transparency);
        patch([t_vec(1)+b3/2-0.3*b-b4+0.1 t_vec(1)+b3/2-0.3*b+0.1 t_vec(1)-b3/2-0.3*b+0.1 t_vec(1)-b3/2-0.3*b-b4+0.1],[z_NUM_Ref_Filmtime_2(1)+b2/2-r*4-b4 z_NUM_Ref_Filmtime_2(1)+b2/2+b4-r*4-b4 z_NUM_Ref_Filmtime_2(1)+b2/2+b4-r*4-b4 z_NUM_Ref_Filmtime_2(1)+b2/2-r*4-b4],TUMBlack,'EdgeColor','none','FaceAlpha',transparency);
        
        %PD-Regulator
        curr_distance_wall = abs(z_NUM_Ref_Filmtime_2(1)+b2/2-r*4)-abs(z_NUM_Ref_Filmtime(1)-r)-b2/2;
        curr_distance_wall_spring = curr_distance_wall + (r-r*cos(asin(hd/r))) + b2;
        
        % Damper
        patch([t_vec(1)+hd-h/4 t_vec(1)+hd+h/4 t_vec(1)+hd+h/4 t_vec(1)+hd-h/4],[z_NUM_Ref_Filmtime_2(1)+b2/2-r*4 z_NUM_Ref_Filmtime_2(1)+b2/2-r*4 z_NUM_Ref_Filmtime_2(1)+b2/2-r*4+1/2*curr_distance_wall z_NUM_Ref_Filmtime_2(1)+b2/2-r*4+1/2*curr_distance_wall],TUMBlack,'EdgeColor','none','FaceAlpha',transparency)
        damper_width = 7/8; %of hd
        inner_damper_width = 0.4;
        patch([t_vec(1)+hd-hd*damper_width t_vec(1)+hd+hd*damper_width t_vec(1)+hd+hd*damper_width t_vec(1)+hd-hd*damper_width],[z_NUM_Ref_Filmtime_2(1)+b2/2-r*4+1/2*curr_distance_wall z_NUM_Ref_Filmtime_2(1)+b2/2-r*4+1/2*curr_distance_wall z_NUM_Ref_Filmtime_2(1)+b2/2-r*4+0.6*curr_distance_wall z_NUM_Ref_Filmtime_2(1)+b2/2-r*4+0.6*curr_distance_wall],TUMBlack,'EdgeColor','none','FaceAlpha',transparency)
        patch([t_vec(1)+hd-hd*damper_width t_vec(1)+hd-hd*damper_width+h/2 t_vec(1)+hd-hd*damper_width+h/2 t_vec(1)+hd-hd*damper_width],[z_NUM_Ref_Filmtime_2(1)+b2/2-r*4+0.6*curr_distance_wall z_NUM_Ref_Filmtime_2(1)+b2/2-r*4+0.6*curr_distance_wall z_NUM_Ref_Filmtime_2(1)+b2/2-r*4+3/4*curr_distance_wall z_NUM_Ref_Filmtime_2(1)+b2/2-r*4+3/4*curr_distance_wall],TUMBlack,'EdgeColor','none','FaceAlpha',transparency)
        patch([t_vec(1)+hd+hd*damper_width-h/2 t_vec(1)+hd+hd*damper_width t_vec(1)+hd+hd*damper_width t_vec(1)+hd+hd*damper_width-h/2],[z_NUM_Ref_Filmtime_2(1)+b2/2-r*4+0.6*curr_distance_wall z_NUM_Ref_Filmtime_2(1)+b2/2-r*4+0.6*curr_distance_wall z_NUM_Ref_Filmtime_2(1)+b2/2-r*4+3/4*curr_distance_wall z_NUM_Ref_Filmtime_2(1)+b2/2-r*4+3/4*curr_distance_wall],TUMBlack,'EdgeColor','none','FaceAlpha',transparency)  
        patch([t_vec(1)+hd-hd*inner_damper_width t_vec(1)+hd+hd*inner_damper_width t_vec(1)+hd+hd*inner_damper_width t_vec(1)+hd-hd*inner_damper_width],[z_NUM_Ref_Filmtime_2(1)+b2/2-r*4+2/3*curr_distance_wall z_NUM_Ref_Filmtime_2(1)+b2/2-r*4+2/3*curr_distance_wall z_NUM_Ref_Filmtime_2(1)+b2/2-r*4+3/4*curr_distance_wall z_NUM_Ref_Filmtime_2(1)+b2/2-r*4+3/4*curr_distance_wall],TUMBlack,'EdgeColor','none','FaceAlpha',transparency)
        patch([t_vec(1)+hd-h/4 t_vec(1)+hd+h/4 t_vec(1)+hd+h/4 t_vec(1)+hd-h/4],[z_NUM_Ref_Filmtime_2(1)+b2/2-r*4+3/4*curr_distance_wall z_NUM_Ref_Filmtime_2(1)+b2/2-r*4+3/4*curr_distance_wall z_NUM_Ref_Filmtime(1) z_NUM_Ref_Filmtime(1)],TUMBlack,'EdgeColor','none','FaceAlpha',transparency)
        
        % Feder Kp
        patch([t_vec(1)-hd-h/2 t_vec(1)-hd+h/2 t_vec(1)-hd+h/2 t_vec(1)-hd-h/2],            [z_NUM_Ref_Filmtime_2(1)+b2/2-4*r                               z_NUM_Ref_Filmtime_2(1)+b2/2-4*r                                z_NUM_Ref_Filmtime_2(1)+b2/2-4*r+0.1*curr_distance_wall_spring  z_NUM_Ref_Filmtime_2(1)+b2/2-4*r+0.1*curr_distance_wall_spring],TUMBlack,'EdgeColor','none','FaceAlpha',transparency);
        patch([t_vec(1)-hd+h/2 t_vec(1)-hd+h/2+b6 t_vec(1)-hd-h/2+b6 t_vec(1)-hd-h/2],      [z_NUM_Ref_Filmtime_2(1)+b2/2-4*r+0.1*curr_distance_wall_spring z_NUM_Ref_Filmtime_2(1)+b2/2-4*r+0.2*curr_distance_wall_spring  z_NUM_Ref_Filmtime_2(1)+b2/2-4*r+0.2*curr_distance_wall_spring  z_NUM_Ref_Filmtime_2(1)+b2/2-4*r+0.1*curr_distance_wall_spring],TUMBlack,'EdgeColor','none','FaceAlpha',transparency);
        patch([t_vec(1)-hd+h/2+b6 t_vec(1)-hd+h/2-b6 t_vec(1)-hd-h/2-b6 t_vec(1)-hd-h/2+b6],[z_NUM_Ref_Filmtime_2(1)+b2/2-4*r+0.2*curr_distance_wall_spring z_NUM_Ref_Filmtime_2(1)+b2/2-4*r+0.4*curr_distance_wall_spring z_NUM_Ref_Filmtime_2(1)+b2/2-4*r+0.4*curr_distance_wall_spring  z_NUM_Ref_Filmtime_2(1)+b2/2-4*r+0.2*curr_distance_wall_spring],TUMBlack,'EdgeColor','none','FaceAlpha',transparency);
        patch([t_vec(1)-hd+h/2-b6 t_vec(1)-hd+h/2+b6 t_vec(1)-hd-h/2+b6 t_vec(1)-hd-h/2-b6],[z_NUM_Ref_Filmtime_2(1)+b2/2-4*r+0.4*curr_distance_wall_spring z_NUM_Ref_Filmtime_2(1)+b2/2-4*r+0.6*curr_distance_wall_spring z_NUM_Ref_Filmtime_2(1)+b2/2-4*r+0.6*curr_distance_wall_spring  z_NUM_Ref_Filmtime_2(1)+b2/2-4*r+0.4*curr_distance_wall_spring],TUMBlack,'EdgeColor','none','FaceAlpha',transparency);
        patch([t_vec(1)-hd+h/2+b6 t_vec(1)-hd+h/2-b6 t_vec(1)-hd-h/2-b6 t_vec(1)-hd-h/2+b6],[z_NUM_Ref_Filmtime_2(1)+b2/2-4*r+0.6*curr_distance_wall_spring z_NUM_Ref_Filmtime_2(1)+b2/2-4*r+0.8*curr_distance_wall_spring z_NUM_Ref_Filmtime_2(1)+b2/2-4*r+0.8*curr_distance_wall_spring  z_NUM_Ref_Filmtime_2(1)+b2/2-4*r+0.6*curr_distance_wall_spring],TUMBlack,'EdgeColor','none','FaceAlpha',transparency);
        patch([t_vec(1)-hd+h/2-b6 t_vec(1)-hd+h/2    t_vec(1)-hd-h/2    t_vec(1)-hd-h/2-b6],[z_NUM_Ref_Filmtime_2(1)+b2/2-4*r+0.8*curr_distance_wall_spring z_NUM_Ref_Filmtime_2(1)+b2/2-4*r+0.9*curr_distance_wall_spring z_NUM_Ref_Filmtime_2(1)+b2/2-4*r+0.9*curr_distance_wall_spring  z_NUM_Ref_Filmtime_2(1)+b2/2-4*r+0.8*curr_distance_wall_spring],TUMBlack,'EdgeColor','none','FaceAlpha',transparency);
        patch([t_vec(1)-hd+h/2    t_vec(1)-hd+h/2    t_vec(1)-hd-h/2    t_vec(1)-hd-h/2],   [z_NUM_Ref_Filmtime_2(1)+b2/2-4*r+0.9*curr_distance_wall_spring z_NUM_Ref_Filmtime_2(1)+b2/2-4*r+curr_distance_wall_spring     z_NUM_Ref_Filmtime_2(1)+b2/2-4*r+curr_distance_wall_spring      z_NUM_Ref_Filmtime_2(1)+b2/2-4*r+0.9*curr_distance_wall_spring],TUMBlack,'EdgeColor','none','FaceAlpha',transparency);

        %Ground at 0
        patch([t_vec(1)-1.5,          t_vec(1)+1.5,          t_vec(1)+1.5,          t_vec(1)-1.5],[0 0 0.15 0.15],TUMBlack,'EdgeColor','none','FaceAlpha',transparency)
        patch([t_vec(1)-0.075-0.1     t_vec(1)+0.075-0.1     t_vec(1)+0.5+0.075-0.1 t_vec(1)+0.5-0.075-0.1],[0.15 0.15 0.65 0.65],TUMBlack,'EdgeColor','none','FaceAlpha',transparency)
        patch([t_vec(1)-0.075-0.5-0.1 t_vec(1)+0.075-0.5-0.1 t_vec(1)+0.075-0.1     t_vec(1)-0.075-0.1],[0.15 0.15 0.65 0.65],TUMBlack,'EdgeColor','none','FaceAlpha',transparency)
        patch([t_vec(1)-0.075+0.5-0.1 t_vec(1)+0.075+0.5-0.1 t_vec(1)+1+0.075-0.1     t_vec(1)+1-0.075-0.1],[0.15 0.15 0.65 0.65],TUMBlack,'EdgeColor','none','FaceAlpha',transparency)
        patch([t_vec(1)-0.075-1-0.1   t_vec(1)+0.075-1-0.1   t_vec(1)+0.075-0.1-0.5 t_vec(1)-0.075-0.1-0.5],[0.15 0.15 0.65 0.65],TUMBlack,'EdgeColor','none','FaceAlpha',transparency)
        patch([t_vec(1)-0.075+1-0.1   t_vec(1)+0.075+1-0.1   t_vec(1)+1.5+0.075-0.1 t_vec(1)+1.5-0.075-0.1],[0.15 0.15 0.65 0.65],TUMBlack,'EdgeColor','none','FaceAlpha',transparency)
        

        xlabel('Time / s','Interpreter','Latex')
        ylabel('Displacement / mm','Interpreter','Latex')
        xticks([0 1 2 3 4])
        set(gca,'Fontsize',18)
        ylim([-12 1])
        xlim([-1.5 5.5])
        title('Iteration 1')
        
        subplot(1,2,2);     %Last Iteration
        hold on
        syms x y
        
        plot(t_vec(1:idx),z_NUM_Last_Filmtime(1:idx)+r,'Color',[0 0.4 0.74]) 
           
        % Numeical Mass Circle
        draw_circle(t_vec(idx),z_NUM_Last_Filmtime(idx),r,TUMBlue,1);
        
        axis equal

        % Experimental Mass Circle
        draw_circle(t_vec(idx), z_EXP_Last_Filmtime(idx),r,TUMGreen,1);
              
        % spring kE
        curr_distance = abs(z_NUM_Last_Filmtime(idx)+r)-abs(z_EXP_Last_Filmtime(idx)-r);
        patch([t_vec(idx)-h/2 t_vec(idx)+h/2 t_vec(idx)+h/2 t_vec(idx)-h/2],            [z_NUM_Last_Filmtime(idx)+r z_NUM_Last_Filmtime(idx)+r z_NUM_Last_Filmtime(idx)+r+ 0.1*curr_distance z_NUM_Last_Filmtime(idx)+r+ 0.1*curr_distance],TUMGreen,'EdgeColor','none');
        patch([t_vec(idx)+h/2 t_vec(idx)+h/2+b5 t_vec(idx)-h/2+b5 t_vec(idx)-h/2],      [z_NUM_Last_Filmtime(idx)+r+0.1*curr_distance z_NUM_Last_Filmtime(idx)+r+0.15*curr_distance z_NUM_Last_Filmtime(idx)+r+0.15*curr_distance z_NUM_Last_Filmtime(idx)+r+0.1*curr_distance],TUMGreen,'EdgeColor','none');
        patch([t_vec(idx)+h/2+b5 t_vec(idx)+h/2-b5 t_vec(idx)-h/2-b5 t_vec(idx)-h/2+b5],[z_NUM_Last_Filmtime(idx)+r+0.15*curr_distance z_NUM_Last_Filmtime(idx)+r+0.2*curr_distance z_NUM_Last_Filmtime(idx)+r+0.2*curr_distance z_NUM_Last_Filmtime(idx)+r+0.15*curr_distance],TUMGreen,'EdgeColor','none');
        patch([t_vec(idx)+h/2-b5 t_vec(idx)+h/2+b5 t_vec(idx)-h/2+b5 t_vec(idx)-h/2-b5],[z_NUM_Last_Filmtime(idx)+r+0.2*curr_distance z_NUM_Last_Filmtime(idx)+r+0.25*curr_distance z_NUM_Last_Filmtime(idx)+r+0.25*curr_distance z_NUM_Last_Filmtime(idx)+r+0.2*curr_distance],TUMGreen,'EdgeColor','none');
        patch([t_vec(idx)+h/2+b5 t_vec(idx)+h/2-b5 t_vec(idx)-h/2-b5 t_vec(idx)-h/2+b5],[z_NUM_Last_Filmtime(idx)+r+0.25*curr_distance z_NUM_Last_Filmtime(idx)+r+0.3*curr_distance z_NUM_Last_Filmtime(idx)+r+0.3*curr_distance z_NUM_Last_Filmtime(idx)+r+0.25*curr_distance],TUMGreen,'EdgeColor','none');      
        patch([t_vec(idx)+h/2-b5 t_vec(idx)+h/2+b5 t_vec(idx)-h/2+b5 t_vec(idx)-h/2-b5],[z_NUM_Last_Filmtime(idx)+r+0.3*curr_distance z_NUM_Last_Filmtime(idx)+r+0.35*curr_distance z_NUM_Last_Filmtime(idx)+r+0.35*curr_distance z_NUM_Last_Filmtime(idx)+r+0.3*curr_distance],TUMGreen,'EdgeColor','none');
        patch([t_vec(idx)+h/2+b5 t_vec(idx)+h/2-b5 t_vec(idx)-h/2-b5 t_vec(idx)-h/2+b5],[z_NUM_Last_Filmtime(idx)+r+0.35*curr_distance z_NUM_Last_Filmtime(idx)+r+0.4*curr_distance z_NUM_Last_Filmtime(idx)+r+0.4*curr_distance z_NUM_Last_Filmtime(idx)+r+0.35*curr_distance],TUMGreen,'EdgeColor','none');
        patch([t_vec(idx)+h/2-b5 t_vec(idx)+h/2+b5 t_vec(idx)-h/2+b5 t_vec(idx)-h/2-b5],[z_NUM_Last_Filmtime(idx)+r+0.4*curr_distance z_NUM_Last_Filmtime(idx)+r+0.45*curr_distance z_NUM_Last_Filmtime(idx)+r+0.45*curr_distance z_NUM_Last_Filmtime(idx)+r+0.4*curr_distance],TUMGreen,'EdgeColor','none');
        patch([t_vec(idx)+h/2+b5 t_vec(idx)+h/2-b5 t_vec(idx)-h/2-b5 t_vec(idx)-h/2+b5],[z_NUM_Last_Filmtime(idx)+r+0.45*curr_distance z_NUM_Last_Filmtime(idx)+r+0.5*curr_distance z_NUM_Last_Filmtime(idx)+r+0.5*curr_distance z_NUM_Last_Filmtime(idx)+r+0.45*curr_distance],TUMGreen,'EdgeColor','none'); 
        patch([t_vec(idx)+h/2-b5 t_vec(idx)+h/2+b5 t_vec(idx)-h/2+b5 t_vec(idx)-h/2-b5],[z_NUM_Last_Filmtime(idx)+r+0.5*curr_distance z_NUM_Last_Filmtime(idx)+r+0.55*curr_distance z_NUM_Last_Filmtime(idx)+r+0.55*curr_distance z_NUM_Last_Filmtime(idx)+r+0.5*curr_distance],TUMGreen,'EdgeColor','none');
        patch([t_vec(idx)+h/2+b5 t_vec(idx)+h/2-b5 t_vec(idx)-h/2-b5 t_vec(idx)-h/2+b5],[z_NUM_Last_Filmtime(idx)+r+0.55*curr_distance z_NUM_Last_Filmtime(idx)+r+0.6*curr_distance z_NUM_Last_Filmtime(idx)+r+0.6*curr_distance z_NUM_Last_Filmtime(idx)+r+0.55*curr_distance],TUMGreen,'EdgeColor','none');
        patch([t_vec(idx)+h/2-b5 t_vec(idx)+h/2+b5 t_vec(idx)-h/2+b5 t_vec(idx)-h/2-b5],[z_NUM_Last_Filmtime(idx)+r+0.6*curr_distance z_NUM_Last_Filmtime(idx)+r+0.65*curr_distance z_NUM_Last_Filmtime(idx)+r+0.65*curr_distance z_NUM_Last_Filmtime(idx)+r+0.6*curr_distance],TUMGreen,'EdgeColor','none');
        patch([t_vec(idx)+h/2+b5 t_vec(idx)+h/2-b5 t_vec(idx)-h/2-b5 t_vec(idx)-h/2+b5],[z_NUM_Last_Filmtime(idx)+r+0.65*curr_distance z_NUM_Last_Filmtime(idx)+r+0.7*curr_distance z_NUM_Last_Filmtime(idx)+r+0.7*curr_distance z_NUM_Last_Filmtime(idx)+r+0.65*curr_distance],TUMGreen,'EdgeColor','none');
        patch([t_vec(idx)+h/2-b5 t_vec(idx)+h/2+b5 t_vec(idx)-h/2+b5 t_vec(idx)-h/2-b5],[z_NUM_Last_Filmtime(idx)+r+0.7*curr_distance z_NUM_Last_Filmtime(idx)+r+0.75*curr_distance z_NUM_Last_Filmtime(idx)+r+0.75*curr_distance z_NUM_Last_Filmtime(idx)+r+0.7*curr_distance],TUMGreen,'EdgeColor','none');
        patch([t_vec(idx)+h/2+b5 t_vec(idx)+h/2-b5 t_vec(idx)-h/2-b5 t_vec(idx)-h/2+b5],[z_NUM_Last_Filmtime(idx)+r+0.75*curr_distance z_NUM_Last_Filmtime(idx)+r+0.8*curr_distance z_NUM_Last_Filmtime(idx)+r+0.8*curr_distance z_NUM_Last_Filmtime(idx)+r+0.75*curr_distance],TUMGreen,'EdgeColor','none');
        patch([t_vec(idx)+h/2-b5 t_vec(idx)+h/2+b5 t_vec(idx)-h/2+b5 t_vec(idx)-h/2-b5],[z_NUM_Last_Filmtime(idx)+r+0.8*curr_distance z_NUM_Last_Filmtime(idx)+r+0.85*curr_distance z_NUM_Last_Filmtime(idx)+r+0.85*curr_distance z_NUM_Last_Filmtime(idx)+r+0.8*curr_distance],TUMGreen,'EdgeColor','none');
        patch([t_vec(idx)+h/2+b5 t_vec(idx)+h/2-b5 t_vec(idx)-h/2-b5 t_vec(idx)-h/2+b5],[z_NUM_Last_Filmtime(idx)+r+0.85*curr_distance z_NUM_Last_Filmtime(idx)+r+0.9*curr_distance z_NUM_Last_Filmtime(idx)+r+0.9*curr_distance z_NUM_Last_Filmtime(idx)+r+0.85*curr_distance],TUMGreen,'EdgeColor','none');
        patch([t_vec(idx)+h/2-b5 t_vec(idx)+h/2    t_vec(idx)-h/2    t_vec(idx)-h/2-b5],[z_NUM_Last_Filmtime(idx)+r+0.9*curr_distance z_NUM_Last_Filmtime(idx)+r+0.95*curr_distance z_NUM_Last_Filmtime(idx)+r+0.95*curr_distance z_NUM_Last_Filmtime(idx)+r+0.9*curr_distance],TUMGreen,'EdgeColor','none');
        patch([t_vec(idx)+h/2    t_vec(idx)+h/2    t_vec(idx)-h/2    t_vec(idx)-h/2],   [z_NUM_Last_Filmtime(idx)+r+0.95*curr_distance z_NUM_Last_Filmtime(idx)+r+curr_distance z_NUM_Last_Filmtime(idx)+r+curr_distance z_NUM_Last_Filmtime(idx)+r+0.95*curr_distance],TUMGreen,'EdgeColor','none');
       
        %wall
        patch([t_vec(idx)+b/2 t_vec(idx)+b/2 t_vec(idx)-b/2 t_vec(idx)-b/2],[z_des_Last_Filmtime(idx)-b2/2-r*4 z_des_Last_Filmtime(idx)+b2/2-r*4 z_des_Last_Filmtime(idx)+b2/2-r*4 z_des_Last_Filmtime(idx)-b2/2-r*4],TUMBlue,'EdgeColor','none'); 
       
        %wall diagonals
        patch([t_vec(idx)+b3/2-b4+0.1 t_vec(idx)+b3/2+0.1 t_vec(idx)-b3/2+0.1 t_vec(idx)-b3/2-b4+0.1],[z_des_Last_Filmtime(idx)+b2/2-r*4-b4 z_des_Last_Filmtime(idx)+b2/2+b4-r*4-b4 z_des_Last_Filmtime(idx)+b2/2+b4-r*4-b4 z_des_Last_Filmtime(idx)+b2/2-r*4-b4],TUMBlue,'EdgeColor','none');     
        patch([t_vec(idx)+b3/2+0.3*b-b4+0.1 t_vec(idx)+b3/2+0.3*b+0.1 t_vec(idx)-b3/2+0.3*b+0.1 t_vec(idx)-b3/2+0.3*b-b4+0.1],[z_des_Last_Filmtime(idx)+b2/2-r*4-b4 z_des_Last_Filmtime(idx)+b2/2+b4-r*4-b4 z_des_Last_Filmtime(idx)+b2/2+b4-r*4-b4 z_des_Last_Filmtime(idx)+b2/2-r*4-b4],TUMBlue,'EdgeColor','none');
        patch([t_vec(idx)+b3/2-0.3*b-b4+0.1 t_vec(idx)+b3/2-0.3*b+0.1 t_vec(idx)-b3/2-0.3*b+0.1 t_vec(idx)-b3/2-0.3*b-b4+0.1],[z_des_Last_Filmtime(idx)+b2/2-r*4-b4 z_des_Last_Filmtime(idx)+b2/2+b4-r*4-b4 z_des_Last_Filmtime(idx)+b2/2+b4-r*4-b4 z_des_Last_Filmtime(idx)+b2/2-r*4-b4],TUMBlue,'EdgeColor','none');
        
        %PD-Regulator
        curr_distance_wall = abs(z_des_Last_Filmtime(idx)+b2/2-r*4)-abs(z_NUM_Last_Filmtime(idx)-r)-b2/2;
        curr_distance_wall_spring = curr_distance_wall + (r-r*cos(asin(hd/r))) + b2;
        
        % Damper
        patch([t_vec(idx)+hd-h/4 t_vec(idx)+hd+h/4 t_vec(idx)+hd+h/4 t_vec(idx)+hd-h/4],[z_des_Last_Filmtime(idx)+b2/2-r*4 z_des_Last_Filmtime(idx)+b2/2-r*4 z_des_Last_Filmtime(idx)+b2/2-r*4+1/2*curr_distance_wall z_des_Last_Filmtime(idx)+b2/2-r*4+1/2*curr_distance_wall],TUMBlue,'EdgeColor','none')
        damper_width = 7/8; %of hd
        inner_damper_width = 0.4;
        patch([t_vec(idx)+hd-hd*damper_width t_vec(idx)+hd+hd*damper_width t_vec(idx)+hd+hd*damper_width t_vec(idx)+hd-hd*damper_width],[z_des_Last_Filmtime(idx)+b2/2-r*4+1/2*curr_distance_wall z_des_Last_Filmtime(idx)+b2/2-r*4+1/2*curr_distance_wall z_des_Last_Filmtime(idx)+b2/2-r*4+0.6*curr_distance_wall z_des_Last_Filmtime(idx)+b2/2-r*4+0.6*curr_distance_wall],TUMBlue,'EdgeColor','none')
        patch([t_vec(idx)+hd-hd*damper_width t_vec(idx)+hd-hd*damper_width+h/2 t_vec(idx)+hd-hd*damper_width+h/2 t_vec(idx)+hd-hd*damper_width],[z_des_Last_Filmtime(idx)+b2/2-r*4+0.6*curr_distance_wall z_des_Last_Filmtime(idx)+b2/2-r*4+0.6*curr_distance_wall z_des_Last_Filmtime(idx)+b2/2-r*4+3/4*curr_distance_wall z_des_Last_Filmtime(idx)+b2/2-r*4+3/4*curr_distance_wall],TUMBlue,'EdgeColor','none')
        patch([t_vec(idx)+hd+hd*damper_width-h/2 t_vec(idx)+hd+hd*damper_width t_vec(idx)+hd+hd*damper_width t_vec(idx)+hd+hd*damper_width-h/2],[z_des_Last_Filmtime(idx)+b2/2-r*4+0.6*curr_distance_wall z_des_Last_Filmtime(idx)+b2/2-r*4+0.6*curr_distance_wall z_des_Last_Filmtime(idx)+b2/2-r*4+3/4*curr_distance_wall z_des_Last_Filmtime(idx)+b2/2-r*4+3/4*curr_distance_wall],TUMBlue,'EdgeColor','none')  
        patch([t_vec(idx)+hd-hd*inner_damper_width t_vec(idx)+hd+hd*inner_damper_width t_vec(idx)+hd+hd*inner_damper_width t_vec(idx)+hd-hd*inner_damper_width],[z_des_Last_Filmtime(idx)+b2/2-r*4+2/3*curr_distance_wall z_des_Last_Filmtime(idx)+b2/2-r*4+2/3*curr_distance_wall z_des_Last_Filmtime(idx)+b2/2-r*4+3/4*curr_distance_wall z_des_Last_Filmtime(idx)+b2/2-r*4+3/4*curr_distance_wall],TUMBlue,'EdgeColor','none')
        patch([t_vec(idx)+hd-h/4 t_vec(idx)+hd+h/4 t_vec(idx)+hd+h/4 t_vec(idx)+hd-h/4],[z_des_Last_Filmtime(idx)+b2/2-r*4+3/4*curr_distance_wall z_des_Last_Filmtime(idx)+b2/2-r*4+3/4*curr_distance_wall z_NUM_Last_Filmtime(idx) z_NUM_Last_Filmtime(idx)],TUMBlue,'EdgeColor','none')
        
        % Feder Kp
        patch([t_vec(idx)-hd-h/2 t_vec(idx)-hd+h/2 t_vec(idx)-hd+h/2 t_vec(idx)-hd-h/2],            [z_des_Last_Filmtime(idx)+b2/2-4*r                               z_des_Last_Filmtime(idx)+b2/2-4*r                                z_des_Last_Filmtime(idx)+b2/2-4*r+0.1*curr_distance_wall_spring  z_des_Last_Filmtime(idx)+b2/2-4*r+0.1*curr_distance_wall_spring],TUMBlue,'EdgeColor','none');
        patch([t_vec(idx)-hd+h/2 t_vec(idx)-hd+h/2+b6 t_vec(idx)-hd-h/2+b6 t_vec(idx)-hd-h/2],      [z_des_Last_Filmtime(idx)+b2/2-4*r+0.1*curr_distance_wall_spring z_des_Last_Filmtime(idx)+b2/2-4*r+0.2*curr_distance_wall_spring  z_des_Last_Filmtime(idx)+b2/2-4*r+0.2*curr_distance_wall_spring  z_des_Last_Filmtime(idx)+b2/2-4*r+0.1*curr_distance_wall_spring],TUMBlue,'EdgeColor','none');
        patch([t_vec(idx)-hd+h/2+b6 t_vec(idx)-hd+h/2-b6 t_vec(idx)-hd-h/2-b6 t_vec(idx)-hd-h/2+b6],[z_des_Last_Filmtime(idx)+b2/2-4*r+0.2*curr_distance_wall_spring z_des_Last_Filmtime(idx)+b2/2-4*r+0.4*curr_distance_wall_spring z_des_Last_Filmtime(idx)+b2/2-4*r+0.4*curr_distance_wall_spring  z_des_Last_Filmtime(idx)+b2/2-4*r+0.2*curr_distance_wall_spring],TUMBlue,'EdgeColor','none');
        patch([t_vec(idx)-hd+h/2-b6 t_vec(idx)-hd+h/2+b6 t_vec(idx)-hd-h/2+b6 t_vec(idx)-hd-h/2-b6],[z_des_Last_Filmtime(idx)+b2/2-4*r+0.4*curr_distance_wall_spring z_des_Last_Filmtime(idx)+b2/2-4*r+0.6*curr_distance_wall_spring z_des_Last_Filmtime(idx)+b2/2-4*r+0.6*curr_distance_wall_spring  z_des_Last_Filmtime(idx)+b2/2-4*r+0.4*curr_distance_wall_spring],TUMBlue,'EdgeColor','none');
        patch([t_vec(idx)-hd+h/2+b6 t_vec(idx)-hd+h/2-b6 t_vec(idx)-hd-h/2-b6 t_vec(idx)-hd-h/2+b6],[z_des_Last_Filmtime(idx)+b2/2-4*r+0.6*curr_distance_wall_spring z_des_Last_Filmtime(idx)+b2/2-4*r+0.8*curr_distance_wall_spring z_des_Last_Filmtime(idx)+b2/2-4*r+0.8*curr_distance_wall_spring  z_des_Last_Filmtime(idx)+b2/2-4*r+0.6*curr_distance_wall_spring],TUMBlue,'EdgeColor','none');
        patch([t_vec(idx)-hd+h/2-b6 t_vec(idx)-hd+h/2    t_vec(idx)-hd-h/2    t_vec(idx)-hd-h/2-b6],[z_des_Last_Filmtime(idx)+b2/2-4*r+0.8*curr_distance_wall_spring z_des_Last_Filmtime(idx)+b2/2-4*r+0.9*curr_distance_wall_spring z_des_Last_Filmtime(idx)+b2/2-4*r+0.9*curr_distance_wall_spring  z_des_Last_Filmtime(idx)+b2/2-4*r+0.8*curr_distance_wall_spring],TUMBlue,'EdgeColor','none');
        patch([t_vec(idx)-hd+h/2    t_vec(idx)-hd+h/2    t_vec(idx)-hd-h/2    t_vec(idx)-hd-h/2],   [z_des_Last_Filmtime(idx)+b2/2-4*r+0.9*curr_distance_wall_spring z_des_Last_Filmtime(idx)+b2/2-4*r+curr_distance_wall_spring     z_des_Last_Filmtime(idx)+b2/2-4*r+curr_distance_wall_spring      z_des_Last_Filmtime(idx)+b2/2-4*r+0.9*curr_distance_wall_spring],TUMBlue,'EdgeColor','none');

        %Ground at 0
        patch([t_vec(idx)-1.5,          t_vec(idx)+1.5,          t_vec(idx)+1.5,          t_vec(idx)-1.5],[0 0 0.15 0.15],TUMGreen,'EdgeColor','none')
        patch([t_vec(idx)-0.075-0.1     t_vec(idx)+0.075-0.1     t_vec(idx)+0.5+0.075-0.1 t_vec(idx)+0.5-0.075-0.1],[0.15 0.15 0.65 0.65],TUMGreen,'EdgeColor','none')
        patch([t_vec(idx)-0.075-0.5-0.1 t_vec(idx)+0.075-0.5-0.1 t_vec(idx)+0.075-0.1     t_vec(idx)-0.075-0.1],[0.15 0.15 0.65 0.65],TUMGreen,'EdgeColor','none')
        patch([t_vec(idx)-0.075+0.5-0.1 t_vec(idx)+0.075+0.5-0.1 t_vec(idx)+1+0.075-0.1     t_vec(idx)+1-0.075-0.1],[0.15 0.15 0.65 0.65],TUMGreen,'EdgeColor','none')
        patch([t_vec(idx)-0.075-1-0.1   t_vec(idx)+0.075-1-0.1   t_vec(idx)+0.075-0.1-0.5 t_vec(idx)-0.075-0.1-0.5],[0.15 0.15 0.65 0.65],TUMGreen,'EdgeColor','none')
        patch([t_vec(idx)-0.075+1-0.1   t_vec(idx)+0.075+1-0.1   t_vec(idx)+1.5+0.075-0.1 t_vec(idx)+1.5-0.075-0.1],[0.15 0.15 0.65 0.65],TUMGreen,'EdgeColor','none')
        
        %Stationary Ref
        transparency = 1;
        draw_circle(t_vec(1),z_NUM_Ref_Filmtime(1),r,TUMBlack,transparency);
        draw_circle(t_vec(1), z_EXP_Ref_Filmtime(1),r,TUMBlack,transparency);
        axis equal
    
        % spring kE
        curr_distance = abs(z_NUM_Ref_Filmtime(1)+r)-abs(z_EXP_Ref_Filmtime(1)-r);
        patch([t_vec(1)-h/2 t_vec(1)+h/2 t_vec(1)+h/2 t_vec(1)-h/2],            [z_NUM_Ref_Filmtime(1)+r z_NUM_Ref_Filmtime(1)+r z_NUM_Ref_Filmtime(1)+r+ 0.1*curr_distance z_NUM_Ref_Filmtime(1)+r+ 0.1*curr_distance],TUMBlack,'EdgeColor','none','FaceAlpha',transparency);
        patch([t_vec(1)+h/2 t_vec(1)+h/2+b5 t_vec(1)-h/2+b5 t_vec(1)-h/2],      [z_NUM_Ref_Filmtime(1)+r+0.1*curr_distance z_NUM_Ref_Filmtime(1)+r+0.15*curr_distance z_NUM_Ref_Filmtime(1)+r+0.15*curr_distance z_NUM_Ref_Filmtime(1)+r+0.1*curr_distance],TUMBlack,'EdgeColor','none','FaceAlpha',transparency);
        patch([t_vec(1)+h/2+b5 t_vec(1)+h/2-b5 t_vec(1)-h/2-b5 t_vec(1)-h/2+b5],[z_NUM_Ref_Filmtime(1)+r+0.15*curr_distance z_NUM_Ref_Filmtime(1)+r+0.2*curr_distance z_NUM_Ref_Filmtime(1)+r+0.2*curr_distance z_NUM_Ref_Filmtime(1)+r+0.15*curr_distance],TUMBlack,'EdgeColor','none','FaceAlpha',transparency);
        patch([t_vec(1)+h/2-b5 t_vec(1)+h/2+b5 t_vec(1)-h/2+b5 t_vec(1)-h/2-b5],[z_NUM_Ref_Filmtime(1)+r+0.2*curr_distance z_NUM_Ref_Filmtime(1)+r+0.25*curr_distance z_NUM_Ref_Filmtime(1)+r+0.25*curr_distance z_NUM_Ref_Filmtime(1)+r+0.2*curr_distance],TUMBlack,'EdgeColor','none','FaceAlpha',transparency);
        patch([t_vec(1)+h/2+b5 t_vec(1)+h/2-b5 t_vec(1)-h/2-b5 t_vec(1)-h/2+b5],[z_NUM_Ref_Filmtime(1)+r+0.25*curr_distance z_NUM_Ref_Filmtime(1)+r+0.3*curr_distance z_NUM_Ref_Filmtime(1)+r+0.3*curr_distance z_NUM_Ref_Filmtime(1)+r+0.25*curr_distance],TUMBlack,'EdgeColor','none','FaceAlpha',transparency);      
        patch([t_vec(1)+h/2-b5 t_vec(1)+h/2+b5 t_vec(1)-h/2+b5 t_vec(1)-h/2-b5],[z_NUM_Ref_Filmtime(1)+r+0.3*curr_distance z_NUM_Ref_Filmtime(1)+r+0.35*curr_distance z_NUM_Ref_Filmtime(1)+r+0.35*curr_distance z_NUM_Ref_Filmtime(1)+r+0.3*curr_distance],TUMBlack,'EdgeColor','none','FaceAlpha',transparency);
        patch([t_vec(1)+h/2+b5 t_vec(1)+h/2-b5 t_vec(1)-h/2-b5 t_vec(1)-h/2+b5],[z_NUM_Ref_Filmtime(1)+r+0.35*curr_distance z_NUM_Ref_Filmtime(1)+r+0.4*curr_distance z_NUM_Ref_Filmtime(1)+r+0.4*curr_distance z_NUM_Ref_Filmtime(1)+r+0.35*curr_distance],TUMBlack,'EdgeColor','none','FaceAlpha',transparency);
        patch([t_vec(1)+h/2-b5 t_vec(1)+h/2+b5 t_vec(1)-h/2+b5 t_vec(1)-h/2-b5],[z_NUM_Ref_Filmtime(1)+r+0.4*curr_distance z_NUM_Ref_Filmtime(1)+r+0.45*curr_distance z_NUM_Ref_Filmtime(1)+r+0.45*curr_distance z_NUM_Ref_Filmtime(1)+r+0.4*curr_distance],TUMBlack,'EdgeColor','none','FaceAlpha',transparency);
        patch([t_vec(1)+h/2+b5 t_vec(1)+h/2-b5 t_vec(1)-h/2-b5 t_vec(1)-h/2+b5],[z_NUM_Ref_Filmtime(1)+r+0.45*curr_distance z_NUM_Ref_Filmtime(1)+r+0.5*curr_distance z_NUM_Ref_Filmtime(1)+r+0.5*curr_distance z_NUM_Ref_Filmtime(1)+r+0.45*curr_distance],TUMBlack,'EdgeColor','none','FaceAlpha',transparency); 
        patch([t_vec(1)+h/2-b5 t_vec(1)+h/2+b5 t_vec(1)-h/2+b5 t_vec(1)-h/2-b5],[z_NUM_Ref_Filmtime(1)+r+0.5*curr_distance z_NUM_Ref_Filmtime(1)+r+0.55*curr_distance z_NUM_Ref_Filmtime(1)+r+0.55*curr_distance z_NUM_Ref_Filmtime(1)+r+0.5*curr_distance],TUMBlack,'EdgeColor','none','FaceAlpha',transparency);
        patch([t_vec(1)+h/2+b5 t_vec(1)+h/2-b5 t_vec(1)-h/2-b5 t_vec(1)-h/2+b5],[z_NUM_Ref_Filmtime(1)+r+0.55*curr_distance z_NUM_Ref_Filmtime(1)+r+0.6*curr_distance z_NUM_Ref_Filmtime(1)+r+0.6*curr_distance z_NUM_Ref_Filmtime(1)+r+0.55*curr_distance],TUMBlack,'EdgeColor','none','FaceAlpha',transparency);
        patch([t_vec(1)+h/2-b5 t_vec(1)+h/2+b5 t_vec(1)-h/2+b5 t_vec(1)-h/2-b5],[z_NUM_Ref_Filmtime(1)+r+0.6*curr_distance z_NUM_Ref_Filmtime(1)+r+0.65*curr_distance z_NUM_Ref_Filmtime(1)+r+0.65*curr_distance z_NUM_Ref_Filmtime(1)+r+0.6*curr_distance],TUMBlack,'EdgeColor','none','FaceAlpha',transparency);
        patch([t_vec(1)+h/2+b5 t_vec(1)+h/2-b5 t_vec(1)-h/2-b5 t_vec(1)-h/2+b5],[z_NUM_Ref_Filmtime(1)+r+0.65*curr_distance z_NUM_Ref_Filmtime(1)+r+0.7*curr_distance z_NUM_Ref_Filmtime(1)+r+0.7*curr_distance z_NUM_Ref_Filmtime(1)+r+0.65*curr_distance],TUMBlack,'EdgeColor','none','FaceAlpha',transparency);
        patch([t_vec(1)+h/2-b5 t_vec(1)+h/2+b5 t_vec(1)-h/2+b5 t_vec(1)-h/2-b5],[z_NUM_Ref_Filmtime(1)+r+0.7*curr_distance z_NUM_Ref_Filmtime(1)+r+0.75*curr_distance z_NUM_Ref_Filmtime(1)+r+0.75*curr_distance z_NUM_Ref_Filmtime(1)+r+0.7*curr_distance],TUMBlack,'EdgeColor','none','FaceAlpha',transparency);
        patch([t_vec(1)+h/2+b5 t_vec(1)+h/2-b5 t_vec(1)-h/2-b5 t_vec(1)-h/2+b5],[z_NUM_Ref_Filmtime(1)+r+0.75*curr_distance z_NUM_Ref_Filmtime(1)+r+0.8*curr_distance z_NUM_Ref_Filmtime(1)+r+0.8*curr_distance z_NUM_Ref_Filmtime(1)+r+0.75*curr_distance],TUMBlack,'EdgeColor','none','FaceAlpha',transparency);
        patch([t_vec(1)+h/2-b5 t_vec(1)+h/2+b5 t_vec(1)-h/2+b5 t_vec(1)-h/2-b5],[z_NUM_Ref_Filmtime(1)+r+0.8*curr_distance z_NUM_Ref_Filmtime(1)+r+0.85*curr_distance z_NUM_Ref_Filmtime(1)+r+0.85*curr_distance z_NUM_Ref_Filmtime(1)+r+0.8*curr_distance],TUMBlack,'EdgeColor','none','FaceAlpha',transparency);
        patch([t_vec(1)+h/2+b5 t_vec(1)+h/2-b5 t_vec(1)-h/2-b5 t_vec(1)-h/2+b5],[z_NUM_Ref_Filmtime(1)+r+0.85*curr_distance z_NUM_Ref_Filmtime(1)+r+0.9*curr_distance z_NUM_Ref_Filmtime(1)+r+0.9*curr_distance z_NUM_Ref_Filmtime(1)+r+0.85*curr_distance],TUMBlack,'EdgeColor','none','FaceAlpha',transparency);
        patch([t_vec(1)+h/2-b5 t_vec(1)+h/2    t_vec(1)-h/2    t_vec(1)-h/2-b5],[z_NUM_Ref_Filmtime(1)+r+0.9*curr_distance z_NUM_Ref_Filmtime(1)+r+0.95*curr_distance z_NUM_Ref_Filmtime(1)+r+0.95*curr_distance z_NUM_Ref_Filmtime(1)+r+0.9*curr_distance],TUMBlack,'EdgeColor','none','FaceAlpha',transparency);
        patch([t_vec(1)+h/2    t_vec(1)+h/2    t_vec(1)-h/2    t_vec(1)-h/2],   [z_NUM_Ref_Filmtime(1)+r+0.95*curr_distance z_NUM_Ref_Filmtime(1)+r+curr_distance z_NUM_Ref_Filmtime(1)+r+curr_distance z_NUM_Ref_Filmtime(1)+r+0.95*curr_distance],TUMBlack,'EdgeColor','none','FaceAlpha',transparency);
        
        %wall
        patch([t_vec(1)+b/2 t_vec(1)+b/2 t_vec(1)-b/2 t_vec(1)-b/2],[z_NUM_Ref_Filmtime_2(1)-b2/2-r*4 z_NUM_Ref_Filmtime_2(1)+b2/2-r*4 z_NUM_Ref_Filmtime_2(1)+b2/2-r*4 z_NUM_Ref_Filmtime_2(1)-b2/2-r*4],TUMBlack,'EdgeColor','none','FaceAlpha',transparency); 
        
        %wall diagonals
        patch([t_vec(1)+b3/2-b4+0.1 t_vec(1)+b3/2+0.1 t_vec(1)-b3/2+0.1 t_vec(1)-b3/2-b4+0.1],[z_NUM_Ref_Filmtime_2(1)+b2/2-r*4-b4 z_NUM_Ref_Filmtime_2(1)+b2/2+b4-r*4-b4 z_NUM_Ref_Filmtime_2(1)+b2/2+b4-r*4-b4 z_NUM_Ref_Filmtime_2(1)+b2/2-r*4-b4],TUMBlack,'EdgeColor','none','FaceAlpha',transparency);     
        patch([t_vec(1)+b3/2+0.3*b-b4+0.1 t_vec(1)+b3/2+0.3*b+0.1 t_vec(1)-b3/2+0.3*b+0.1 t_vec(1)-b3/2+0.3*b-b4+0.1],[z_NUM_Ref_Filmtime_2(1)+b2/2-r*4-b4 z_NUM_Ref_Filmtime_2(1)+b2/2+b4-r*4-b4 z_NUM_Ref_Filmtime_2(1)+b2/2+b4-r*4-b4 z_NUM_Ref_Filmtime_2(1)+b2/2-r*4-b4],TUMBlack,'EdgeColor','none','FaceAlpha',transparency);
        patch([t_vec(1)+b3/2-0.3*b-b4+0.1 t_vec(1)+b3/2-0.3*b+0.1 t_vec(1)-b3/2-0.3*b+0.1 t_vec(1)-b3/2-0.3*b-b4+0.1],[z_NUM_Ref_Filmtime_2(1)+b2/2-r*4-b4 z_NUM_Ref_Filmtime_2(1)+b2/2+b4-r*4-b4 z_NUM_Ref_Filmtime_2(1)+b2/2+b4-r*4-b4 z_NUM_Ref_Filmtime_2(1)+b2/2-r*4-b4],TUMBlack,'EdgeColor','none','FaceAlpha',transparency);
        
        %PD-Regulator
        curr_distance_wall = abs(z_NUM_Ref_Filmtime_2(1)+b2/2-r*4)-abs(z_NUM_Ref_Filmtime(1)-r)-b2/2;
        curr_distance_wall_spring = curr_distance_wall + (r-r*cos(asin(hd/r))) + b2;
        
        % Dämpfer
        patch([t_vec(1)+hd-h/4 t_vec(1)+hd+h/4 t_vec(1)+hd+h/4 t_vec(1)+hd-h/4],[z_NUM_Ref_Filmtime_2(1)+b2/2-r*4 z_NUM_Ref_Filmtime_2(1)+b2/2-r*4 z_NUM_Ref_Filmtime_2(1)+b2/2-r*4+1/2*curr_distance_wall z_NUM_Ref_Filmtime_2(1)+b2/2-r*4+1/2*curr_distance_wall],TUMBlack,'EdgeColor','none','FaceAlpha',transparency)
        damper_width = 7/8; %of hd
        inner_damper_width = 0.4;
        patch([t_vec(1)+hd-hd*damper_width t_vec(1)+hd+hd*damper_width t_vec(1)+hd+hd*damper_width t_vec(1)+hd-hd*damper_width],[z_NUM_Ref_Filmtime_2(1)+b2/2-r*4+1/2*curr_distance_wall z_NUM_Ref_Filmtime_2(1)+b2/2-r*4+1/2*curr_distance_wall z_NUM_Ref_Filmtime_2(1)+b2/2-r*4+0.6*curr_distance_wall z_NUM_Ref_Filmtime_2(1)+b2/2-r*4+0.6*curr_distance_wall],TUMBlack,'EdgeColor','none','FaceAlpha',transparency)
        patch([t_vec(1)+hd-hd*damper_width t_vec(1)+hd-hd*damper_width+h/2 t_vec(1)+hd-hd*damper_width+h/2 t_vec(1)+hd-hd*damper_width],[z_NUM_Ref_Filmtime_2(1)+b2/2-r*4+0.6*curr_distance_wall z_NUM_Ref_Filmtime_2(1)+b2/2-r*4+0.6*curr_distance_wall z_NUM_Ref_Filmtime_2(1)+b2/2-r*4+3/4*curr_distance_wall z_NUM_Ref_Filmtime_2(1)+b2/2-r*4+3/4*curr_distance_wall],TUMBlack,'EdgeColor','none','FaceAlpha',transparency)
        patch([t_vec(1)+hd+hd*damper_width-h/2 t_vec(1)+hd+hd*damper_width t_vec(1)+hd+hd*damper_width t_vec(1)+hd+hd*damper_width-h/2],[z_NUM_Ref_Filmtime_2(1)+b2/2-r*4+0.6*curr_distance_wall z_NUM_Ref_Filmtime_2(1)+b2/2-r*4+0.6*curr_distance_wall z_NUM_Ref_Filmtime_2(1)+b2/2-r*4+3/4*curr_distance_wall z_NUM_Ref_Filmtime_2(1)+b2/2-r*4+3/4*curr_distance_wall],TUMBlack,'EdgeColor','none','FaceAlpha',transparency)  
        patch([t_vec(1)+hd-hd*inner_damper_width t_vec(1)+hd+hd*inner_damper_width t_vec(1)+hd+hd*inner_damper_width t_vec(1)+hd-hd*inner_damper_width],[z_NUM_Ref_Filmtime_2(1)+b2/2-r*4+2/3*curr_distance_wall z_NUM_Ref_Filmtime_2(1)+b2/2-r*4+2/3*curr_distance_wall z_NUM_Ref_Filmtime_2(1)+b2/2-r*4+3/4*curr_distance_wall z_NUM_Ref_Filmtime_2(1)+b2/2-r*4+3/4*curr_distance_wall],TUMBlack,'EdgeColor','none','FaceAlpha',transparency)
        patch([t_vec(1)+hd-h/4 t_vec(1)+hd+h/4 t_vec(1)+hd+h/4 t_vec(1)+hd-h/4],[z_NUM_Ref_Filmtime_2(1)+b2/2-r*4+3/4*curr_distance_wall z_NUM_Ref_Filmtime_2(1)+b2/2-r*4+3/4*curr_distance_wall z_NUM_Ref_Filmtime(1) z_NUM_Ref_Filmtime(1)],TUMBlack,'EdgeColor','none','FaceAlpha',transparency)
        
        % Feder Kp
        patch([t_vec(1)-hd-h/2 t_vec(1)-hd+h/2 t_vec(1)-hd+h/2 t_vec(1)-hd-h/2],            [z_NUM_Ref_Filmtime_2(1)+b2/2-4*r                               z_NUM_Ref_Filmtime_2(1)+b2/2-4*r                                z_NUM_Ref_Filmtime_2(1)+b2/2-4*r+0.1*curr_distance_wall_spring  z_NUM_Ref_Filmtime_2(1)+b2/2-4*r+0.1*curr_distance_wall_spring],TUMBlack,'EdgeColor','none','FaceAlpha',transparency);
        patch([t_vec(1)-hd+h/2 t_vec(1)-hd+h/2+b6 t_vec(1)-hd-h/2+b6 t_vec(1)-hd-h/2],      [z_NUM_Ref_Filmtime_2(1)+b2/2-4*r+0.1*curr_distance_wall_spring z_NUM_Ref_Filmtime_2(1)+b2/2-4*r+0.2*curr_distance_wall_spring  z_NUM_Ref_Filmtime_2(1)+b2/2-4*r+0.2*curr_distance_wall_spring  z_NUM_Ref_Filmtime_2(1)+b2/2-4*r+0.1*curr_distance_wall_spring],TUMBlack,'EdgeColor','none','FaceAlpha',transparency);
        patch([t_vec(1)-hd+h/2+b6 t_vec(1)-hd+h/2-b6 t_vec(1)-hd-h/2-b6 t_vec(1)-hd-h/2+b6],[z_NUM_Ref_Filmtime_2(1)+b2/2-4*r+0.2*curr_distance_wall_spring z_NUM_Ref_Filmtime_2(1)+b2/2-4*r+0.4*curr_distance_wall_spring z_NUM_Ref_Filmtime_2(1)+b2/2-4*r+0.4*curr_distance_wall_spring  z_NUM_Ref_Filmtime_2(1)+b2/2-4*r+0.2*curr_distance_wall_spring],TUMBlack,'EdgeColor','none','FaceAlpha',transparency);
        patch([t_vec(1)-hd+h/2-b6 t_vec(1)-hd+h/2+b6 t_vec(1)-hd-h/2+b6 t_vec(1)-hd-h/2-b6],[z_NUM_Ref_Filmtime_2(1)+b2/2-4*r+0.4*curr_distance_wall_spring z_NUM_Ref_Filmtime_2(1)+b2/2-4*r+0.6*curr_distance_wall_spring z_NUM_Ref_Filmtime_2(1)+b2/2-4*r+0.6*curr_distance_wall_spring  z_NUM_Ref_Filmtime_2(1)+b2/2-4*r+0.4*curr_distance_wall_spring],TUMBlack,'EdgeColor','none','FaceAlpha',transparency);
        patch([t_vec(1)-hd+h/2+b6 t_vec(1)-hd+h/2-b6 t_vec(1)-hd-h/2-b6 t_vec(1)-hd-h/2+b6],[z_NUM_Ref_Filmtime_2(1)+b2/2-4*r+0.6*curr_distance_wall_spring z_NUM_Ref_Filmtime_2(1)+b2/2-4*r+0.8*curr_distance_wall_spring z_NUM_Ref_Filmtime_2(1)+b2/2-4*r+0.8*curr_distance_wall_spring  z_NUM_Ref_Filmtime_2(1)+b2/2-4*r+0.6*curr_distance_wall_spring],TUMBlack,'EdgeColor','none','FaceAlpha',transparency);
        patch([t_vec(1)-hd+h/2-b6 t_vec(1)-hd+h/2    t_vec(1)-hd-h/2    t_vec(1)-hd-h/2-b6],[z_NUM_Ref_Filmtime_2(1)+b2/2-4*r+0.8*curr_distance_wall_spring z_NUM_Ref_Filmtime_2(1)+b2/2-4*r+0.9*curr_distance_wall_spring z_NUM_Ref_Filmtime_2(1)+b2/2-4*r+0.9*curr_distance_wall_spring  z_NUM_Ref_Filmtime_2(1)+b2/2-4*r+0.8*curr_distance_wall_spring],TUMBlack,'EdgeColor','none','FaceAlpha',transparency);
        patch([t_vec(1)-hd+h/2    t_vec(1)-hd+h/2    t_vec(1)-hd-h/2    t_vec(1)-hd-h/2],   [z_NUM_Ref_Filmtime_2(1)+b2/2-4*r+0.9*curr_distance_wall_spring z_NUM_Ref_Filmtime_2(1)+b2/2-4*r+curr_distance_wall_spring     z_NUM_Ref_Filmtime_2(1)+b2/2-4*r+curr_distance_wall_spring      z_NUM_Ref_Filmtime_2(1)+b2/2-4*r+0.9*curr_distance_wall_spring],TUMBlack,'EdgeColor','none','FaceAlpha',transparency);

        %Ground at 0
        patch([t_vec(1)-1.5,          t_vec(1)+1.5,          t_vec(1)+1.5,          t_vec(1)-1.5],[0 0 0.15 0.15],TUMBlack,'EdgeColor','none','FaceAlpha',transparency)
        patch([t_vec(1)-0.075-0.1     t_vec(1)+0.075-0.1     t_vec(1)+0.5+0.075-0.1 t_vec(1)+0.5-0.075-0.1],[0.15 0.15 0.65 0.65],TUMBlack,'EdgeColor','none','FaceAlpha',transparency)
        patch([t_vec(1)-0.075-0.5-0.1 t_vec(1)+0.075-0.5-0.1 t_vec(1)+0.075-0.1     t_vec(1)-0.075-0.1],[0.15 0.15 0.65 0.65],TUMBlack,'EdgeColor','none','FaceAlpha',transparency)
        patch([t_vec(1)-0.075+0.5-0.1 t_vec(1)+0.075+0.5-0.1 t_vec(1)+1+0.075-0.1     t_vec(1)+1-0.075-0.1],[0.15 0.15 0.65 0.65],TUMBlack,'EdgeColor','none','FaceAlpha',transparency)
        patch([t_vec(1)-0.075-1-0.1   t_vec(1)+0.075-1-0.1   t_vec(1)+0.075-0.1-0.5 t_vec(1)-0.075-0.1-0.5],[0.15 0.15 0.65 0.65],TUMBlack,'EdgeColor','none','FaceAlpha',transparency)
        patch([t_vec(1)-0.075+1-0.1   t_vec(1)+0.075+1-0.1   t_vec(1)+1.5+0.075-0.1 t_vec(1)+1.5-0.075-0.1],[0.15 0.15 0.65 0.65],TUMBlack,'EdgeColor','none','FaceAlpha',transparency)
        

        xlabel('Time / s','Interpreter','Latex')
        ylabel('Displacement / mm','Interpreter','Latex')
        xticks([0 1 2 3 4])
        set(gca,'Fontsize',18)
        title(lastIter)
        ylim([-12 1])
        xlim([-1.5 5.5])

        drawnow
        F=getframe(gcf);
        writeVideo(v,F);
    end
    
    %Reference
    for idx = 1:(length(t_vec)-1)
        
        clf
        figure(8)
        subplot(1,2,1)
        hold on
        syms x y

        %Stationary Actual First Iteration
        plot(t_vec(1:end-1),z_NUM_First_Filmtime(1:(length(t_vec)-1))+r,'Color',[0 0.4 0.74]) 
        draw_circle(t_vec(end-1),z_NUM_First_Filmtime(length(t_vec)-1),r,TUMBlue,1);
        axis equal
        draw_circle(t_vec(end-1), z_EXP_First_Filmtime(length(t_vec)-1),r,TUMGreen,1);
        
        curr_distance = abs(z_NUM_First_Filmtime(length(t_vec)-1)+r)-abs(z_EXP_First_Filmtime(end-1)-r);
        patch([t_vec(end-1)-h/2 t_vec(end-1)+h/2 t_vec(end-1)+h/2 t_vec(end-1)-h/2],            [z_NUM_First_Filmtime(length(t_vec)-1)+r z_NUM_First_Filmtime(length(t_vec)-1)+r z_NUM_First_Filmtime(length(t_vec)-1)+r+ 0.1*curr_distance z_NUM_First_Filmtime(length(t_vec)-1)+r+ 0.1*curr_distance],TUMGreen,'EdgeColor','none');
        patch([t_vec(end-1)+h/2 t_vec(end-1)+h/2+b5 t_vec(end-1)-h/2+b5 t_vec(end-1)-h/2],      [z_NUM_First_Filmtime(length(t_vec)-1)+r+0.1*curr_distance z_NUM_First_Filmtime(length(t_vec)-1)+r+0.15*curr_distance z_NUM_First_Filmtime(length(t_vec)-1)+r+0.15*curr_distance z_NUM_First_Filmtime(length(t_vec)-1)+r+0.1*curr_distance],TUMGreen,'EdgeColor','none');
        patch([t_vec(end-1)+h/2+b5 t_vec(end-1)+h/2-b5 t_vec(end-1)-h/2-b5 t_vec(end-1)-h/2+b5],[z_NUM_First_Filmtime(length(t_vec)-1)+r+0.15*curr_distance z_NUM_First_Filmtime(length(t_vec)-1)+r+0.2*curr_distance z_NUM_First_Filmtime(length(t_vec)-1)+r+0.2*curr_distance z_NUM_First_Filmtime(length(t_vec)-1)+r+0.15*curr_distance],TUMGreen,'EdgeColor','none');
        patch([t_vec(end-1)+h/2-b5 t_vec(end-1)+h/2+b5 t_vec(end-1)-h/2+b5 t_vec(end-1)-h/2-b5],[z_NUM_First_Filmtime(length(t_vec)-1)+r+0.2*curr_distance z_NUM_First_Filmtime(length(t_vec)-1)+r+0.25*curr_distance z_NUM_First_Filmtime(length(t_vec)-1)+r+0.25*curr_distance z_NUM_First_Filmtime(length(t_vec)-1)+r+0.2*curr_distance],TUMGreen,'EdgeColor','none');
        patch([t_vec(end-1)+h/2+b5 t_vec(end-1)+h/2-b5 t_vec(end-1)-h/2-b5 t_vec(end-1)-h/2+b5],[z_NUM_First_Filmtime(length(t_vec)-1)+r+0.25*curr_distance z_NUM_First_Filmtime(length(t_vec)-1)+r+0.3*curr_distance z_NUM_First_Filmtime(length(t_vec)-1)+r+0.3*curr_distance z_NUM_First_Filmtime(length(t_vec)-1)+r+0.25*curr_distance],TUMGreen,'EdgeColor','none');      
        patch([t_vec(end-1)+h/2-b5 t_vec(end-1)+h/2+b5 t_vec(end-1)-h/2+b5 t_vec(end-1)-h/2-b5],[z_NUM_First_Filmtime(length(t_vec)-1)+r+0.3*curr_distance z_NUM_First_Filmtime(length(t_vec)-1)+r+0.35*curr_distance z_NUM_First_Filmtime(length(t_vec)-1)+r+0.35*curr_distance z_NUM_First_Filmtime(length(t_vec)-1)+r+0.3*curr_distance],TUMGreen,'EdgeColor','none');
        patch([t_vec(end-1)+h/2+b5 t_vec(end-1)+h/2-b5 t_vec(end-1)-h/2-b5 t_vec(end-1)-h/2+b5],[z_NUM_First_Filmtime(length(t_vec)-1)+r+0.35*curr_distance z_NUM_First_Filmtime(length(t_vec)-1)+r+0.4*curr_distance z_NUM_First_Filmtime(length(t_vec)-1)+r+0.4*curr_distance z_NUM_First_Filmtime(length(t_vec)-1)+r+0.35*curr_distance],TUMGreen,'EdgeColor','none');
        patch([t_vec(end-1)+h/2-b5 t_vec(end-1)+h/2+b5 t_vec(end-1)-h/2+b5 t_vec(end-1)-h/2-b5],[z_NUM_First_Filmtime(length(t_vec)-1)+r+0.4*curr_distance z_NUM_First_Filmtime(length(t_vec)-1)+r+0.45*curr_distance z_NUM_First_Filmtime(length(t_vec)-1)+r+0.45*curr_distance z_NUM_First_Filmtime(length(t_vec)-1)+r+0.4*curr_distance],TUMGreen,'EdgeColor','none');
        patch([t_vec(end-1)+h/2+b5 t_vec(end-1)+h/2-b5 t_vec(end-1)-h/2-b5 t_vec(end-1)-h/2+b5],[z_NUM_First_Filmtime(length(t_vec)-1)+r+0.45*curr_distance z_NUM_First_Filmtime(length(t_vec)-1)+r+0.5*curr_distance z_NUM_First_Filmtime(length(t_vec)-1)+r+0.5*curr_distance z_NUM_First_Filmtime(length(t_vec)-1)+r+0.45*curr_distance],TUMGreen,'EdgeColor','none'); 
        patch([t_vec(end-1)+h/2-b5 t_vec(end-1)+h/2+b5 t_vec(end-1)-h/2+b5 t_vec(end-1)-h/2-b5],[z_NUM_First_Filmtime(length(t_vec)-1)+r+0.5*curr_distance z_NUM_First_Filmtime(length(t_vec)-1)+r+0.55*curr_distance z_NUM_First_Filmtime(length(t_vec)-1)+r+0.55*curr_distance z_NUM_First_Filmtime(length(t_vec)-1)+r+0.5*curr_distance],TUMGreen,'EdgeColor','none');
        patch([t_vec(end-1)+h/2+b5 t_vec(end-1)+h/2-b5 t_vec(end-1)-h/2-b5 t_vec(end-1)-h/2+b5],[z_NUM_First_Filmtime(length(t_vec)-1)+r+0.55*curr_distance z_NUM_First_Filmtime(length(t_vec)-1)+r+0.6*curr_distance z_NUM_First_Filmtime(length(t_vec)-1)+r+0.6*curr_distance z_NUM_First_Filmtime(length(t_vec)-1)+r+0.55*curr_distance],TUMGreen,'EdgeColor','none');
        patch([t_vec(end-1)+h/2-b5 t_vec(end-1)+h/2+b5 t_vec(end-1)-h/2+b5 t_vec(end-1)-h/2-b5],[z_NUM_First_Filmtime(length(t_vec)-1)+r+0.6*curr_distance z_NUM_First_Filmtime(length(t_vec)-1)+r+0.65*curr_distance z_NUM_First_Filmtime(length(t_vec)-1)+r+0.65*curr_distance z_NUM_First_Filmtime(length(t_vec)-1)+r+0.6*curr_distance],TUMGreen,'EdgeColor','none');
        patch([t_vec(end-1)+h/2+b5 t_vec(end-1)+h/2-b5 t_vec(end-1)-h/2-b5 t_vec(end-1)-h/2+b5],[z_NUM_First_Filmtime(length(t_vec)-1)+r+0.65*curr_distance z_NUM_First_Filmtime(length(t_vec)-1)+r+0.7*curr_distance z_NUM_First_Filmtime(length(t_vec)-1)+r+0.7*curr_distance z_NUM_First_Filmtime(length(t_vec)-1)+r+0.65*curr_distance],TUMGreen,'EdgeColor','none');
        patch([t_vec(end-1)+h/2-b5 t_vec(end-1)+h/2+b5 t_vec(end-1)-h/2+b5 t_vec(end-1)-h/2-b5],[z_NUM_First_Filmtime(length(t_vec)-1)+r+0.7*curr_distance z_NUM_First_Filmtime(length(t_vec)-1)+r+0.75*curr_distance z_NUM_First_Filmtime(length(t_vec)-1)+r+0.75*curr_distance z_NUM_First_Filmtime(length(t_vec)-1)+r+0.7*curr_distance],TUMGreen,'EdgeColor','none');
        patch([t_vec(end-1)+h/2+b5 t_vec(end-1)+h/2-b5 t_vec(end-1)-h/2-b5 t_vec(end-1)-h/2+b5],[z_NUM_First_Filmtime(length(t_vec)-1)+r+0.75*curr_distance z_NUM_First_Filmtime(length(t_vec)-1)+r+0.8*curr_distance z_NUM_First_Filmtime(length(t_vec)-1)+r+0.8*curr_distance z_NUM_First_Filmtime(length(t_vec)-1)+r+0.75*curr_distance],TUMGreen,'EdgeColor','none');
        patch([t_vec(end-1)+h/2-b5 t_vec(end-1)+h/2+b5 t_vec(end-1)-h/2+b5 t_vec(end-1)-h/2-b5],[z_NUM_First_Filmtime(length(t_vec)-1)+r+0.8*curr_distance z_NUM_First_Filmtime(length(t_vec)-1)+r+0.85*curr_distance z_NUM_First_Filmtime(length(t_vec)-1)+r+0.85*curr_distance z_NUM_First_Filmtime(length(t_vec)-1)+r+0.8*curr_distance],TUMGreen,'EdgeColor','none');
        patch([t_vec(end-1)+h/2+b5 t_vec(end-1)+h/2-b5 t_vec(end-1)-h/2-b5 t_vec(end-1)-h/2+b5],[z_NUM_First_Filmtime(length(t_vec)-1)+r+0.85*curr_distance z_NUM_First_Filmtime(length(t_vec)-1)+r+0.9*curr_distance z_NUM_First_Filmtime(length(t_vec)-1)+r+0.9*curr_distance z_NUM_First_Filmtime(length(t_vec)-1)+r+0.85*curr_distance],TUMGreen,'EdgeColor','none');
        patch([t_vec(end-1)+h/2-b5 t_vec(end-1)+h/2    t_vec(end-1)-h/2    t_vec(end-1)-h/2-b5],[z_NUM_First_Filmtime(length(t_vec)-1)+r+0.9*curr_distance z_NUM_First_Filmtime(length(t_vec)-1)+r+0.95*curr_distance z_NUM_First_Filmtime(length(t_vec)-1)+r+0.95*curr_distance z_NUM_First_Filmtime(length(t_vec)-1)+r+0.9*curr_distance],TUMGreen,'EdgeColor','none');
        patch([t_vec(end-1)+h/2    t_vec(end-1)+h/2    t_vec(end-1)-h/2    t_vec(end-1)-h/2],   [z_NUM_First_Filmtime(length(t_vec)-1)+r+0.95*curr_distance z_NUM_First_Filmtime(length(t_vec)-1)+r+curr_distance z_NUM_First_Filmtime(length(t_vec)-1)+r+curr_distance z_NUM_First_Filmtime(length(t_vec)-1)+r+0.95*curr_distance],TUMGreen,'EdgeColor','none');
        
        %wall
        patch([t_vec(end-1)+b/2 t_vec(end-1)+b/2 t_vec(end-1)-b/2 t_vec(end-1)-b/2],[z_des_First_Filmtime(length(t_vec)-1)-b2/2-r*4 z_des_First_Filmtime(length(t_vec)-1)+b2/2-r*4 z_des_First_Filmtime(length(t_vec)-1)+b2/2-r*4 z_des_First_Filmtime(length(t_vec)-1)-b2/2-r*4],TUMBlue,'EdgeColor','none'); 
        
        %wall diagonals
        patch([t_vec(end-1)+b3/2-b4+0.1 t_vec(end-1)+b3/2+0.1 t_vec(end-1)-b3/2+0.1 t_vec(end-1)-b3/2-b4+0.1],[z_des_First_Filmtime(length(t_vec)-1)+b2/2-r*4-b4 z_des_First_Filmtime(length(t_vec)-1)+b2/2+b4-r*4-b4 z_des_First_Filmtime(length(t_vec)-1)+b2/2+b4-r*4-b4 z_des_First_Filmtime(length(t_vec)-1)+b2/2-r*4-b4],TUMBlue,'EdgeColor','none');     
        patch([t_vec(end-1)+b3/2+0.3*b-b4+0.1 t_vec(end-1)+b3/2+0.3*b+0.1 t_vec(end-1)-b3/2+0.3*b+0.1 t_vec(end-1)-b3/2+0.3*b-b4+0.1],[z_des_First_Filmtime(length(t_vec)-1)+b2/2-r*4-b4 z_des_First_Filmtime(length(t_vec)-1)+b2/2+b4-r*4-b4 z_des_First_Filmtime(length(t_vec)-1)+b2/2+b4-r*4-b4 z_des_First_Filmtime(length(t_vec)-1)+b2/2-r*4-b4],TUMBlue,'EdgeColor','none');
        patch([t_vec(end-1)+b3/2-0.3*b-b4+0.1 t_vec(end-1)+b3/2-0.3*b+0.1 t_vec(end-1)-b3/2-0.3*b+0.1 t_vec(end-1)-b3/2-0.3*b-b4+0.1],[z_des_First_Filmtime(length(t_vec)-1)+b2/2-r*4-b4 z_des_First_Filmtime(length(t_vec)-1)+b2/2+b4-r*4-b4 z_des_First_Filmtime(length(t_vec)-1)+b2/2+b4-r*4-b4 z_des_First_Filmtime(length(t_vec)-1)+b2/2-r*4-b4],TUMBlue,'EdgeColor','none');
        
        %PD-Regulator
        curr_distance_wall = abs(z_des_First_Filmtime(length(t_vec)-1)+b2/2-r*4)-abs(z_NUM_First_Filmtime(length(t_vec)-1)-r)-b2/2;
        curr_distance_wall_spring = curr_distance_wall + (r-r*cos(asin(hd/r))) + b2;
        
        % Damper
        patch([t_vec(end-1)+hd-h/4 t_vec(end-1)+hd+h/4 t_vec(end-1)+hd+h/4 t_vec(end-1)+hd-h/4],[z_des_First_Filmtime(length(t_vec)-1)+b2/2-r*4 z_des_First_Filmtime(length(t_vec)-1)+b2/2-r*4 z_des_First_Filmtime(length(t_vec)-1)+b2/2-r*4+1/2*curr_distance_wall z_des_First_Filmtime(length(t_vec)-1)+b2/2-r*4+1/2*curr_distance_wall],TUMBlue,'EdgeColor','none')
        damper_width = 7/8; %of hd
        inner_damper_width = 0.4;
        patch([t_vec(end-1)+hd-hd*damper_width t_vec(end-1)+hd+hd*damper_width t_vec(end-1)+hd+hd*damper_width t_vec(end-1)+hd-hd*damper_width],[z_des_First_Filmtime(length(t_vec)-1)+b2/2-r*4+1/2*curr_distance_wall z_des_First_Filmtime(length(t_vec)-1)+b2/2-r*4+1/2*curr_distance_wall z_des_First_Filmtime(length(t_vec)-1)+b2/2-r*4+0.6*curr_distance_wall z_des_First_Filmtime(length(t_vec)-1)+b2/2-r*4+0.6*curr_distance_wall],TUMBlue,'EdgeColor','none')
        patch([t_vec(end-1)+hd-hd*damper_width t_vec(end-1)+hd-hd*damper_width+h/2 t_vec(end-1)+hd-hd*damper_width+h/2 t_vec(end-1)+hd-hd*damper_width],[z_des_First_Filmtime(length(t_vec)-1)+b2/2-r*4+0.6*curr_distance_wall z_des_First_Filmtime(length(t_vec)-1)+b2/2-r*4+0.6*curr_distance_wall z_des_First_Filmtime(length(t_vec)-1)+b2/2-r*4+3/4*curr_distance_wall z_des_First_Filmtime(length(t_vec)-1)+b2/2-r*4+3/4*curr_distance_wall],TUMBlue,'EdgeColor','none')
        patch([t_vec(end-1)+hd+hd*damper_width-h/2 t_vec(end-1)+hd+hd*damper_width t_vec(end-1)+hd+hd*damper_width t_vec(end-1)+hd+hd*damper_width-h/2],[z_des_First_Filmtime(length(t_vec)-1)+b2/2-r*4+0.6*curr_distance_wall z_des_First_Filmtime(length(t_vec)-1)+b2/2-r*4+0.6*curr_distance_wall z_des_First_Filmtime(length(t_vec)-1)+b2/2-r*4+3/4*curr_distance_wall z_des_First_Filmtime(length(t_vec)-1)+b2/2-r*4+3/4*curr_distance_wall],TUMBlue,'EdgeColor','none')  
        patch([t_vec(end-1)+hd-hd*inner_damper_width t_vec(end-1)+hd+hd*inner_damper_width t_vec(end-1)+hd+hd*inner_damper_width t_vec(end-1)+hd-hd*inner_damper_width],[z_des_First_Filmtime(length(t_vec)-1)+b2/2-r*4+2/3*curr_distance_wall z_des_First_Filmtime(length(t_vec)-1)+b2/2-r*4+2/3*curr_distance_wall z_des_First_Filmtime(length(t_vec)-1)+b2/2-r*4+3/4*curr_distance_wall z_des_First_Filmtime(length(t_vec)-1)+b2/2-r*4+3/4*curr_distance_wall],TUMBlue,'EdgeColor','none')
        patch([t_vec(end-1)+hd-h/4 t_vec(end-1)+hd+h/4 t_vec(end-1)+hd+h/4 t_vec(end-1)+hd-h/4],[z_des_First_Filmtime(length(t_vec)-1)+b2/2-r*4+3/4*curr_distance_wall z_des_First_Filmtime(length(t_vec)-1)+b2/2-r*4+3/4*curr_distance_wall z_NUM_First_Filmtime(length(t_vec)-1) z_NUM_First_Filmtime(length(t_vec)-1)],TUMBlue,'EdgeColor','none')
        
        % Feder Kp
        patch([t_vec(end-1)-hd-h/2 t_vec(end-1)-hd+h/2 t_vec(end-1)-hd+h/2 t_vec(end-1)-hd-h/2],            [z_des_First_Filmtime(length(t_vec)-1)+b2/2-4*r                               z_des_First_Filmtime(length(t_vec)-1)+b2/2-4*r                                z_des_First_Filmtime(length(t_vec)-1)+b2/2-4*r+0.1*curr_distance_wall_spring  z_des_First_Filmtime(length(t_vec)-1)+b2/2-4*r+0.1*curr_distance_wall_spring],TUMBlue,'EdgeColor','none');
        patch([t_vec(end-1)-hd+h/2 t_vec(end-1)-hd+h/2+b6 t_vec(end-1)-hd-h/2+b6 t_vec(end-1)-hd-h/2],      [z_des_First_Filmtime(length(t_vec)-1)+b2/2-4*r+0.1*curr_distance_wall_spring z_des_First_Filmtime(length(t_vec)-1)+b2/2-4*r+0.2*curr_distance_wall_spring  z_des_First_Filmtime(length(t_vec)-1)+b2/2-4*r+0.2*curr_distance_wall_spring  z_des_First_Filmtime(length(t_vec)-1)+b2/2-4*r+0.1*curr_distance_wall_spring],TUMBlue,'EdgeColor','none');
        patch([t_vec(end-1)-hd+h/2+b6 t_vec(end-1)-hd+h/2-b6 t_vec(end-1)-hd-h/2-b6 t_vec(end-1)-hd-h/2+b6],[z_des_First_Filmtime(length(t_vec)-1)+b2/2-4*r+0.2*curr_distance_wall_spring z_des_First_Filmtime(length(t_vec)-1)+b2/2-4*r+0.4*curr_distance_wall_spring z_des_First_Filmtime(length(t_vec)-1)+b2/2-4*r+0.4*curr_distance_wall_spring  z_des_First_Filmtime(length(t_vec)-1)+b2/2-4*r+0.2*curr_distance_wall_spring],TUMBlue,'EdgeColor','none');
        patch([t_vec(end-1)-hd+h/2-b6 t_vec(end-1)-hd+h/2+b6 t_vec(end-1)-hd-h/2+b6 t_vec(end-1)-hd-h/2-b6],[z_des_First_Filmtime(length(t_vec)-1)+b2/2-4*r+0.4*curr_distance_wall_spring z_des_First_Filmtime(length(t_vec)-1)+b2/2-4*r+0.6*curr_distance_wall_spring z_des_First_Filmtime(length(t_vec)-1)+b2/2-4*r+0.6*curr_distance_wall_spring  z_des_First_Filmtime(length(t_vec)-1)+b2/2-4*r+0.4*curr_distance_wall_spring],TUMBlue,'EdgeColor','none');
        patch([t_vec(end-1)-hd+h/2+b6 t_vec(end-1)-hd+h/2-b6 t_vec(end-1)-hd-h/2-b6 t_vec(end-1)-hd-h/2+b6],[z_des_First_Filmtime(length(t_vec)-1)+b2/2-4*r+0.6*curr_distance_wall_spring z_des_First_Filmtime(length(t_vec)-1)+b2/2-4*r+0.8*curr_distance_wall_spring z_des_First_Filmtime(length(t_vec)-1)+b2/2-4*r+0.8*curr_distance_wall_spring  z_des_First_Filmtime(length(t_vec)-1)+b2/2-4*r+0.6*curr_distance_wall_spring],TUMBlue,'EdgeColor','none');
        patch([t_vec(end-1)-hd+h/2-b6 t_vec(end-1)-hd+h/2    t_vec(end-1)-hd-h/2    t_vec(end-1)-hd-h/2-b6],[z_des_First_Filmtime(length(t_vec)-1)+b2/2-4*r+0.8*curr_distance_wall_spring z_des_First_Filmtime(length(t_vec)-1)+b2/2-4*r+0.9*curr_distance_wall_spring z_des_First_Filmtime(length(t_vec)-1)+b2/2-4*r+0.9*curr_distance_wall_spring  z_des_First_Filmtime(length(t_vec)-1)+b2/2-4*r+0.8*curr_distance_wall_spring],TUMBlue,'EdgeColor','none');
        patch([t_vec(end-1)-hd+h/2    t_vec(end-1)-hd+h/2    t_vec(end-1)-hd-h/2    t_vec(end-1)-hd-h/2],   [z_des_First_Filmtime(length(t_vec)-1)+b2/2-4*r+0.9*curr_distance_wall_spring z_des_First_Filmtime(length(t_vec)-1)+b2/2-4*r+curr_distance_wall_spring     z_des_First_Filmtime(length(t_vec)-1)+b2/2-4*r+curr_distance_wall_spring      z_des_First_Filmtime(length(t_vec)-1)+b2/2-4*r+0.9*curr_distance_wall_spring],TUMBlue,'EdgeColor','none');

        %Ground at 0
        patch([t_vec(end-1)-1.5,          t_vec(end-1)+1.5,          t_vec(end-1)+1.5,          t_vec(end-1)-1.5],[0 0 0.15 0.15],TUMGreen,'EdgeColor','none')
        patch([t_vec(end-1)-0.075-0.1     t_vec(end-1)+0.075-0.1     t_vec(end-1)+0.5+0.075-0.1 t_vec(end-1)+0.5-0.075-0.1],[0.15 0.15 0.65 0.65],TUMGreen,'EdgeColor','none')
        patch([t_vec(end-1)-0.075-0.5-0.1 t_vec(end-1)+0.075-0.5-0.1 t_vec(end-1)+0.075-0.1     t_vec(end-1)-0.075-0.1],[0.15 0.15 0.65 0.65],TUMGreen,'EdgeColor','none')
        patch([t_vec(end-1)-0.075+0.5-0.1 t_vec(end-1)+0.075+0.5-0.1 t_vec(end-1)+1+0.075-0.1     t_vec(end-1)+1-0.075-0.1],[0.15 0.15 0.65 0.65],TUMGreen,'EdgeColor','none')
        patch([t_vec(end-1)-0.075-1-0.1   t_vec(end-1)+0.075-1-0.1   t_vec(end-1)+0.075-0.1-0.5 t_vec(end-1)-0.075-0.1-0.5],[0.15 0.15 0.65 0.65],TUMGreen,'EdgeColor','none')
        patch([t_vec(end-1)-0.075+1-0.1   t_vec(end-1)+0.075+1-0.1   t_vec(end-1)+1.5+0.075-0.1 t_vec(end-1)+1.5-0.075-0.1],[0.15 0.15 0.65 0.65],TUMGreen,'EdgeColor','none')
        
        %Reference for First Iteration
		transparency = 1;
        draw_circle(t_vec(idx),z_NUM_Ref_Filmtime(idx),r,TUMBlack,transparency);
        plot(t_vec(1:idx),z_NUM_Ref_Filmtime(1:idx)+r, '--', 'Color',TUMBlack)
        draw_circle(t_vec(idx), z_EXP_Ref_Filmtime(idx),r,TUMBlack,transparency);
        axis equal   
        
        % spring kE
        curr_distance = abs(z_NUM_Ref_Filmtime(idx)+r)-abs(z_EXP_Ref_Filmtime(idx)-r);
        patch([t_vec(idx)-h/2 t_vec(idx)+h/2 t_vec(idx)+h/2 t_vec(idx)-h/2],            [z_NUM_Ref_Filmtime(idx)+r z_NUM_Ref_Filmtime(idx)+r z_NUM_Ref_Filmtime(idx)+r+ 0.1*curr_distance z_NUM_Ref_Filmtime(idx)+r+ 0.1*curr_distance],TUMBlack,'EdgeColor','none','FaceAlpha',transparency);
        patch([t_vec(idx)+h/2 t_vec(idx)+h/2+b5 t_vec(idx)-h/2+b5 t_vec(idx)-h/2],      [z_NUM_Ref_Filmtime(idx)+r+0.1*curr_distance z_NUM_Ref_Filmtime(idx)+r+0.15*curr_distance z_NUM_Ref_Filmtime(idx)+r+0.15*curr_distance z_NUM_Ref_Filmtime(idx)+r+0.1*curr_distance],TUMBlack,'EdgeColor','none','FaceAlpha',transparency);
        patch([t_vec(idx)+h/2+b5 t_vec(idx)+h/2-b5 t_vec(idx)-h/2-b5 t_vec(idx)-h/2+b5],[z_NUM_Ref_Filmtime(idx)+r+0.15*curr_distance z_NUM_Ref_Filmtime(idx)+r+0.2*curr_distance z_NUM_Ref_Filmtime(idx)+r+0.2*curr_distance z_NUM_Ref_Filmtime(idx)+r+0.15*curr_distance],TUMBlack,'EdgeColor','none','FaceAlpha',transparency);
        patch([t_vec(idx)+h/2-b5 t_vec(idx)+h/2+b5 t_vec(idx)-h/2+b5 t_vec(idx)-h/2-b5],[z_NUM_Ref_Filmtime(idx)+r+0.2*curr_distance z_NUM_Ref_Filmtime(idx)+r+0.25*curr_distance z_NUM_Ref_Filmtime(idx)+r+0.25*curr_distance z_NUM_Ref_Filmtime(idx)+r+0.2*curr_distance],TUMBlack,'EdgeColor','none','FaceAlpha',transparency);
        patch([t_vec(idx)+h/2+b5 t_vec(idx)+h/2-b5 t_vec(idx)-h/2-b5 t_vec(idx)-h/2+b5],[z_NUM_Ref_Filmtime(idx)+r+0.25*curr_distance z_NUM_Ref_Filmtime(idx)+r+0.3*curr_distance z_NUM_Ref_Filmtime(idx)+r+0.3*curr_distance z_NUM_Ref_Filmtime(idx)+r+0.25*curr_distance],TUMBlack,'EdgeColor','none','FaceAlpha',transparency);      
        patch([t_vec(idx)+h/2-b5 t_vec(idx)+h/2+b5 t_vec(idx)-h/2+b5 t_vec(idx)-h/2-b5],[z_NUM_Ref_Filmtime(idx)+r+0.3*curr_distance z_NUM_Ref_Filmtime(idx)+r+0.35*curr_distance z_NUM_Ref_Filmtime(idx)+r+0.35*curr_distance z_NUM_Ref_Filmtime(idx)+r+0.3*curr_distance],TUMBlack,'EdgeColor','none','FaceAlpha',transparency);
        patch([t_vec(idx)+h/2+b5 t_vec(idx)+h/2-b5 t_vec(idx)-h/2-b5 t_vec(idx)-h/2+b5],[z_NUM_Ref_Filmtime(idx)+r+0.35*curr_distance z_NUM_Ref_Filmtime(idx)+r+0.4*curr_distance z_NUM_Ref_Filmtime(idx)+r+0.4*curr_distance z_NUM_Ref_Filmtime(idx)+r+0.35*curr_distance],TUMBlack,'EdgeColor','none','FaceAlpha',transparency);
        patch([t_vec(idx)+h/2-b5 t_vec(idx)+h/2+b5 t_vec(idx)-h/2+b5 t_vec(idx)-h/2-b5],[z_NUM_Ref_Filmtime(idx)+r+0.4*curr_distance z_NUM_Ref_Filmtime(idx)+r+0.45*curr_distance z_NUM_Ref_Filmtime(idx)+r+0.45*curr_distance z_NUM_Ref_Filmtime(idx)+r+0.4*curr_distance],TUMBlack,'EdgeColor','none','FaceAlpha',transparency);
        patch([t_vec(idx)+h/2+b5 t_vec(idx)+h/2-b5 t_vec(idx)-h/2-b5 t_vec(idx)-h/2+b5],[z_NUM_Ref_Filmtime(idx)+r+0.45*curr_distance z_NUM_Ref_Filmtime(idx)+r+0.5*curr_distance z_NUM_Ref_Filmtime(idx)+r+0.5*curr_distance z_NUM_Ref_Filmtime(idx)+r+0.45*curr_distance],TUMBlack,'EdgeColor','none','FaceAlpha',transparency); 
        patch([t_vec(idx)+h/2-b5 t_vec(idx)+h/2+b5 t_vec(idx)-h/2+b5 t_vec(idx)-h/2-b5],[z_NUM_Ref_Filmtime(idx)+r+0.5*curr_distance z_NUM_Ref_Filmtime(idx)+r+0.55*curr_distance z_NUM_Ref_Filmtime(idx)+r+0.55*curr_distance z_NUM_Ref_Filmtime(idx)+r+0.5*curr_distance],TUMBlack,'EdgeColor','none','FaceAlpha',transparency);
        patch([t_vec(idx)+h/2+b5 t_vec(idx)+h/2-b5 t_vec(idx)-h/2-b5 t_vec(idx)-h/2+b5],[z_NUM_Ref_Filmtime(idx)+r+0.55*curr_distance z_NUM_Ref_Filmtime(idx)+r+0.6*curr_distance z_NUM_Ref_Filmtime(idx)+r+0.6*curr_distance z_NUM_Ref_Filmtime(idx)+r+0.55*curr_distance],TUMBlack,'EdgeColor','none','FaceAlpha',transparency);
        patch([t_vec(idx)+h/2-b5 t_vec(idx)+h/2+b5 t_vec(idx)-h/2+b5 t_vec(idx)-h/2-b5],[z_NUM_Ref_Filmtime(idx)+r+0.6*curr_distance z_NUM_Ref_Filmtime(idx)+r+0.65*curr_distance z_NUM_Ref_Filmtime(idx)+r+0.65*curr_distance z_NUM_Ref_Filmtime(idx)+r+0.6*curr_distance],TUMBlack,'EdgeColor','none','FaceAlpha',transparency);
        patch([t_vec(idx)+h/2+b5 t_vec(idx)+h/2-b5 t_vec(idx)-h/2-b5 t_vec(idx)-h/2+b5],[z_NUM_Ref_Filmtime(idx)+r+0.65*curr_distance z_NUM_Ref_Filmtime(idx)+r+0.7*curr_distance z_NUM_Ref_Filmtime(idx)+r+0.7*curr_distance z_NUM_Ref_Filmtime(idx)+r+0.65*curr_distance],TUMBlack,'EdgeColor','none','FaceAlpha',transparency);
        patch([t_vec(idx)+h/2-b5 t_vec(idx)+h/2+b5 t_vec(idx)-h/2+b5 t_vec(idx)-h/2-b5],[z_NUM_Ref_Filmtime(idx)+r+0.7*curr_distance z_NUM_Ref_Filmtime(idx)+r+0.75*curr_distance z_NUM_Ref_Filmtime(idx)+r+0.75*curr_distance z_NUM_Ref_Filmtime(idx)+r+0.7*curr_distance],TUMBlack,'EdgeColor','none','FaceAlpha',transparency);
        patch([t_vec(idx)+h/2+b5 t_vec(idx)+h/2-b5 t_vec(idx)-h/2-b5 t_vec(idx)-h/2+b5],[z_NUM_Ref_Filmtime(idx)+r+0.75*curr_distance z_NUM_Ref_Filmtime(idx)+r+0.8*curr_distance z_NUM_Ref_Filmtime(idx)+r+0.8*curr_distance z_NUM_Ref_Filmtime(idx)+r+0.75*curr_distance],TUMBlack,'EdgeColor','none','FaceAlpha',transparency);
        patch([t_vec(idx)+h/2-b5 t_vec(idx)+h/2+b5 t_vec(idx)-h/2+b5 t_vec(idx)-h/2-b5],[z_NUM_Ref_Filmtime(idx)+r+0.8*curr_distance z_NUM_Ref_Filmtime(idx)+r+0.85*curr_distance z_NUM_Ref_Filmtime(idx)+r+0.85*curr_distance z_NUM_Ref_Filmtime(idx)+r+0.8*curr_distance],TUMBlack,'EdgeColor','none','FaceAlpha',transparency);
        patch([t_vec(idx)+h/2+b5 t_vec(idx)+h/2-b5 t_vec(idx)-h/2-b5 t_vec(idx)-h/2+b5],[z_NUM_Ref_Filmtime(idx)+r+0.85*curr_distance z_NUM_Ref_Filmtime(idx)+r+0.9*curr_distance z_NUM_Ref_Filmtime(idx)+r+0.9*curr_distance z_NUM_Ref_Filmtime(idx)+r+0.85*curr_distance],TUMBlack,'EdgeColor','none','FaceAlpha',transparency);
        patch([t_vec(idx)+h/2-b5 t_vec(idx)+h/2    t_vec(idx)-h/2    t_vec(idx)-h/2-b5],[z_NUM_Ref_Filmtime(idx)+r+0.9*curr_distance z_NUM_Ref_Filmtime(idx)+r+0.95*curr_distance z_NUM_Ref_Filmtime(idx)+r+0.95*curr_distance z_NUM_Ref_Filmtime(idx)+r+0.9*curr_distance],TUMBlack,'EdgeColor','none','FaceAlpha',transparency);
        patch([t_vec(idx)+h/2    t_vec(idx)+h/2    t_vec(idx)-h/2    t_vec(idx)-h/2],   [z_NUM_Ref_Filmtime(idx)+r+0.95*curr_distance z_NUM_Ref_Filmtime(idx)+r+curr_distance z_NUM_Ref_Filmtime(idx)+r+curr_distance z_NUM_Ref_Filmtime(idx)+r+0.95*curr_distance],TUMBlack,'EdgeColor','none','FaceAlpha',transparency);
        
        %wall
        patch([t_vec(idx)+b/2 t_vec(idx)+b/2 t_vec(idx)-b/2 t_vec(idx)-b/2],[z_NUM_Ref_Filmtime_2(idx)-b2/2-r*4 z_NUM_Ref_Filmtime_2(idx)+b2/2-r*4 z_NUM_Ref_Filmtime_2(idx)+b2/2-r*4 z_NUM_Ref_Filmtime_2(idx)-b2/2-r*4],TUMBlack,'EdgeColor','none','FaceAlpha',transparency); 
        
        %wall diagonals
        patch([t_vec(idx)+b3/2-b4+0.1 t_vec(idx)+b3/2+0.1 t_vec(idx)-b3/2+0.1 t_vec(idx)-b3/2-b4+0.1],[z_NUM_Ref_Filmtime_2(idx)+b2/2-r*4-b4 z_NUM_Ref_Filmtime_2(idx)+b2/2+b4-r*4-b4 z_NUM_Ref_Filmtime_2(idx)+b2/2+b4-r*4-b4 z_NUM_Ref_Filmtime_2(idx)+b2/2-r*4-b4],TUMBlack,'EdgeColor','none','FaceAlpha',transparency);     
        patch([t_vec(idx)+b3/2+0.3*b-b4+0.1 t_vec(idx)+b3/2+0.3*b+0.1 t_vec(idx)-b3/2+0.3*b+0.1 t_vec(idx)-b3/2+0.3*b-b4+0.1],[z_NUM_Ref_Filmtime_2(idx)+b2/2-r*4-b4 z_NUM_Ref_Filmtime_2(idx)+b2/2+b4-r*4-b4 z_NUM_Ref_Filmtime_2(idx)+b2/2+b4-r*4-b4 z_NUM_Ref_Filmtime_2(idx)+b2/2-r*4-b4],TUMBlack,'EdgeColor','none','FaceAlpha',transparency);
        patch([t_vec(idx)+b3/2-0.3*b-b4+0.1 t_vec(idx)+b3/2-0.3*b+0.1 t_vec(idx)-b3/2-0.3*b+0.1 t_vec(idx)-b3/2-0.3*b-b4+0.1],[z_NUM_Ref_Filmtime_2(idx)+b2/2-r*4-b4 z_NUM_Ref_Filmtime_2(idx)+b2/2+b4-r*4-b4 z_NUM_Ref_Filmtime_2(idx)+b2/2+b4-r*4-b4 z_NUM_Ref_Filmtime_2(idx)+b2/2-r*4-b4],TUMBlack,'EdgeColor','none','FaceAlpha',transparency);
        
        %PD-Regler
        curr_distance_wall = abs(z_NUM_Ref_Filmtime_2(idx)+b2/2-r*4)-abs(z_NUM_Ref_Filmtime(idx)-r)-b2/2;
        curr_distance_wall_spring = curr_distance_wall + (r-r*cos(asin(hd/r))) + b2;
        
        % Damper
        patch([t_vec(idx)+hd-h/4 t_vec(idx)+hd+h/4 t_vec(idx)+hd+h/4 t_vec(idx)+hd-h/4],[z_NUM_Ref_Filmtime_2(idx)+b2/2-r*4 z_NUM_Ref_Filmtime_2(idx)+b2/2-r*4 z_NUM_Ref_Filmtime_2(idx)+b2/2-r*4+1/2*curr_distance_wall z_NUM_Ref_Filmtime_2(idx)+b2/2-r*4+1/2*curr_distance_wall],TUMBlack,'EdgeColor','none','FaceAlpha',transparency)
        damper_width = 7/8; %of hd
        inner_damper_width = 0.4;
        patch([t_vec(idx)+hd-hd*damper_width t_vec(idx)+hd+hd*damper_width t_vec(idx)+hd+hd*damper_width t_vec(idx)+hd-hd*damper_width],[z_NUM_Ref_Filmtime_2(idx)+b2/2-r*4+1/2*curr_distance_wall z_NUM_Ref_Filmtime_2(idx)+b2/2-r*4+1/2*curr_distance_wall z_NUM_Ref_Filmtime_2(idx)+b2/2-r*4+0.6*curr_distance_wall z_NUM_Ref_Filmtime_2(idx)+b2/2-r*4+0.6*curr_distance_wall],TUMBlack,'EdgeColor','none','FaceAlpha',transparency)
        patch([t_vec(idx)+hd-hd*damper_width t_vec(idx)+hd-hd*damper_width+h/2 t_vec(idx)+hd-hd*damper_width+h/2 t_vec(idx)+hd-hd*damper_width],[z_NUM_Ref_Filmtime_2(idx)+b2/2-r*4+0.6*curr_distance_wall z_NUM_Ref_Filmtime_2(idx)+b2/2-r*4+0.6*curr_distance_wall z_NUM_Ref_Filmtime_2(idx)+b2/2-r*4+3/4*curr_distance_wall z_NUM_Ref_Filmtime_2(idx)+b2/2-r*4+3/4*curr_distance_wall],TUMBlack,'EdgeColor','none','FaceAlpha',transparency)
        patch([t_vec(idx)+hd+hd*damper_width-h/2 t_vec(idx)+hd+hd*damper_width t_vec(idx)+hd+hd*damper_width t_vec(idx)+hd+hd*damper_width-h/2],[z_NUM_Ref_Filmtime_2(idx)+b2/2-r*4+0.6*curr_distance_wall z_NUM_Ref_Filmtime_2(idx)+b2/2-r*4+0.6*curr_distance_wall z_NUM_Ref_Filmtime_2(idx)+b2/2-r*4+3/4*curr_distance_wall z_NUM_Ref_Filmtime_2(idx)+b2/2-r*4+3/4*curr_distance_wall],TUMBlack,'EdgeColor','none','FaceAlpha',transparency)  
        patch([t_vec(idx)+hd-hd*inner_damper_width t_vec(idx)+hd+hd*inner_damper_width t_vec(idx)+hd+hd*inner_damper_width t_vec(idx)+hd-hd*inner_damper_width],[z_NUM_Ref_Filmtime_2(idx)+b2/2-r*4+2/3*curr_distance_wall z_NUM_Ref_Filmtime_2(idx)+b2/2-r*4+2/3*curr_distance_wall z_NUM_Ref_Filmtime_2(idx)+b2/2-r*4+3/4*curr_distance_wall z_NUM_Ref_Filmtime_2(idx)+b2/2-r*4+3/4*curr_distance_wall],TUMBlack,'EdgeColor','none','FaceAlpha',transparency)
        patch([t_vec(idx)+hd-h/4 t_vec(idx)+hd+h/4 t_vec(idx)+hd+h/4 t_vec(idx)+hd-h/4],[z_NUM_Ref_Filmtime_2(idx)+b2/2-r*4+3/4*curr_distance_wall z_NUM_Ref_Filmtime_2(idx)+b2/2-r*4+3/4*curr_distance_wall z_NUM_Ref_Filmtime(idx) z_NUM_Ref_Filmtime(idx)],TUMBlack,'EdgeColor','none','FaceAlpha',transparency)
        
        % Feder Kp
        patch([t_vec(idx)-hd-h/2 t_vec(idx)-hd+h/2 t_vec(idx)-hd+h/2 t_vec(idx)-hd-h/2],            [z_NUM_Ref_Filmtime_2(idx)+b2/2-4*r                               z_NUM_Ref_Filmtime_2(idx)+b2/2-4*r                                z_NUM_Ref_Filmtime_2(idx)+b2/2-4*r+0.1*curr_distance_wall_spring  z_NUM_Ref_Filmtime_2(idx)+b2/2-4*r+0.1*curr_distance_wall_spring],TUMBlack,'EdgeColor','none','FaceAlpha',transparency);
        patch([t_vec(idx)-hd+h/2 t_vec(idx)-hd+h/2+b6 t_vec(idx)-hd-h/2+b6 t_vec(idx)-hd-h/2],      [z_NUM_Ref_Filmtime_2(idx)+b2/2-4*r+0.1*curr_distance_wall_spring z_NUM_Ref_Filmtime_2(idx)+b2/2-4*r+0.2*curr_distance_wall_spring  z_NUM_Ref_Filmtime_2(idx)+b2/2-4*r+0.2*curr_distance_wall_spring  z_NUM_Ref_Filmtime_2(idx)+b2/2-4*r+0.1*curr_distance_wall_spring],TUMBlack,'EdgeColor','none','FaceAlpha',transparency);
        patch([t_vec(idx)-hd+h/2+b6 t_vec(idx)-hd+h/2-b6 t_vec(idx)-hd-h/2-b6 t_vec(idx)-hd-h/2+b6],[z_NUM_Ref_Filmtime_2(idx)+b2/2-4*r+0.2*curr_distance_wall_spring z_NUM_Ref_Filmtime_2(idx)+b2/2-4*r+0.4*curr_distance_wall_spring z_NUM_Ref_Filmtime_2(idx)+b2/2-4*r+0.4*curr_distance_wall_spring  z_NUM_Ref_Filmtime_2(idx)+b2/2-4*r+0.2*curr_distance_wall_spring],TUMBlack,'EdgeColor','none','FaceAlpha',transparency);
        patch([t_vec(idx)-hd+h/2-b6 t_vec(idx)-hd+h/2+b6 t_vec(idx)-hd-h/2+b6 t_vec(idx)-hd-h/2-b6],[z_NUM_Ref_Filmtime_2(idx)+b2/2-4*r+0.4*curr_distance_wall_spring z_NUM_Ref_Filmtime_2(idx)+b2/2-4*r+0.6*curr_distance_wall_spring z_NUM_Ref_Filmtime_2(idx)+b2/2-4*r+0.6*curr_distance_wall_spring  z_NUM_Ref_Filmtime_2(idx)+b2/2-4*r+0.4*curr_distance_wall_spring],TUMBlack,'EdgeColor','none','FaceAlpha',transparency);
        patch([t_vec(idx)-hd+h/2+b6 t_vec(idx)-hd+h/2-b6 t_vec(idx)-hd-h/2-b6 t_vec(idx)-hd-h/2+b6],[z_NUM_Ref_Filmtime_2(idx)+b2/2-4*r+0.6*curr_distance_wall_spring z_NUM_Ref_Filmtime_2(idx)+b2/2-4*r+0.8*curr_distance_wall_spring z_NUM_Ref_Filmtime_2(idx)+b2/2-4*r+0.8*curr_distance_wall_spring  z_NUM_Ref_Filmtime_2(idx)+b2/2-4*r+0.6*curr_distance_wall_spring],TUMBlack,'EdgeColor','none','FaceAlpha',transparency);
        patch([t_vec(idx)-hd+h/2-b6 t_vec(idx)-hd+h/2    t_vec(idx)-hd-h/2    t_vec(idx)-hd-h/2-b6],[z_NUM_Ref_Filmtime_2(idx)+b2/2-4*r+0.8*curr_distance_wall_spring z_NUM_Ref_Filmtime_2(idx)+b2/2-4*r+0.9*curr_distance_wall_spring z_NUM_Ref_Filmtime_2(idx)+b2/2-4*r+0.9*curr_distance_wall_spring  z_NUM_Ref_Filmtime_2(idx)+b2/2-4*r+0.8*curr_distance_wall_spring],TUMBlack,'EdgeColor','none','FaceAlpha',transparency);
        patch([t_vec(idx)-hd+h/2    t_vec(idx)-hd+h/2    t_vec(idx)-hd-h/2    t_vec(idx)-hd-h/2],   [z_NUM_Ref_Filmtime_2(idx)+b2/2-4*r+0.9*curr_distance_wall_spring z_NUM_Ref_Filmtime_2(idx)+b2/2-4*r+curr_distance_wall_spring     z_NUM_Ref_Filmtime_2(idx)+b2/2-4*r+curr_distance_wall_spring      z_NUM_Ref_Filmtime_2(idx)+b2/2-4*r+0.9*curr_distance_wall_spring],TUMBlack,'EdgeColor','none','FaceAlpha',transparency);

        %Ground at 0
        patch([t_vec(idx)-1.5,          t_vec(idx)+1.5,          t_vec(idx)+1.5,          t_vec(idx)-1.5],[0 0 0.15 0.15],TUMBlack,'EdgeColor','none','FaceAlpha',transparency)
        patch([t_vec(idx)-0.075-0.1     t_vec(idx)+0.075-0.1     t_vec(idx)+0.5+0.075-0.1 t_vec(idx)+0.5-0.075-0.1],[0.15 0.15 0.65 0.65],TUMBlack,'EdgeColor','none','FaceAlpha',transparency)
        patch([t_vec(idx)-0.075-0.5-0.1 t_vec(idx)+0.075-0.5-0.1 t_vec(idx)+0.075-0.1     t_vec(idx)-0.075-0.1],[0.15 0.15 0.65 0.65],TUMBlack,'EdgeColor','none','FaceAlpha',transparency)
        patch([t_vec(idx)-0.075+0.5-0.1 t_vec(idx)+0.075+0.5-0.1 t_vec(idx)+1+0.075-0.1     t_vec(idx)+1-0.075-0.1],[0.15 0.15 0.65 0.65],TUMBlack,'EdgeColor','none','FaceAlpha',transparency)
        patch([t_vec(idx)-0.075-1-0.1   t_vec(idx)+0.075-1-0.1   t_vec(idx)+0.075-0.1-0.5 t_vec(idx)-0.075-0.1-0.5],[0.15 0.15 0.65 0.65],TUMBlack,'EdgeColor','none','FaceAlpha',transparency)
        patch([t_vec(idx)-0.075+1-0.1   t_vec(idx)+0.075+1-0.1   t_vec(idx)+1.5+0.075-0.1 t_vec(idx)+1.5-0.075-0.1],[0.15 0.15 0.65 0.65],TUMBlack,'EdgeColor','none','FaceAlpha',transparency)
     
        xlabel('Time / s','Interpreter','Latex')
        ylabel('Displacement / mm','Interpreter','Latex')
        xticks([0 1 2 3 4])
        set(gca,'Fontsize',18)
        ylim([-12 1])
        xlim([-1.5 5.5])
        title('Iteration 1')
        
        subplot(1,2,2)
        hold on
        syms x y

        %Stationary Actual for Last iteration
        plot(t_vec(1:end-1),z_NUM_Last_Filmtime(1:(length(t_vec)-1))+r,'Color',[0 0.4 0.74]) 
        draw_circle(t_vec(end-1),z_NUM_Last_Filmtime(length(t_vec)-1),r,TUMBlue,1);
        axis equal
        draw_circle(t_vec(end-1), z_EXP_Last_Filmtime(length(t_vec)-1),r,TUMGreen,1);
        
        curr_distance = abs(z_NUM_Last_Filmtime(length(t_vec)-1)+r)-abs(z_EXP_Last_Filmtime(end-1)-r);
        patch([t_vec(end-1)-h/2 t_vec(end-1)+h/2 t_vec(end-1)+h/2 t_vec(end-1)-h/2],            [z_NUM_Last_Filmtime(length(t_vec)-1)+r z_NUM_Last_Filmtime(length(t_vec)-1)+r z_NUM_Last_Filmtime(length(t_vec)-1)+r+ 0.1*curr_distance z_NUM_Last_Filmtime(length(t_vec)-1)+r+ 0.1*curr_distance],TUMGreen,'EdgeColor','none');
        patch([t_vec(end-1)+h/2 t_vec(end-1)+h/2+b5 t_vec(end-1)-h/2+b5 t_vec(end-1)-h/2],      [z_NUM_Last_Filmtime(length(t_vec)-1)+r+0.1*curr_distance z_NUM_Last_Filmtime(length(t_vec)-1)+r+0.15*curr_distance z_NUM_Last_Filmtime(length(t_vec)-1)+r+0.15*curr_distance z_NUM_Last_Filmtime(length(t_vec)-1)+r+0.1*curr_distance],TUMGreen,'EdgeColor','none');
        patch([t_vec(end-1)+h/2+b5 t_vec(end-1)+h/2-b5 t_vec(end-1)-h/2-b5 t_vec(end-1)-h/2+b5],[z_NUM_Last_Filmtime(length(t_vec)-1)+r+0.15*curr_distance z_NUM_Last_Filmtime(length(t_vec)-1)+r+0.2*curr_distance z_NUM_Last_Filmtime(length(t_vec)-1)+r+0.2*curr_distance z_NUM_Last_Filmtime(length(t_vec)-1)+r+0.15*curr_distance],TUMGreen,'EdgeColor','none');
        patch([t_vec(end-1)+h/2-b5 t_vec(end-1)+h/2+b5 t_vec(end-1)-h/2+b5 t_vec(end-1)-h/2-b5],[z_NUM_Last_Filmtime(length(t_vec)-1)+r+0.2*curr_distance z_NUM_Last_Filmtime(length(t_vec)-1)+r+0.25*curr_distance z_NUM_Last_Filmtime(length(t_vec)-1)+r+0.25*curr_distance z_NUM_Last_Filmtime(length(t_vec)-1)+r+0.2*curr_distance],TUMGreen,'EdgeColor','none');
        patch([t_vec(end-1)+h/2+b5 t_vec(end-1)+h/2-b5 t_vec(end-1)-h/2-b5 t_vec(end-1)-h/2+b5],[z_NUM_Last_Filmtime(length(t_vec)-1)+r+0.25*curr_distance z_NUM_Last_Filmtime(length(t_vec)-1)+r+0.3*curr_distance z_NUM_Last_Filmtime(length(t_vec)-1)+r+0.3*curr_distance z_NUM_Last_Filmtime(length(t_vec)-1)+r+0.25*curr_distance],TUMGreen,'EdgeColor','none');      
        patch([t_vec(end-1)+h/2-b5 t_vec(end-1)+h/2+b5 t_vec(end-1)-h/2+b5 t_vec(end-1)-h/2-b5],[z_NUM_Last_Filmtime(length(t_vec)-1)+r+0.3*curr_distance z_NUM_Last_Filmtime(length(t_vec)-1)+r+0.35*curr_distance z_NUM_Last_Filmtime(length(t_vec)-1)+r+0.35*curr_distance z_NUM_Last_Filmtime(length(t_vec)-1)+r+0.3*curr_distance],TUMGreen,'EdgeColor','none');
        patch([t_vec(end-1)+h/2+b5 t_vec(end-1)+h/2-b5 t_vec(end-1)-h/2-b5 t_vec(end-1)-h/2+b5],[z_NUM_Last_Filmtime(length(t_vec)-1)+r+0.35*curr_distance z_NUM_Last_Filmtime(length(t_vec)-1)+r+0.4*curr_distance z_NUM_Last_Filmtime(length(t_vec)-1)+r+0.4*curr_distance z_NUM_Last_Filmtime(length(t_vec)-1)+r+0.35*curr_distance],TUMGreen,'EdgeColor','none');
        patch([t_vec(end-1)+h/2-b5 t_vec(end-1)+h/2+b5 t_vec(end-1)-h/2+b5 t_vec(end-1)-h/2-b5],[z_NUM_Last_Filmtime(length(t_vec)-1)+r+0.4*curr_distance z_NUM_Last_Filmtime(length(t_vec)-1)+r+0.45*curr_distance z_NUM_Last_Filmtime(length(t_vec)-1)+r+0.45*curr_distance z_NUM_Last_Filmtime(length(t_vec)-1)+r+0.4*curr_distance],TUMGreen,'EdgeColor','none');
        patch([t_vec(end-1)+h/2+b5 t_vec(end-1)+h/2-b5 t_vec(end-1)-h/2-b5 t_vec(end-1)-h/2+b5],[z_NUM_Last_Filmtime(length(t_vec)-1)+r+0.45*curr_distance z_NUM_Last_Filmtime(length(t_vec)-1)+r+0.5*curr_distance z_NUM_Last_Filmtime(length(t_vec)-1)+r+0.5*curr_distance z_NUM_Last_Filmtime(length(t_vec)-1)+r+0.45*curr_distance],TUMGreen,'EdgeColor','none'); 
        patch([t_vec(end-1)+h/2-b5 t_vec(end-1)+h/2+b5 t_vec(end-1)-h/2+b5 t_vec(end-1)-h/2-b5],[z_NUM_Last_Filmtime(length(t_vec)-1)+r+0.5*curr_distance z_NUM_Last_Filmtime(length(t_vec)-1)+r+0.55*curr_distance z_NUM_Last_Filmtime(length(t_vec)-1)+r+0.55*curr_distance z_NUM_Last_Filmtime(length(t_vec)-1)+r+0.5*curr_distance],TUMGreen,'EdgeColor','none');
        patch([t_vec(end-1)+h/2+b5 t_vec(end-1)+h/2-b5 t_vec(end-1)-h/2-b5 t_vec(end-1)-h/2+b5],[z_NUM_Last_Filmtime(length(t_vec)-1)+r+0.55*curr_distance z_NUM_Last_Filmtime(length(t_vec)-1)+r+0.6*curr_distance z_NUM_Last_Filmtime(length(t_vec)-1)+r+0.6*curr_distance z_NUM_Last_Filmtime(length(t_vec)-1)+r+0.55*curr_distance],TUMGreen,'EdgeColor','none');
        patch([t_vec(end-1)+h/2-b5 t_vec(end-1)+h/2+b5 t_vec(end-1)-h/2+b5 t_vec(end-1)-h/2-b5],[z_NUM_Last_Filmtime(length(t_vec)-1)+r+0.6*curr_distance z_NUM_Last_Filmtime(length(t_vec)-1)+r+0.65*curr_distance z_NUM_Last_Filmtime(length(t_vec)-1)+r+0.65*curr_distance z_NUM_Last_Filmtime(length(t_vec)-1)+r+0.6*curr_distance],TUMGreen,'EdgeColor','none');
        patch([t_vec(end-1)+h/2+b5 t_vec(end-1)+h/2-b5 t_vec(end-1)-h/2-b5 t_vec(end-1)-h/2+b5],[z_NUM_Last_Filmtime(length(t_vec)-1)+r+0.65*curr_distance z_NUM_Last_Filmtime(length(t_vec)-1)+r+0.7*curr_distance z_NUM_Last_Filmtime(length(t_vec)-1)+r+0.7*curr_distance z_NUM_Last_Filmtime(length(t_vec)-1)+r+0.65*curr_distance],TUMGreen,'EdgeColor','none');
        patch([t_vec(end-1)+h/2-b5 t_vec(end-1)+h/2+b5 t_vec(end-1)-h/2+b5 t_vec(end-1)-h/2-b5],[z_NUM_Last_Filmtime(length(t_vec)-1)+r+0.7*curr_distance z_NUM_Last_Filmtime(length(t_vec)-1)+r+0.75*curr_distance z_NUM_Last_Filmtime(length(t_vec)-1)+r+0.75*curr_distance z_NUM_Last_Filmtime(length(t_vec)-1)+r+0.7*curr_distance],TUMGreen,'EdgeColor','none');
        patch([t_vec(end-1)+h/2+b5 t_vec(end-1)+h/2-b5 t_vec(end-1)-h/2-b5 t_vec(end-1)-h/2+b5],[z_NUM_Last_Filmtime(length(t_vec)-1)+r+0.75*curr_distance z_NUM_Last_Filmtime(length(t_vec)-1)+r+0.8*curr_distance z_NUM_Last_Filmtime(length(t_vec)-1)+r+0.8*curr_distance z_NUM_Last_Filmtime(length(t_vec)-1)+r+0.75*curr_distance],TUMGreen,'EdgeColor','none');
        patch([t_vec(end-1)+h/2-b5 t_vec(end-1)+h/2+b5 t_vec(end-1)-h/2+b5 t_vec(end-1)-h/2-b5],[z_NUM_Last_Filmtime(length(t_vec)-1)+r+0.8*curr_distance z_NUM_Last_Filmtime(length(t_vec)-1)+r+0.85*curr_distance z_NUM_Last_Filmtime(length(t_vec)-1)+r+0.85*curr_distance z_NUM_Last_Filmtime(length(t_vec)-1)+r+0.8*curr_distance],TUMGreen,'EdgeColor','none');
        patch([t_vec(end-1)+h/2+b5 t_vec(end-1)+h/2-b5 t_vec(end-1)-h/2-b5 t_vec(end-1)-h/2+b5],[z_NUM_Last_Filmtime(length(t_vec)-1)+r+0.85*curr_distance z_NUM_Last_Filmtime(length(t_vec)-1)+r+0.9*curr_distance z_NUM_Last_Filmtime(length(t_vec)-1)+r+0.9*curr_distance z_NUM_Last_Filmtime(length(t_vec)-1)+r+0.85*curr_distance],TUMGreen,'EdgeColor','none');
        patch([t_vec(end-1)+h/2-b5 t_vec(end-1)+h/2    t_vec(end-1)-h/2    t_vec(end-1)-h/2-b5],[z_NUM_Last_Filmtime(length(t_vec)-1)+r+0.9*curr_distance z_NUM_Last_Filmtime(length(t_vec)-1)+r+0.95*curr_distance z_NUM_Last_Filmtime(length(t_vec)-1)+r+0.95*curr_distance z_NUM_Last_Filmtime(length(t_vec)-1)+r+0.9*curr_distance],TUMGreen,'EdgeColor','none');
        patch([t_vec(end-1)+h/2    t_vec(end-1)+h/2    t_vec(end-1)-h/2    t_vec(end-1)-h/2],   [z_NUM_Last_Filmtime(length(t_vec)-1)+r+0.95*curr_distance z_NUM_Last_Filmtime(length(t_vec)-1)+r+curr_distance z_NUM_Last_Filmtime(length(t_vec)-1)+r+curr_distance z_NUM_Last_Filmtime(length(t_vec)-1)+r+0.95*curr_distance],TUMGreen,'EdgeColor','none');
        
        %wall
        patch([t_vec(end-1)+b/2 t_vec(end-1)+b/2 t_vec(end-1)-b/2 t_vec(end-1)-b/2],[z_des_Last_Filmtime(length(t_vec)-1)-b2/2-r*4 z_des_Last_Filmtime(length(t_vec)-1)+b2/2-r*4 z_des_Last_Filmtime(length(t_vec)-1)+b2/2-r*4 z_des_Last_Filmtime(length(t_vec)-1)-b2/2-r*4],TUMBlue,'EdgeColor','none'); 
        
        %wall diagonals
        patch([t_vec(end-1)+b3/2-b4+0.1 t_vec(end-1)+b3/2+0.1 t_vec(end-1)-b3/2+0.1 t_vec(end-1)-b3/2-b4+0.1],[z_des_Last_Filmtime(length(t_vec)-1)+b2/2-r*4-b4 z_des_Last_Filmtime(length(t_vec)-1)+b2/2+b4-r*4-b4 z_des_Last_Filmtime(length(t_vec)-1)+b2/2+b4-r*4-b4 z_des_Last_Filmtime(length(t_vec)-1)+b2/2-r*4-b4],TUMBlue,'EdgeColor','none');     
        patch([t_vec(end-1)+b3/2+0.3*b-b4+0.1 t_vec(end-1)+b3/2+0.3*b+0.1 t_vec(end-1)-b3/2+0.3*b+0.1 t_vec(end-1)-b3/2+0.3*b-b4+0.1],[z_des_Last_Filmtime(length(t_vec)-1)+b2/2-r*4-b4 z_des_Last_Filmtime(length(t_vec)-1)+b2/2+b4-r*4-b4 z_des_Last_Filmtime(length(t_vec)-1)+b2/2+b4-r*4-b4 z_des_Last_Filmtime(length(t_vec)-1)+b2/2-r*4-b4],TUMBlue,'EdgeColor','none');
        patch([t_vec(end-1)+b3/2-0.3*b-b4+0.1 t_vec(end-1)+b3/2-0.3*b+0.1 t_vec(end-1)-b3/2-0.3*b+0.1 t_vec(end-1)-b3/2-0.3*b-b4+0.1],[z_des_Last_Filmtime(length(t_vec)-1)+b2/2-r*4-b4 z_des_Last_Filmtime(length(t_vec)-1)+b2/2+b4-r*4-b4 z_des_Last_Filmtime(length(t_vec)-1)+b2/2+b4-r*4-b4 z_des_Last_Filmtime(length(t_vec)-1)+b2/2-r*4-b4],TUMBlue,'EdgeColor','none');
        
        %PD-Regler
        curr_distance_wall = abs(z_des_Last_Filmtime(length(t_vec)-1)+b2/2-r*4)-abs(z_NUM_Last_Filmtime(length(t_vec)-1)-r)-b2/2;
        curr_distance_wall_spring = curr_distance_wall + (r-r*cos(asin(hd/r))) + b2;
        % Damper
        patch([t_vec(end-1)+hd-h/4 t_vec(end-1)+hd+h/4 t_vec(end-1)+hd+h/4 t_vec(end-1)+hd-h/4],[z_des_Last_Filmtime(length(t_vec)-1)+b2/2-r*4 z_des_Last_Filmtime(length(t_vec)-1)+b2/2-r*4 z_des_Last_Filmtime(length(t_vec)-1)+b2/2-r*4+1/2*curr_distance_wall z_des_Last_Filmtime(length(t_vec)-1)+b2/2-r*4+1/2*curr_distance_wall],TUMBlue,'EdgeColor','none')
        damper_width = 7/8; %of hd
        inner_damper_width = 0.4;
        patch([t_vec(end-1)+hd-hd*damper_width t_vec(end-1)+hd+hd*damper_width t_vec(end-1)+hd+hd*damper_width t_vec(end-1)+hd-hd*damper_width],[z_des_Last_Filmtime(length(t_vec)-1)+b2/2-r*4+1/2*curr_distance_wall z_des_Last_Filmtime(length(t_vec)-1)+b2/2-r*4+1/2*curr_distance_wall z_des_Last_Filmtime(length(t_vec)-1)+b2/2-r*4+0.6*curr_distance_wall z_des_Last_Filmtime(length(t_vec)-1)+b2/2-r*4+0.6*curr_distance_wall],TUMBlue,'EdgeColor','none')
        patch([t_vec(end-1)+hd-hd*damper_width t_vec(end-1)+hd-hd*damper_width+h/2 t_vec(end-1)+hd-hd*damper_width+h/2 t_vec(end-1)+hd-hd*damper_width],[z_des_Last_Filmtime(length(t_vec)-1)+b2/2-r*4+0.6*curr_distance_wall z_des_Last_Filmtime(length(t_vec)-1)+b2/2-r*4+0.6*curr_distance_wall z_des_Last_Filmtime(length(t_vec)-1)+b2/2-r*4+3/4*curr_distance_wall z_des_Last_Filmtime(length(t_vec)-1)+b2/2-r*4+3/4*curr_distance_wall],TUMBlue,'EdgeColor','none')
        patch([t_vec(end-1)+hd+hd*damper_width-h/2 t_vec(end-1)+hd+hd*damper_width t_vec(end-1)+hd+hd*damper_width t_vec(end-1)+hd+hd*damper_width-h/2],[z_des_Last_Filmtime(length(t_vec)-1)+b2/2-r*4+0.6*curr_distance_wall z_des_Last_Filmtime(length(t_vec)-1)+b2/2-r*4+0.6*curr_distance_wall z_des_Last_Filmtime(length(t_vec)-1)+b2/2-r*4+3/4*curr_distance_wall z_des_Last_Filmtime(length(t_vec)-1)+b2/2-r*4+3/4*curr_distance_wall],TUMBlue,'EdgeColor','none')  
        patch([t_vec(end-1)+hd-hd*inner_damper_width t_vec(end-1)+hd+hd*inner_damper_width t_vec(end-1)+hd+hd*inner_damper_width t_vec(end-1)+hd-hd*inner_damper_width],[z_des_Last_Filmtime(length(t_vec)-1)+b2/2-r*4+2/3*curr_distance_wall z_des_Last_Filmtime(length(t_vec)-1)+b2/2-r*4+2/3*curr_distance_wall z_des_Last_Filmtime(length(t_vec)-1)+b2/2-r*4+3/4*curr_distance_wall z_des_Last_Filmtime(length(t_vec)-1)+b2/2-r*4+3/4*curr_distance_wall],TUMBlue,'EdgeColor','none')
        patch([t_vec(end-1)+hd-h/4 t_vec(end-1)+hd+h/4 t_vec(end-1)+hd+h/4 t_vec(end-1)+hd-h/4],[z_des_Last_Filmtime(length(t_vec)-1)+b2/2-r*4+3/4*curr_distance_wall z_des_Last_Filmtime(length(t_vec)-1)+b2/2-r*4+3/4*curr_distance_wall z_NUM_Last_Filmtime(length(t_vec)-1) z_NUM_Last_Filmtime(length(t_vec)-1)],TUMBlue,'EdgeColor','none')
        
        % Feder Kp
        patch([t_vec(end-1)-hd-h/2 t_vec(end-1)-hd+h/2 t_vec(end-1)-hd+h/2 t_vec(end-1)-hd-h/2],            [z_des_Last_Filmtime(length(t_vec)-1)+b2/2-4*r                               z_des_Last_Filmtime(length(t_vec)-1)+b2/2-4*r                                z_des_Last_Filmtime(length(t_vec)-1)+b2/2-4*r+0.1*curr_distance_wall_spring  z_des_Last_Filmtime(length(t_vec)-1)+b2/2-4*r+0.1*curr_distance_wall_spring],TUMBlue,'EdgeColor','none');
        patch([t_vec(end-1)-hd+h/2 t_vec(end-1)-hd+h/2+b6 t_vec(end-1)-hd-h/2+b6 t_vec(end-1)-hd-h/2],      [z_des_Last_Filmtime(length(t_vec)-1)+b2/2-4*r+0.1*curr_distance_wall_spring z_des_Last_Filmtime(length(t_vec)-1)+b2/2-4*r+0.2*curr_distance_wall_spring  z_des_Last_Filmtime(length(t_vec)-1)+b2/2-4*r+0.2*curr_distance_wall_spring  z_des_Last_Filmtime(length(t_vec)-1)+b2/2-4*r+0.1*curr_distance_wall_spring],TUMBlue,'EdgeColor','none');
        patch([t_vec(end-1)-hd+h/2+b6 t_vec(end-1)-hd+h/2-b6 t_vec(end-1)-hd-h/2-b6 t_vec(end-1)-hd-h/2+b6],[z_des_Last_Filmtime(length(t_vec)-1)+b2/2-4*r+0.2*curr_distance_wall_spring z_des_Last_Filmtime(length(t_vec)-1)+b2/2-4*r+0.4*curr_distance_wall_spring z_des_Last_Filmtime(length(t_vec)-1)+b2/2-4*r+0.4*curr_distance_wall_spring  z_des_Last_Filmtime(length(t_vec)-1)+b2/2-4*r+0.2*curr_distance_wall_spring],TUMBlue,'EdgeColor','none');
        patch([t_vec(end-1)-hd+h/2-b6 t_vec(end-1)-hd+h/2+b6 t_vec(end-1)-hd-h/2+b6 t_vec(end-1)-hd-h/2-b6],[z_des_Last_Filmtime(length(t_vec)-1)+b2/2-4*r+0.4*curr_distance_wall_spring z_des_Last_Filmtime(length(t_vec)-1)+b2/2-4*r+0.6*curr_distance_wall_spring z_des_Last_Filmtime(length(t_vec)-1)+b2/2-4*r+0.6*curr_distance_wall_spring  z_des_Last_Filmtime(length(t_vec)-1)+b2/2-4*r+0.4*curr_distance_wall_spring],TUMBlue,'EdgeColor','none');
        patch([t_vec(end-1)-hd+h/2+b6 t_vec(end-1)-hd+h/2-b6 t_vec(end-1)-hd-h/2-b6 t_vec(end-1)-hd-h/2+b6],[z_des_Last_Filmtime(length(t_vec)-1)+b2/2-4*r+0.6*curr_distance_wall_spring z_des_Last_Filmtime(length(t_vec)-1)+b2/2-4*r+0.8*curr_distance_wall_spring z_des_Last_Filmtime(length(t_vec)-1)+b2/2-4*r+0.8*curr_distance_wall_spring  z_des_Last_Filmtime(length(t_vec)-1)+b2/2-4*r+0.6*curr_distance_wall_spring],TUMBlue,'EdgeColor','none');
        patch([t_vec(end-1)-hd+h/2-b6 t_vec(end-1)-hd+h/2    t_vec(end-1)-hd-h/2    t_vec(end-1)-hd-h/2-b6],[z_des_Last_Filmtime(length(t_vec)-1)+b2/2-4*r+0.8*curr_distance_wall_spring z_des_Last_Filmtime(length(t_vec)-1)+b2/2-4*r+0.9*curr_distance_wall_spring z_des_Last_Filmtime(length(t_vec)-1)+b2/2-4*r+0.9*curr_distance_wall_spring  z_des_Last_Filmtime(length(t_vec)-1)+b2/2-4*r+0.8*curr_distance_wall_spring],TUMBlue,'EdgeColor','none');
        patch([t_vec(end-1)-hd+h/2    t_vec(end-1)-hd+h/2    t_vec(end-1)-hd-h/2    t_vec(end-1)-hd-h/2],   [z_des_Last_Filmtime(length(t_vec)-1)+b2/2-4*r+0.9*curr_distance_wall_spring z_des_Last_Filmtime(length(t_vec)-1)+b2/2-4*r+curr_distance_wall_spring     z_des_Last_Filmtime(length(t_vec)-1)+b2/2-4*r+curr_distance_wall_spring      z_des_Last_Filmtime(length(t_vec)-1)+b2/2-4*r+0.9*curr_distance_wall_spring],TUMBlue,'EdgeColor','none');

        %Ground at 0
        patch([t_vec(end-1)-1.5,          t_vec(end-1)+1.5,          t_vec(end-1)+1.5,          t_vec(end-1)-1.5],[0 0 0.15 0.15],TUMGreen,'EdgeColor','none')
        patch([t_vec(end-1)-0.075-0.1     t_vec(end-1)+0.075-0.1     t_vec(end-1)+0.5+0.075-0.1 t_vec(end-1)+0.5-0.075-0.1],[0.15 0.15 0.65 0.65],TUMGreen,'EdgeColor','none')
        patch([t_vec(end-1)-0.075-0.5-0.1 t_vec(end-1)+0.075-0.5-0.1 t_vec(end-1)+0.075-0.1     t_vec(end-1)-0.075-0.1],[0.15 0.15 0.65 0.65],TUMGreen,'EdgeColor','none')
        patch([t_vec(end-1)-0.075+0.5-0.1 t_vec(end-1)+0.075+0.5-0.1 t_vec(end-1)+1+0.075-0.1     t_vec(end-1)+1-0.075-0.1],[0.15 0.15 0.65 0.65],TUMGreen,'EdgeColor','none')
        patch([t_vec(end-1)-0.075-1-0.1   t_vec(end-1)+0.075-1-0.1   t_vec(end-1)+0.075-0.1-0.5 t_vec(end-1)-0.075-0.1-0.5],[0.15 0.15 0.65 0.65],TUMGreen,'EdgeColor','none')
        patch([t_vec(end-1)-0.075+1-0.1   t_vec(end-1)+0.075+1-0.1   t_vec(end-1)+1.5+0.075-0.1 t_vec(end-1)+1.5-0.075-0.1],[0.15 0.15 0.65 0.65],TUMGreen,'EdgeColor','none')
        
        %Reference for last iteration   
		transparency = 1;
        draw_circle(t_vec(idx),z_NUM_Ref_Filmtime(idx),r,TUMBlack,transparency);
        plot(t_vec(1:idx),z_NUM_Ref_Filmtime(1:idx)+r, '--', 'Color',TUMBlack)
        draw_circle(t_vec(idx), z_EXP_Ref_Filmtime(idx),r,TUMBlack,transparency);
        axis equal       
        
        % spring kE
        curr_distance = abs(z_NUM_Ref_Filmtime(idx)+r)-abs(z_EXP_Ref_Filmtime(idx)-r);
        patch([t_vec(idx)-h/2 t_vec(idx)+h/2 t_vec(idx)+h/2 t_vec(idx)-h/2],            [z_NUM_Ref_Filmtime(idx)+r z_NUM_Ref_Filmtime(idx)+r z_NUM_Ref_Filmtime(idx)+r+ 0.1*curr_distance z_NUM_Ref_Filmtime(idx)+r+ 0.1*curr_distance],TUMBlack,'EdgeColor','none','FaceAlpha',transparency);
        patch([t_vec(idx)+h/2 t_vec(idx)+h/2+b5 t_vec(idx)-h/2+b5 t_vec(idx)-h/2],      [z_NUM_Ref_Filmtime(idx)+r+0.1*curr_distance z_NUM_Ref_Filmtime(idx)+r+0.15*curr_distance z_NUM_Ref_Filmtime(idx)+r+0.15*curr_distance z_NUM_Ref_Filmtime(idx)+r+0.1*curr_distance],TUMBlack,'EdgeColor','none','FaceAlpha',transparency);
        patch([t_vec(idx)+h/2+b5 t_vec(idx)+h/2-b5 t_vec(idx)-h/2-b5 t_vec(idx)-h/2+b5],[z_NUM_Ref_Filmtime(idx)+r+0.15*curr_distance z_NUM_Ref_Filmtime(idx)+r+0.2*curr_distance z_NUM_Ref_Filmtime(idx)+r+0.2*curr_distance z_NUM_Ref_Filmtime(idx)+r+0.15*curr_distance],TUMBlack,'EdgeColor','none','FaceAlpha',transparency);
        patch([t_vec(idx)+h/2-b5 t_vec(idx)+h/2+b5 t_vec(idx)-h/2+b5 t_vec(idx)-h/2-b5],[z_NUM_Ref_Filmtime(idx)+r+0.2*curr_distance z_NUM_Ref_Filmtime(idx)+r+0.25*curr_distance z_NUM_Ref_Filmtime(idx)+r+0.25*curr_distance z_NUM_Ref_Filmtime(idx)+r+0.2*curr_distance],TUMBlack,'EdgeColor','none','FaceAlpha',transparency);
        patch([t_vec(idx)+h/2+b5 t_vec(idx)+h/2-b5 t_vec(idx)-h/2-b5 t_vec(idx)-h/2+b5],[z_NUM_Ref_Filmtime(idx)+r+0.25*curr_distance z_NUM_Ref_Filmtime(idx)+r+0.3*curr_distance z_NUM_Ref_Filmtime(idx)+r+0.3*curr_distance z_NUM_Ref_Filmtime(idx)+r+0.25*curr_distance],TUMBlack,'EdgeColor','none','FaceAlpha',transparency);      
        patch([t_vec(idx)+h/2-b5 t_vec(idx)+h/2+b5 t_vec(idx)-h/2+b5 t_vec(idx)-h/2-b5],[z_NUM_Ref_Filmtime(idx)+r+0.3*curr_distance z_NUM_Ref_Filmtime(idx)+r+0.35*curr_distance z_NUM_Ref_Filmtime(idx)+r+0.35*curr_distance z_NUM_Ref_Filmtime(idx)+r+0.3*curr_distance],TUMBlack,'EdgeColor','none','FaceAlpha',transparency);
        patch([t_vec(idx)+h/2+b5 t_vec(idx)+h/2-b5 t_vec(idx)-h/2-b5 t_vec(idx)-h/2+b5],[z_NUM_Ref_Filmtime(idx)+r+0.35*curr_distance z_NUM_Ref_Filmtime(idx)+r+0.4*curr_distance z_NUM_Ref_Filmtime(idx)+r+0.4*curr_distance z_NUM_Ref_Filmtime(idx)+r+0.35*curr_distance],TUMBlack,'EdgeColor','none','FaceAlpha',transparency);
        patch([t_vec(idx)+h/2-b5 t_vec(idx)+h/2+b5 t_vec(idx)-h/2+b5 t_vec(idx)-h/2-b5],[z_NUM_Ref_Filmtime(idx)+r+0.4*curr_distance z_NUM_Ref_Filmtime(idx)+r+0.45*curr_distance z_NUM_Ref_Filmtime(idx)+r+0.45*curr_distance z_NUM_Ref_Filmtime(idx)+r+0.4*curr_distance],TUMBlack,'EdgeColor','none','FaceAlpha',transparency);
        patch([t_vec(idx)+h/2+b5 t_vec(idx)+h/2-b5 t_vec(idx)-h/2-b5 t_vec(idx)-h/2+b5],[z_NUM_Ref_Filmtime(idx)+r+0.45*curr_distance z_NUM_Ref_Filmtime(idx)+r+0.5*curr_distance z_NUM_Ref_Filmtime(idx)+r+0.5*curr_distance z_NUM_Ref_Filmtime(idx)+r+0.45*curr_distance],TUMBlack,'EdgeColor','none','FaceAlpha',transparency); 
        patch([t_vec(idx)+h/2-b5 t_vec(idx)+h/2+b5 t_vec(idx)-h/2+b5 t_vec(idx)-h/2-b5],[z_NUM_Ref_Filmtime(idx)+r+0.5*curr_distance z_NUM_Ref_Filmtime(idx)+r+0.55*curr_distance z_NUM_Ref_Filmtime(idx)+r+0.55*curr_distance z_NUM_Ref_Filmtime(idx)+r+0.5*curr_distance],TUMBlack,'EdgeColor','none','FaceAlpha',transparency);
        patch([t_vec(idx)+h/2+b5 t_vec(idx)+h/2-b5 t_vec(idx)-h/2-b5 t_vec(idx)-h/2+b5],[z_NUM_Ref_Filmtime(idx)+r+0.55*curr_distance z_NUM_Ref_Filmtime(idx)+r+0.6*curr_distance z_NUM_Ref_Filmtime(idx)+r+0.6*curr_distance z_NUM_Ref_Filmtime(idx)+r+0.55*curr_distance],TUMBlack,'EdgeColor','none','FaceAlpha',transparency);
        patch([t_vec(idx)+h/2-b5 t_vec(idx)+h/2+b5 t_vec(idx)-h/2+b5 t_vec(idx)-h/2-b5],[z_NUM_Ref_Filmtime(idx)+r+0.6*curr_distance z_NUM_Ref_Filmtime(idx)+r+0.65*curr_distance z_NUM_Ref_Filmtime(idx)+r+0.65*curr_distance z_NUM_Ref_Filmtime(idx)+r+0.6*curr_distance],TUMBlack,'EdgeColor','none','FaceAlpha',transparency);
        patch([t_vec(idx)+h/2+b5 t_vec(idx)+h/2-b5 t_vec(idx)-h/2-b5 t_vec(idx)-h/2+b5],[z_NUM_Ref_Filmtime(idx)+r+0.65*curr_distance z_NUM_Ref_Filmtime(idx)+r+0.7*curr_distance z_NUM_Ref_Filmtime(idx)+r+0.7*curr_distance z_NUM_Ref_Filmtime(idx)+r+0.65*curr_distance],TUMBlack,'EdgeColor','none','FaceAlpha',transparency);
        patch([t_vec(idx)+h/2-b5 t_vec(idx)+h/2+b5 t_vec(idx)-h/2+b5 t_vec(idx)-h/2-b5],[z_NUM_Ref_Filmtime(idx)+r+0.7*curr_distance z_NUM_Ref_Filmtime(idx)+r+0.75*curr_distance z_NUM_Ref_Filmtime(idx)+r+0.75*curr_distance z_NUM_Ref_Filmtime(idx)+r+0.7*curr_distance],TUMBlack,'EdgeColor','none','FaceAlpha',transparency);
        patch([t_vec(idx)+h/2+b5 t_vec(idx)+h/2-b5 t_vec(idx)-h/2-b5 t_vec(idx)-h/2+b5],[z_NUM_Ref_Filmtime(idx)+r+0.75*curr_distance z_NUM_Ref_Filmtime(idx)+r+0.8*curr_distance z_NUM_Ref_Filmtime(idx)+r+0.8*curr_distance z_NUM_Ref_Filmtime(idx)+r+0.75*curr_distance],TUMBlack,'EdgeColor','none','FaceAlpha',transparency);
        patch([t_vec(idx)+h/2-b5 t_vec(idx)+h/2+b5 t_vec(idx)-h/2+b5 t_vec(idx)-h/2-b5],[z_NUM_Ref_Filmtime(idx)+r+0.8*curr_distance z_NUM_Ref_Filmtime(idx)+r+0.85*curr_distance z_NUM_Ref_Filmtime(idx)+r+0.85*curr_distance z_NUM_Ref_Filmtime(idx)+r+0.8*curr_distance],TUMBlack,'EdgeColor','none','FaceAlpha',transparency);
        patch([t_vec(idx)+h/2+b5 t_vec(idx)+h/2-b5 t_vec(idx)-h/2-b5 t_vec(idx)-h/2+b5],[z_NUM_Ref_Filmtime(idx)+r+0.85*curr_distance z_NUM_Ref_Filmtime(idx)+r+0.9*curr_distance z_NUM_Ref_Filmtime(idx)+r+0.9*curr_distance z_NUM_Ref_Filmtime(idx)+r+0.85*curr_distance],TUMBlack,'EdgeColor','none','FaceAlpha',transparency);
        patch([t_vec(idx)+h/2-b5 t_vec(idx)+h/2    t_vec(idx)-h/2    t_vec(idx)-h/2-b5],[z_NUM_Ref_Filmtime(idx)+r+0.9*curr_distance z_NUM_Ref_Filmtime(idx)+r+0.95*curr_distance z_NUM_Ref_Filmtime(idx)+r+0.95*curr_distance z_NUM_Ref_Filmtime(idx)+r+0.9*curr_distance],TUMBlack,'EdgeColor','none','FaceAlpha',transparency);
        patch([t_vec(idx)+h/2    t_vec(idx)+h/2    t_vec(idx)-h/2    t_vec(idx)-h/2],   [z_NUM_Ref_Filmtime(idx)+r+0.95*curr_distance z_NUM_Ref_Filmtime(idx)+r+curr_distance z_NUM_Ref_Filmtime(idx)+r+curr_distance z_NUM_Ref_Filmtime(idx)+r+0.95*curr_distance],TUMBlack,'EdgeColor','none','FaceAlpha',transparency);
        
        %wall
        patch([t_vec(idx)+b/2 t_vec(idx)+b/2 t_vec(idx)-b/2 t_vec(idx)-b/2],[z_NUM_Ref_Filmtime_2(idx)-b2/2-r*4 z_NUM_Ref_Filmtime_2(idx)+b2/2-r*4 z_NUM_Ref_Filmtime_2(idx)+b2/2-r*4 z_NUM_Ref_Filmtime_2(idx)-b2/2-r*4],TUMBlack,'EdgeColor','none','FaceAlpha',transparency); 
        
        %wall diagonals
        patch([t_vec(idx)+b3/2-b4+0.1 t_vec(idx)+b3/2+0.1 t_vec(idx)-b3/2+0.1 t_vec(idx)-b3/2-b4+0.1],[z_NUM_Ref_Filmtime_2(idx)+b2/2-r*4-b4 z_NUM_Ref_Filmtime_2(idx)+b2/2+b4-r*4-b4 z_NUM_Ref_Filmtime_2(idx)+b2/2+b4-r*4-b4 z_NUM_Ref_Filmtime_2(idx)+b2/2-r*4-b4],TUMBlack,'EdgeColor','none','FaceAlpha',transparency);     
        patch([t_vec(idx)+b3/2+0.3*b-b4+0.1 t_vec(idx)+b3/2+0.3*b+0.1 t_vec(idx)-b3/2+0.3*b+0.1 t_vec(idx)-b3/2+0.3*b-b4+0.1],[z_NUM_Ref_Filmtime_2(idx)+b2/2-r*4-b4 z_NUM_Ref_Filmtime_2(idx)+b2/2+b4-r*4-b4 z_NUM_Ref_Filmtime_2(idx)+b2/2+b4-r*4-b4 z_NUM_Ref_Filmtime_2(idx)+b2/2-r*4-b4],TUMBlack,'EdgeColor','none','FaceAlpha',transparency);
        patch([t_vec(idx)+b3/2-0.3*b-b4+0.1 t_vec(idx)+b3/2-0.3*b+0.1 t_vec(idx)-b3/2-0.3*b+0.1 t_vec(idx)-b3/2-0.3*b-b4+0.1],[z_NUM_Ref_Filmtime_2(idx)+b2/2-r*4-b4 z_NUM_Ref_Filmtime_2(idx)+b2/2+b4-r*4-b4 z_NUM_Ref_Filmtime_2(idx)+b2/2+b4-r*4-b4 z_NUM_Ref_Filmtime_2(idx)+b2/2-r*4-b4],TUMBlack,'EdgeColor','none','FaceAlpha',transparency);
       
        %PD-Regulator
        curr_distance_wall = abs(z_NUM_Ref_Filmtime_2(idx)+b2/2-r*4)-abs(z_NUM_Ref_Filmtime(idx)-r)-b2/2;
        curr_distance_wall_spring = curr_distance_wall + (r-r*cos(asin(hd/r))) + b2;
        
        % Damper
        patch([t_vec(idx)+hd-h/4 t_vec(idx)+hd+h/4 t_vec(idx)+hd+h/4 t_vec(idx)+hd-h/4],[z_NUM_Ref_Filmtime_2(idx)+b2/2-r*4 z_NUM_Ref_Filmtime_2(idx)+b2/2-r*4 z_NUM_Ref_Filmtime_2(idx)+b2/2-r*4+1/2*curr_distance_wall z_NUM_Ref_Filmtime_2(idx)+b2/2-r*4+1/2*curr_distance_wall],TUMBlack,'EdgeColor','none','FaceAlpha',transparency)
        damper_width = 7/8; %of hd
        inner_damper_width = 0.4;
        patch([t_vec(idx)+hd-hd*damper_width t_vec(idx)+hd+hd*damper_width t_vec(idx)+hd+hd*damper_width t_vec(idx)+hd-hd*damper_width],[z_NUM_Ref_Filmtime_2(idx)+b2/2-r*4+1/2*curr_distance_wall z_NUM_Ref_Filmtime_2(idx)+b2/2-r*4+1/2*curr_distance_wall z_NUM_Ref_Filmtime_2(idx)+b2/2-r*4+0.6*curr_distance_wall z_NUM_Ref_Filmtime_2(idx)+b2/2-r*4+0.6*curr_distance_wall],TUMBlack,'EdgeColor','none','FaceAlpha',transparency)
        patch([t_vec(idx)+hd-hd*damper_width t_vec(idx)+hd-hd*damper_width+h/2 t_vec(idx)+hd-hd*damper_width+h/2 t_vec(idx)+hd-hd*damper_width],[z_NUM_Ref_Filmtime_2(idx)+b2/2-r*4+0.6*curr_distance_wall z_NUM_Ref_Filmtime_2(idx)+b2/2-r*4+0.6*curr_distance_wall z_NUM_Ref_Filmtime_2(idx)+b2/2-r*4+3/4*curr_distance_wall z_NUM_Ref_Filmtime_2(idx)+b2/2-r*4+3/4*curr_distance_wall],TUMBlack,'EdgeColor','none','FaceAlpha',transparency)
        patch([t_vec(idx)+hd+hd*damper_width-h/2 t_vec(idx)+hd+hd*damper_width t_vec(idx)+hd+hd*damper_width t_vec(idx)+hd+hd*damper_width-h/2],[z_NUM_Ref_Filmtime_2(idx)+b2/2-r*4+0.6*curr_distance_wall z_NUM_Ref_Filmtime_2(idx)+b2/2-r*4+0.6*curr_distance_wall z_NUM_Ref_Filmtime_2(idx)+b2/2-r*4+3/4*curr_distance_wall z_NUM_Ref_Filmtime_2(idx)+b2/2-r*4+3/4*curr_distance_wall],TUMBlack,'EdgeColor','none','FaceAlpha',transparency)  
        patch([t_vec(idx)+hd-hd*inner_damper_width t_vec(idx)+hd+hd*inner_damper_width t_vec(idx)+hd+hd*inner_damper_width t_vec(idx)+hd-hd*inner_damper_width],[z_NUM_Ref_Filmtime_2(idx)+b2/2-r*4+2/3*curr_distance_wall z_NUM_Ref_Filmtime_2(idx)+b2/2-r*4+2/3*curr_distance_wall z_NUM_Ref_Filmtime_2(idx)+b2/2-r*4+3/4*curr_distance_wall z_NUM_Ref_Filmtime_2(idx)+b2/2-r*4+3/4*curr_distance_wall],TUMBlack,'EdgeColor','none','FaceAlpha',transparency)
        patch([t_vec(idx)+hd-h/4 t_vec(idx)+hd+h/4 t_vec(idx)+hd+h/4 t_vec(idx)+hd-h/4],[z_NUM_Ref_Filmtime_2(idx)+b2/2-r*4+3/4*curr_distance_wall z_NUM_Ref_Filmtime_2(idx)+b2/2-r*4+3/4*curr_distance_wall z_NUM_Ref_Filmtime(idx) z_NUM_Ref_Filmtime(idx)],TUMBlack,'EdgeColor','none','FaceAlpha',transparency)
       
        % Feder Kp
        patch([t_vec(idx)-hd-h/2 t_vec(idx)-hd+h/2 t_vec(idx)-hd+h/2 t_vec(idx)-hd-h/2],            [z_NUM_Ref_Filmtime_2(idx)+b2/2-4*r                               z_NUM_Ref_Filmtime_2(idx)+b2/2-4*r                                z_NUM_Ref_Filmtime_2(idx)+b2/2-4*r+0.1*curr_distance_wall_spring  z_NUM_Ref_Filmtime_2(idx)+b2/2-4*r+0.1*curr_distance_wall_spring],TUMBlack,'EdgeColor','none','FaceAlpha',transparency);
        patch([t_vec(idx)-hd+h/2 t_vec(idx)-hd+h/2+b6 t_vec(idx)-hd-h/2+b6 t_vec(idx)-hd-h/2],      [z_NUM_Ref_Filmtime_2(idx)+b2/2-4*r+0.1*curr_distance_wall_spring z_NUM_Ref_Filmtime_2(idx)+b2/2-4*r+0.2*curr_distance_wall_spring  z_NUM_Ref_Filmtime_2(idx)+b2/2-4*r+0.2*curr_distance_wall_spring  z_NUM_Ref_Filmtime_2(idx)+b2/2-4*r+0.1*curr_distance_wall_spring],TUMBlack,'EdgeColor','none','FaceAlpha',transparency);
        patch([t_vec(idx)-hd+h/2+b6 t_vec(idx)-hd+h/2-b6 t_vec(idx)-hd-h/2-b6 t_vec(idx)-hd-h/2+b6],[z_NUM_Ref_Filmtime_2(idx)+b2/2-4*r+0.2*curr_distance_wall_spring z_NUM_Ref_Filmtime_2(idx)+b2/2-4*r+0.4*curr_distance_wall_spring z_NUM_Ref_Filmtime_2(idx)+b2/2-4*r+0.4*curr_distance_wall_spring  z_NUM_Ref_Filmtime_2(idx)+b2/2-4*r+0.2*curr_distance_wall_spring],TUMBlack,'EdgeColor','none','FaceAlpha',transparency);
        patch([t_vec(idx)-hd+h/2-b6 t_vec(idx)-hd+h/2+b6 t_vec(idx)-hd-h/2+b6 t_vec(idx)-hd-h/2-b6],[z_NUM_Ref_Filmtime_2(idx)+b2/2-4*r+0.4*curr_distance_wall_spring z_NUM_Ref_Filmtime_2(idx)+b2/2-4*r+0.6*curr_distance_wall_spring z_NUM_Ref_Filmtime_2(idx)+b2/2-4*r+0.6*curr_distance_wall_spring  z_NUM_Ref_Filmtime_2(idx)+b2/2-4*r+0.4*curr_distance_wall_spring],TUMBlack,'EdgeColor','none','FaceAlpha',transparency);
        patch([t_vec(idx)-hd+h/2+b6 t_vec(idx)-hd+h/2-b6 t_vec(idx)-hd-h/2-b6 t_vec(idx)-hd-h/2+b6],[z_NUM_Ref_Filmtime_2(idx)+b2/2-4*r+0.6*curr_distance_wall_spring z_NUM_Ref_Filmtime_2(idx)+b2/2-4*r+0.8*curr_distance_wall_spring z_NUM_Ref_Filmtime_2(idx)+b2/2-4*r+0.8*curr_distance_wall_spring  z_NUM_Ref_Filmtime_2(idx)+b2/2-4*r+0.6*curr_distance_wall_spring],TUMBlack,'EdgeColor','none','FaceAlpha',transparency);
        patch([t_vec(idx)-hd+h/2-b6 t_vec(idx)-hd+h/2    t_vec(idx)-hd-h/2    t_vec(idx)-hd-h/2-b6],[z_NUM_Ref_Filmtime_2(idx)+b2/2-4*r+0.8*curr_distance_wall_spring z_NUM_Ref_Filmtime_2(idx)+b2/2-4*r+0.9*curr_distance_wall_spring z_NUM_Ref_Filmtime_2(idx)+b2/2-4*r+0.9*curr_distance_wall_spring  z_NUM_Ref_Filmtime_2(idx)+b2/2-4*r+0.8*curr_distance_wall_spring],TUMBlack,'EdgeColor','none','FaceAlpha',transparency);
        patch([t_vec(idx)-hd+h/2    t_vec(idx)-hd+h/2    t_vec(idx)-hd-h/2    t_vec(idx)-hd-h/2],   [z_NUM_Ref_Filmtime_2(idx)+b2/2-4*r+0.9*curr_distance_wall_spring z_NUM_Ref_Filmtime_2(idx)+b2/2-4*r+curr_distance_wall_spring     z_NUM_Ref_Filmtime_2(idx)+b2/2-4*r+curr_distance_wall_spring      z_NUM_Ref_Filmtime_2(idx)+b2/2-4*r+0.9*curr_distance_wall_spring],TUMBlack,'EdgeColor','none','FaceAlpha',transparency);

        %Ground at 0
        patch([t_vec(idx)-1.5,          t_vec(idx)+1.5,          t_vec(idx)+1.5,          t_vec(idx)-1.5],[0 0 0.15 0.15],TUMBlack,'EdgeColor','none','FaceAlpha',transparency)
        patch([t_vec(idx)-0.075-0.1     t_vec(idx)+0.075-0.1     t_vec(idx)+0.5+0.075-0.1 t_vec(idx)+0.5-0.075-0.1],[0.15 0.15 0.65 0.65],TUMBlack,'EdgeColor','none','FaceAlpha',transparency)
        patch([t_vec(idx)-0.075-0.5-0.1 t_vec(idx)+0.075-0.5-0.1 t_vec(idx)+0.075-0.1     t_vec(idx)-0.075-0.1],[0.15 0.15 0.65 0.65],TUMBlack,'EdgeColor','none','FaceAlpha',transparency)
        patch([t_vec(idx)-0.075+0.5-0.1 t_vec(idx)+0.075+0.5-0.1 t_vec(idx)+1+0.075-0.1     t_vec(idx)+1-0.075-0.1],[0.15 0.15 0.65 0.65],TUMBlack,'EdgeColor','none','FaceAlpha',transparency)
        patch([t_vec(idx)-0.075-1-0.1   t_vec(idx)+0.075-1-0.1   t_vec(idx)+0.075-0.1-0.5 t_vec(idx)-0.075-0.1-0.5],[0.15 0.15 0.65 0.65],TUMBlack,'EdgeColor','none','FaceAlpha',transparency)
        patch([t_vec(idx)-0.075+1-0.1   t_vec(idx)+0.075+1-0.1   t_vec(idx)+1.5+0.075-0.1 t_vec(idx)+1.5-0.075-0.1],[0.15 0.15 0.65 0.65],TUMBlack,'EdgeColor','none','FaceAlpha',transparency)
       
        xlabel('Time / s','Interpreter','Latex')
        ylabel('Displacement / mm','Interpreter','Latex')
        xticks([0 1 2 3 4])
        set(gca,'Fontsize',18)
        ylim([-12 1])
        xlim([-1.5 5.5])
        title(lastIter)

        drawnow
        F=getframe(gcf);
        writeVideo(v,F);
    end
for k=1:60
        drawnow
        F=getframe(gcf);
        writeVideo(v,F);
end
%% Plot for zoomed version
     for idx = 1:(length(t_vec)-1)
        clf
        figure(8)
        subplot(2,2,1);     %First Iteration
        hold on
        syms x y

        plot(t_vec(1:idx),z_NUM_First_Filmtime(1:idx)+r,'Color',[0 0.4 0.74])
        draw_circle(t_vec(idx),z_NUM_First_Filmtime(idx),r,TUMBlue,1);
      
        %wall
        patch([t_vec(idx)+b/2 t_vec(idx)+b/2 t_vec(idx)-b/2 t_vec(idx)-b/2],[z_des_First_Filmtime(idx)-b2/2-r*4 z_des_First_Filmtime(idx)+b2/2-r*4 z_des_First_Filmtime(idx)+b2/2-r*4 z_des_First_Filmtime(idx)-b2/2-r*4],TUMBlue,'EdgeColor','none'); 
        
        %wall diagonals
        patch([t_vec(idx)+b3/2-b4+0.1 t_vec(idx)+b3/2+0.1 t_vec(idx)-b3/2+0.1 t_vec(idx)-b3/2-b4+0.1],[z_des_First_Filmtime(idx)+b2/2-r*4-b4 z_des_First_Filmtime(idx)+b2/2+b4-r*4-b4 z_des_First_Filmtime(idx)+b2/2+b4-r*4-b4 z_des_First_Filmtime(idx)+b2/2-r*4-b4],TUMBlue,'EdgeColor','none');     
        patch([t_vec(idx)+b3/2+0.3*b-b4+0.1 t_vec(idx)+b3/2+0.3*b+0.1 t_vec(idx)-b3/2+0.3*b+0.1 t_vec(idx)-b3/2+0.3*b-b4+0.1],[z_des_First_Filmtime(idx)+b2/2-r*4-b4 z_des_First_Filmtime(idx)+b2/2+b4-r*4-b4 z_des_First_Filmtime(idx)+b2/2+b4-r*4-b4 z_des_First_Filmtime(idx)+b2/2-r*4-b4],TUMBlue,'EdgeColor','none');
        patch([t_vec(idx)+b3/2-0.3*b-b4+0.1 t_vec(idx)+b3/2-0.3*b+0.1 t_vec(idx)-b3/2-0.3*b+0.1 t_vec(idx)-b3/2-0.3*b-b4+0.1],[z_des_First_Filmtime(idx)+b2/2-r*4-b4 z_des_First_Filmtime(idx)+b2/2+b4-r*4-b4 z_des_First_Filmtime(idx)+b2/2+b4-r*4-b4 z_des_First_Filmtime(idx)+b2/2-r*4-b4],TUMBlue,'EdgeColor','none');
       
        %PD-Regulator
        curr_distance_wall = abs(z_des_First_Filmtime(idx)+b2/2-r*4)-abs(z_NUM_First_Filmtime(idx)-r)-b2/2;
        curr_distance_wall_spring = curr_distance_wall + (r-r*cos(asin(hd/r))) + b2;
        
        % Damper
        patch([t_vec(idx)+hd-h/4 t_vec(idx)+hd+h/4 t_vec(idx)+hd+h/4 t_vec(idx)+hd-h/4],[z_des_First_Filmtime(idx)+b2/2-r*4 z_des_First_Filmtime(idx)+b2/2-r*4 z_des_First_Filmtime(idx)+b2/2-r*4+1/2*curr_distance_wall z_des_First_Filmtime(idx)+b2/2-r*4+1/2*curr_distance_wall],TUMBlue,'EdgeColor','none')
        damper_width = 7/8; %of hd
        inner_damper_width = 0.4;
        patch([t_vec(idx)+hd-hd*damper_width t_vec(idx)+hd+hd*damper_width t_vec(idx)+hd+hd*damper_width t_vec(idx)+hd-hd*damper_width],[z_des_First_Filmtime(idx)+b2/2-r*4+1/2*curr_distance_wall z_des_First_Filmtime(idx)+b2/2-r*4+1/2*curr_distance_wall z_des_First_Filmtime(idx)+b2/2-r*4+0.6*curr_distance_wall z_des_First_Filmtime(idx)+b2/2-r*4+0.6*curr_distance_wall],TUMBlue,'EdgeColor','none')
        patch([t_vec(idx)+hd-hd*damper_width t_vec(idx)+hd-hd*damper_width+h/2 t_vec(idx)+hd-hd*damper_width+h/2 t_vec(idx)+hd-hd*damper_width],[z_des_First_Filmtime(idx)+b2/2-r*4+0.6*curr_distance_wall z_des_First_Filmtime(idx)+b2/2-r*4+0.6*curr_distance_wall z_des_First_Filmtime(idx)+b2/2-r*4+3/4*curr_distance_wall z_des_First_Filmtime(idx)+b2/2-r*4+3/4*curr_distance_wall],TUMBlue,'EdgeColor','none')
        patch([t_vec(idx)+hd+hd*damper_width-h/2 t_vec(idx)+hd+hd*damper_width t_vec(idx)+hd+hd*damper_width t_vec(idx)+hd+hd*damper_width-h/2],[z_des_First_Filmtime(idx)+b2/2-r*4+0.6*curr_distance_wall z_des_First_Filmtime(idx)+b2/2-r*4+0.6*curr_distance_wall z_des_First_Filmtime(idx)+b2/2-r*4+3/4*curr_distance_wall z_des_First_Filmtime(idx)+b2/2-r*4+3/4*curr_distance_wall],TUMBlue,'EdgeColor','none')  
        patch([t_vec(idx)+hd-hd*inner_damper_width t_vec(idx)+hd+hd*inner_damper_width t_vec(idx)+hd+hd*inner_damper_width t_vec(idx)+hd-hd*inner_damper_width],[z_des_First_Filmtime(idx)+b2/2-r*4+2/3*curr_distance_wall z_des_First_Filmtime(idx)+b2/2-r*4+2/3*curr_distance_wall z_des_First_Filmtime(idx)+b2/2-r*4+3/4*curr_distance_wall z_des_First_Filmtime(idx)+b2/2-r*4+3/4*curr_distance_wall],TUMBlue,'EdgeColor','none')
        patch([t_vec(idx)+hd-h/4 t_vec(idx)+hd+h/4 t_vec(idx)+hd+h/4 t_vec(idx)+hd-h/4],[z_des_First_Filmtime(idx)+b2/2-r*4+3/4*curr_distance_wall z_des_First_Filmtime(idx)+b2/2-r*4+3/4*curr_distance_wall z_NUM_First_Filmtime(idx) z_NUM_First_Filmtime(idx)],TUMBlue,'EdgeColor','none')
        
        % Feder Kp
        patch([t_vec(idx)-hd-h/2 t_vec(idx)-hd+h/2 t_vec(idx)-hd+h/2 t_vec(idx)-hd-h/2],            [z_des_First_Filmtime(idx)+b2/2-4*r                               z_des_First_Filmtime(idx)+b2/2-4*r                                z_des_First_Filmtime(idx)+b2/2-4*r+0.1*curr_distance_wall_spring  z_des_First_Filmtime(idx)+b2/2-4*r+0.1*curr_distance_wall_spring],TUMBlue,'EdgeColor','none');
        patch([t_vec(idx)-hd+h/2 t_vec(idx)-hd+h/2+b6 t_vec(idx)-hd-h/2+b6 t_vec(idx)-hd-h/2],      [z_des_First_Filmtime(idx)+b2/2-4*r+0.1*curr_distance_wall_spring z_des_First_Filmtime(idx)+b2/2-4*r+0.2*curr_distance_wall_spring  z_des_First_Filmtime(idx)+b2/2-4*r+0.2*curr_distance_wall_spring  z_des_First_Filmtime(idx)+b2/2-4*r+0.1*curr_distance_wall_spring],TUMBlue,'EdgeColor','none');
        patch([t_vec(idx)-hd+h/2+b6 t_vec(idx)-hd+h/2-b6 t_vec(idx)-hd-h/2-b6 t_vec(idx)-hd-h/2+b6],[z_des_First_Filmtime(idx)+b2/2-4*r+0.2*curr_distance_wall_spring z_des_First_Filmtime(idx)+b2/2-4*r+0.4*curr_distance_wall_spring z_des_First_Filmtime(idx)+b2/2-4*r+0.4*curr_distance_wall_spring  z_des_First_Filmtime(idx)+b2/2-4*r+0.2*curr_distance_wall_spring],TUMBlue,'EdgeColor','none');
        patch([t_vec(idx)-hd+h/2-b6 t_vec(idx)-hd+h/2+b6 t_vec(idx)-hd-h/2+b6 t_vec(idx)-hd-h/2-b6],[z_des_First_Filmtime(idx)+b2/2-4*r+0.4*curr_distance_wall_spring z_des_First_Filmtime(idx)+b2/2-4*r+0.6*curr_distance_wall_spring z_des_First_Filmtime(idx)+b2/2-4*r+0.6*curr_distance_wall_spring  z_des_First_Filmtime(idx)+b2/2-4*r+0.4*curr_distance_wall_spring],TUMBlue,'EdgeColor','none');
        patch([t_vec(idx)-hd+h/2+b6 t_vec(idx)-hd+h/2-b6 t_vec(idx)-hd-h/2-b6 t_vec(idx)-hd-h/2+b6],[z_des_First_Filmtime(idx)+b2/2-4*r+0.6*curr_distance_wall_spring z_des_First_Filmtime(idx)+b2/2-4*r+0.8*curr_distance_wall_spring z_des_First_Filmtime(idx)+b2/2-4*r+0.8*curr_distance_wall_spring  z_des_First_Filmtime(idx)+b2/2-4*r+0.6*curr_distance_wall_spring],TUMBlue,'EdgeColor','none');
        patch([t_vec(idx)-hd+h/2-b6 t_vec(idx)-hd+h/2    t_vec(idx)-hd-h/2    t_vec(idx)-hd-h/2-b6],[z_des_First_Filmtime(idx)+b2/2-4*r+0.8*curr_distance_wall_spring z_des_First_Filmtime(idx)+b2/2-4*r+0.9*curr_distance_wall_spring z_des_First_Filmtime(idx)+b2/2-4*r+0.9*curr_distance_wall_spring  z_des_First_Filmtime(idx)+b2/2-4*r+0.8*curr_distance_wall_spring],TUMBlue,'EdgeColor','none');
        patch([t_vec(idx)-hd+h/2    t_vec(idx)-hd+h/2    t_vec(idx)-hd-h/2    t_vec(idx)-hd-h/2],   [z_des_First_Filmtime(idx)+b2/2-4*r+0.9*curr_distance_wall_spring z_des_First_Filmtime(idx)+b2/2-4*r+curr_distance_wall_spring     z_des_First_Filmtime(idx)+b2/2-4*r+curr_distance_wall_spring      z_des_First_Filmtime(idx)+b2/2-4*r+0.9*curr_distance_wall_spring],TUMBlue,'EdgeColor','none');

        %Reference for first iteration
		transparency = 0.3;
        draw_circle(t_vec(idx),z_NUM_Ref_Filmtime(idx),r,TUMBlack,transparency);
        plot(t_vec(1:idx),z_NUM_Ref_Filmtime(1:idx)+r, '--', 'Color',TUMBlack)
        axis equal       
       
        %wall
        patch([t_vec(idx)+b/2 t_vec(idx)+b/2 t_vec(idx)-b/2 t_vec(idx)-b/2],[z_NUM_Ref_Filmtime_2(idx)-b2/2-r*4 z_NUM_Ref_Filmtime_2(idx)+b2/2-r*4 z_NUM_Ref_Filmtime_2(idx)+b2/2-r*4 z_NUM_Ref_Filmtime_2(idx)-b2/2-r*4],TUMBlack,'EdgeColor','none','FaceAlpha',transparency); 
       
        %wall diagonals
        patch([t_vec(idx)+b3/2-b4+0.1 t_vec(idx)+b3/2+0.1 t_vec(idx)-b3/2+0.1 t_vec(idx)-b3/2-b4+0.1],[z_NUM_Ref_Filmtime_2(idx)+b2/2-r*4-b4 z_NUM_Ref_Filmtime_2(idx)+b2/2+b4-r*4-b4 z_NUM_Ref_Filmtime_2(idx)+b2/2+b4-r*4-b4 z_NUM_Ref_Filmtime_2(idx)+b2/2-r*4-b4],TUMBlack,'EdgeColor','none','FaceAlpha',transparency);     
        patch([t_vec(idx)+b3/2+0.3*b-b4+0.1 t_vec(idx)+b3/2+0.3*b+0.1 t_vec(idx)-b3/2+0.3*b+0.1 t_vec(idx)-b3/2+0.3*b-b4+0.1],[z_NUM_Ref_Filmtime_2(idx)+b2/2-r*4-b4 z_NUM_Ref_Filmtime_2(idx)+b2/2+b4-r*4-b4 z_NUM_Ref_Filmtime_2(idx)+b2/2+b4-r*4-b4 z_NUM_Ref_Filmtime_2(idx)+b2/2-r*4-b4],TUMBlack,'EdgeColor','none','FaceAlpha',transparency);
        patch([t_vec(idx)+b3/2-0.3*b-b4+0.1 t_vec(idx)+b3/2-0.3*b+0.1 t_vec(idx)-b3/2-0.3*b+0.1 t_vec(idx)-b3/2-0.3*b-b4+0.1],[z_NUM_Ref_Filmtime_2(idx)+b2/2-r*4-b4 z_NUM_Ref_Filmtime_2(idx)+b2/2+b4-r*4-b4 z_NUM_Ref_Filmtime_2(idx)+b2/2+b4-r*4-b4 z_NUM_Ref_Filmtime_2(idx)+b2/2-r*4-b4],TUMBlack,'EdgeColor','none','FaceAlpha',transparency);
       
        %PD-Regulator
        curr_distance_wall = abs(z_NUM_Ref_Filmtime_2(idx)+b2/2-r*4)-abs(z_NUM_Ref_Filmtime(idx)-r)-b2/2;
        curr_distance_wall_spring = curr_distance_wall + (r-r*cos(asin(hd/r))) + b2;
        
        % Damper
        patch([t_vec(idx)+hd-h/4 t_vec(idx)+hd+h/4 t_vec(idx)+hd+h/4 t_vec(idx)+hd-h/4],[z_NUM_Ref_Filmtime_2(idx)+b2/2-r*4 z_NUM_Ref_Filmtime_2(idx)+b2/2-r*4 z_NUM_Ref_Filmtime_2(idx)+b2/2-r*4+1/2*curr_distance_wall z_NUM_Ref_Filmtime_2(idx)+b2/2-r*4+1/2*curr_distance_wall],TUMBlack,'EdgeColor','none','FaceAlpha',transparency)
        damper_width = 7/8; %of hd
        inner_damper_width = 0.4;
        patch([t_vec(idx)+hd-hd*damper_width t_vec(idx)+hd+hd*damper_width t_vec(idx)+hd+hd*damper_width t_vec(idx)+hd-hd*damper_width],[z_NUM_Ref_Filmtime_2(idx)+b2/2-r*4+1/2*curr_distance_wall z_NUM_Ref_Filmtime_2(idx)+b2/2-r*4+1/2*curr_distance_wall z_NUM_Ref_Filmtime_2(idx)+b2/2-r*4+0.6*curr_distance_wall z_NUM_Ref_Filmtime_2(idx)+b2/2-r*4+0.6*curr_distance_wall],TUMBlack,'EdgeColor','none','FaceAlpha',transparency)
        patch([t_vec(idx)+hd-hd*damper_width t_vec(idx)+hd-hd*damper_width+h/2 t_vec(idx)+hd-hd*damper_width+h/2 t_vec(idx)+hd-hd*damper_width],[z_NUM_Ref_Filmtime_2(idx)+b2/2-r*4+0.6*curr_distance_wall z_NUM_Ref_Filmtime_2(idx)+b2/2-r*4+0.6*curr_distance_wall z_NUM_Ref_Filmtime_2(idx)+b2/2-r*4+3/4*curr_distance_wall z_NUM_Ref_Filmtime_2(idx)+b2/2-r*4+3/4*curr_distance_wall],TUMBlack,'EdgeColor','none','FaceAlpha',transparency)
        patch([t_vec(idx)+hd+hd*damper_width-h/2 t_vec(idx)+hd+hd*damper_width t_vec(idx)+hd+hd*damper_width t_vec(idx)+hd+hd*damper_width-h/2],[z_NUM_Ref_Filmtime_2(idx)+b2/2-r*4+0.6*curr_distance_wall z_NUM_Ref_Filmtime_2(idx)+b2/2-r*4+0.6*curr_distance_wall z_NUM_Ref_Filmtime_2(idx)+b2/2-r*4+3/4*curr_distance_wall z_NUM_Ref_Filmtime_2(idx)+b2/2-r*4+3/4*curr_distance_wall],TUMBlack,'EdgeColor','none','FaceAlpha',transparency)  
        patch([t_vec(idx)+hd-hd*inner_damper_width t_vec(idx)+hd+hd*inner_damper_width t_vec(idx)+hd+hd*inner_damper_width t_vec(idx)+hd-hd*inner_damper_width],[z_NUM_Ref_Filmtime_2(idx)+b2/2-r*4+2/3*curr_distance_wall z_NUM_Ref_Filmtime_2(idx)+b2/2-r*4+2/3*curr_distance_wall z_NUM_Ref_Filmtime_2(idx)+b2/2-r*4+3/4*curr_distance_wall z_NUM_Ref_Filmtime_2(idx)+b2/2-r*4+3/4*curr_distance_wall],TUMBlack,'EdgeColor','none','FaceAlpha',transparency)
        patch([t_vec(idx)+hd-h/4 t_vec(idx)+hd+h/4 t_vec(idx)+hd+h/4 t_vec(idx)+hd-h/4],[z_NUM_Ref_Filmtime_2(idx)+b2/2-r*4+3/4*curr_distance_wall z_NUM_Ref_Filmtime_2(idx)+b2/2-r*4+3/4*curr_distance_wall z_NUM_Ref_Filmtime(idx) z_NUM_Ref_Filmtime(idx)],TUMBlack,'EdgeColor','none','FaceAlpha',transparency)
        
        % Feder Kp
        patch([t_vec(idx)-hd-h/2 t_vec(idx)-hd+h/2 t_vec(idx)-hd+h/2 t_vec(idx)-hd-h/2],            [z_NUM_Ref_Filmtime_2(idx)+b2/2-4*r                               z_NUM_Ref_Filmtime_2(idx)+b2/2-4*r                                z_NUM_Ref_Filmtime_2(idx)+b2/2-4*r+0.1*curr_distance_wall_spring  z_NUM_Ref_Filmtime_2(idx)+b2/2-4*r+0.1*curr_distance_wall_spring],TUMBlack,'EdgeColor','none','FaceAlpha',transparency);
        patch([t_vec(idx)-hd+h/2 t_vec(idx)-hd+h/2+b6 t_vec(idx)-hd-h/2+b6 t_vec(idx)-hd-h/2],      [z_NUM_Ref_Filmtime_2(idx)+b2/2-4*r+0.1*curr_distance_wall_spring z_NUM_Ref_Filmtime_2(idx)+b2/2-4*r+0.2*curr_distance_wall_spring  z_NUM_Ref_Filmtime_2(idx)+b2/2-4*r+0.2*curr_distance_wall_spring  z_NUM_Ref_Filmtime_2(idx)+b2/2-4*r+0.1*curr_distance_wall_spring],TUMBlack,'EdgeColor','none','FaceAlpha',transparency);
        patch([t_vec(idx)-hd+h/2+b6 t_vec(idx)-hd+h/2-b6 t_vec(idx)-hd-h/2-b6 t_vec(idx)-hd-h/2+b6],[z_NUM_Ref_Filmtime_2(idx)+b2/2-4*r+0.2*curr_distance_wall_spring z_NUM_Ref_Filmtime_2(idx)+b2/2-4*r+0.4*curr_distance_wall_spring z_NUM_Ref_Filmtime_2(idx)+b2/2-4*r+0.4*curr_distance_wall_spring  z_NUM_Ref_Filmtime_2(idx)+b2/2-4*r+0.2*curr_distance_wall_spring],TUMBlack,'EdgeColor','none','FaceAlpha',transparency);
        patch([t_vec(idx)-hd+h/2-b6 t_vec(idx)-hd+h/2+b6 t_vec(idx)-hd-h/2+b6 t_vec(idx)-hd-h/2-b6],[z_NUM_Ref_Filmtime_2(idx)+b2/2-4*r+0.4*curr_distance_wall_spring z_NUM_Ref_Filmtime_2(idx)+b2/2-4*r+0.6*curr_distance_wall_spring z_NUM_Ref_Filmtime_2(idx)+b2/2-4*r+0.6*curr_distance_wall_spring  z_NUM_Ref_Filmtime_2(idx)+b2/2-4*r+0.4*curr_distance_wall_spring],TUMBlack,'EdgeColor','none','FaceAlpha',transparency);
        patch([t_vec(idx)-hd+h/2+b6 t_vec(idx)-hd+h/2-b6 t_vec(idx)-hd-h/2-b6 t_vec(idx)-hd-h/2+b6],[z_NUM_Ref_Filmtime_2(idx)+b2/2-4*r+0.6*curr_distance_wall_spring z_NUM_Ref_Filmtime_2(idx)+b2/2-4*r+0.8*curr_distance_wall_spring z_NUM_Ref_Filmtime_2(idx)+b2/2-4*r+0.8*curr_distance_wall_spring  z_NUM_Ref_Filmtime_2(idx)+b2/2-4*r+0.6*curr_distance_wall_spring],TUMBlack,'EdgeColor','none','FaceAlpha',transparency);
        patch([t_vec(idx)-hd+h/2-b6 t_vec(idx)-hd+h/2    t_vec(idx)-hd-h/2    t_vec(idx)-hd-h/2-b6],[z_NUM_Ref_Filmtime_2(idx)+b2/2-4*r+0.8*curr_distance_wall_spring z_NUM_Ref_Filmtime_2(idx)+b2/2-4*r+0.9*curr_distance_wall_spring z_NUM_Ref_Filmtime_2(idx)+b2/2-4*r+0.9*curr_distance_wall_spring  z_NUM_Ref_Filmtime_2(idx)+b2/2-4*r+0.8*curr_distance_wall_spring],TUMBlack,'EdgeColor','none','FaceAlpha',transparency);
        patch([t_vec(idx)-hd+h/2    t_vec(idx)-hd+h/2    t_vec(idx)-hd-h/2    t_vec(idx)-hd-h/2],   [z_NUM_Ref_Filmtime_2(idx)+b2/2-4*r+0.9*curr_distance_wall_spring z_NUM_Ref_Filmtime_2(idx)+b2/2-4*r+curr_distance_wall_spring     z_NUM_Ref_Filmtime_2(idx)+b2/2-4*r+curr_distance_wall_spring      z_NUM_Ref_Filmtime_2(idx)+b2/2-4*r+0.9*curr_distance_wall_spring],TUMBlack,'EdgeColor','none','FaceAlpha',transparency);

        xlabel('Time / s','Interpreter','Latex')
        ylabel('Displacement / mm','Interpreter','Latex')
        xticks([0 1 2 3 4])
        set(gca,'Fontsize',18)
        ylim([-10 -6])
        xlim([-1.5 5.5])
        title('Iteration 1')
        
        subplot(2,2,2);     %Last Iteration
        hold on
        syms x y

        plot(t_vec(1:idx),z_NUM_Last_Filmtime(1:idx)+r,'Color',[0 0.4 0.74]) 
        draw_circle(t_vec(idx),z_NUM_Last_Filmtime(idx),r,TUMBlue,1);
     
        %wall
        patch([t_vec(idx)+b/2 t_vec(idx)+b/2 t_vec(idx)-b/2 t_vec(idx)-b/2],[z_des_Last_Filmtime(idx)-b2/2-r*4 z_des_Last_Filmtime(idx)+b2/2-r*4 z_des_Last_Filmtime(idx)+b2/2-r*4 z_des_Last_Filmtime(idx)-b2/2-r*4],TUMBlue,'EdgeColor','none'); 
        
        %wall diagonals
        patch([t_vec(idx)+b3/2-b4+0.1 t_vec(idx)+b3/2+0.1 t_vec(idx)-b3/2+0.1 t_vec(idx)-b3/2-b4+0.1],[z_des_Last_Filmtime(idx)+b2/2-r*4-b4 z_des_Last_Filmtime(idx)+b2/2+b4-r*4-b4 z_des_Last_Filmtime(idx)+b2/2+b4-r*4-b4 z_des_Last_Filmtime(idx)+b2/2-r*4-b4],TUMBlue,'EdgeColor','none');     
        patch([t_vec(idx)+b3/2+0.3*b-b4+0.1 t_vec(idx)+b3/2+0.3*b+0.1 t_vec(idx)-b3/2+0.3*b+0.1 t_vec(idx)-b3/2+0.3*b-b4+0.1],[z_des_Last_Filmtime(idx)+b2/2-r*4-b4 z_des_Last_Filmtime(idx)+b2/2+b4-r*4-b4 z_des_Last_Filmtime(idx)+b2/2+b4-r*4-b4 z_des_Last_Filmtime(idx)+b2/2-r*4-b4],TUMBlue,'EdgeColor','none');
        patch([t_vec(idx)+b3/2-0.3*b-b4+0.1 t_vec(idx)+b3/2-0.3*b+0.1 t_vec(idx)-b3/2-0.3*b+0.1 t_vec(idx)-b3/2-0.3*b-b4+0.1],[z_des_Last_Filmtime(idx)+b2/2-r*4-b4 z_des_Last_Filmtime(idx)+b2/2+b4-r*4-b4 z_des_Last_Filmtime(idx)+b2/2+b4-r*4-b4 z_des_Last_Filmtime(idx)+b2/2-r*4-b4],TUMBlue,'EdgeColor','none');
       
        %PD-Regulator
        curr_distance_wall = abs(z_des_Last_Filmtime(idx)+b2/2-r*4)-abs(z_NUM_Last_Filmtime(idx)-r)-b2/2;
        curr_distance_wall_spring = curr_distance_wall + (r-r*cos(asin(hd/r))) + b2;
        
        % Damper
        patch([t_vec(idx)+hd-h/4 t_vec(idx)+hd+h/4 t_vec(idx)+hd+h/4 t_vec(idx)+hd-h/4],[z_des_Last_Filmtime(idx)+b2/2-r*4 z_des_Last_Filmtime(idx)+b2/2-r*4 z_des_Last_Filmtime(idx)+b2/2-r*4+1/2*curr_distance_wall z_des_Last_Filmtime(idx)+b2/2-r*4+1/2*curr_distance_wall],TUMBlue,'EdgeColor','none')
        damper_width = 7/8; %of hd
        inner_damper_width = 0.4;
        patch([t_vec(idx)+hd-hd*damper_width t_vec(idx)+hd+hd*damper_width t_vec(idx)+hd+hd*damper_width t_vec(idx)+hd-hd*damper_width],[z_des_Last_Filmtime(idx)+b2/2-r*4+1/2*curr_distance_wall z_des_Last_Filmtime(idx)+b2/2-r*4+1/2*curr_distance_wall z_des_Last_Filmtime(idx)+b2/2-r*4+0.6*curr_distance_wall z_des_Last_Filmtime(idx)+b2/2-r*4+0.6*curr_distance_wall],TUMBlue,'EdgeColor','none')
        patch([t_vec(idx)+hd-hd*damper_width t_vec(idx)+hd-hd*damper_width+h/2 t_vec(idx)+hd-hd*damper_width+h/2 t_vec(idx)+hd-hd*damper_width],[z_des_Last_Filmtime(idx)+b2/2-r*4+0.6*curr_distance_wall z_des_Last_Filmtime(idx)+b2/2-r*4+0.6*curr_distance_wall z_des_Last_Filmtime(idx)+b2/2-r*4+3/4*curr_distance_wall z_des_Last_Filmtime(idx)+b2/2-r*4+3/4*curr_distance_wall],TUMBlue,'EdgeColor','none')
        patch([t_vec(idx)+hd+hd*damper_width-h/2 t_vec(idx)+hd+hd*damper_width t_vec(idx)+hd+hd*damper_width t_vec(idx)+hd+hd*damper_width-h/2],[z_des_Last_Filmtime(idx)+b2/2-r*4+0.6*curr_distance_wall z_des_Last_Filmtime(idx)+b2/2-r*4+0.6*curr_distance_wall z_des_Last_Filmtime(idx)+b2/2-r*4+3/4*curr_distance_wall z_des_Last_Filmtime(idx)+b2/2-r*4+3/4*curr_distance_wall],TUMBlue,'EdgeColor','none')  
        patch([t_vec(idx)+hd-hd*inner_damper_width t_vec(idx)+hd+hd*inner_damper_width t_vec(idx)+hd+hd*inner_damper_width t_vec(idx)+hd-hd*inner_damper_width],[z_des_Last_Filmtime(idx)+b2/2-r*4+2/3*curr_distance_wall z_des_Last_Filmtime(idx)+b2/2-r*4+2/3*curr_distance_wall z_des_Last_Filmtime(idx)+b2/2-r*4+3/4*curr_distance_wall z_des_Last_Filmtime(idx)+b2/2-r*4+3/4*curr_distance_wall],TUMBlue,'EdgeColor','none')
        patch([t_vec(idx)+hd-h/4 t_vec(idx)+hd+h/4 t_vec(idx)+hd+h/4 t_vec(idx)+hd-h/4],[z_des_Last_Filmtime(idx)+b2/2-r*4+3/4*curr_distance_wall z_des_Last_Filmtime(idx)+b2/2-r*4+3/4*curr_distance_wall z_NUM_Last_Filmtime(idx) z_NUM_Last_Filmtime(idx)],TUMBlue,'EdgeColor','none')
        
        % Feder Kp
        patch([t_vec(idx)-hd-h/2 t_vec(idx)-hd+h/2 t_vec(idx)-hd+h/2 t_vec(idx)-hd-h/2],            [z_des_Last_Filmtime(idx)+b2/2-4*r                               z_des_Last_Filmtime(idx)+b2/2-4*r                                z_des_Last_Filmtime(idx)+b2/2-4*r+0.1*curr_distance_wall_spring  z_des_Last_Filmtime(idx)+b2/2-4*r+0.1*curr_distance_wall_spring],TUMBlue,'EdgeColor','none');
        patch([t_vec(idx)-hd+h/2 t_vec(idx)-hd+h/2+b6 t_vec(idx)-hd-h/2+b6 t_vec(idx)-hd-h/2],      [z_des_Last_Filmtime(idx)+b2/2-4*r+0.1*curr_distance_wall_spring z_des_Last_Filmtime(idx)+b2/2-4*r+0.2*curr_distance_wall_spring  z_des_Last_Filmtime(idx)+b2/2-4*r+0.2*curr_distance_wall_spring  z_des_Last_Filmtime(idx)+b2/2-4*r+0.1*curr_distance_wall_spring],TUMBlue,'EdgeColor','none');
        patch([t_vec(idx)-hd+h/2+b6 t_vec(idx)-hd+h/2-b6 t_vec(idx)-hd-h/2-b6 t_vec(idx)-hd-h/2+b6],[z_des_Last_Filmtime(idx)+b2/2-4*r+0.2*curr_distance_wall_spring z_des_Last_Filmtime(idx)+b2/2-4*r+0.4*curr_distance_wall_spring z_des_Last_Filmtime(idx)+b2/2-4*r+0.4*curr_distance_wall_spring  z_des_Last_Filmtime(idx)+b2/2-4*r+0.2*curr_distance_wall_spring],TUMBlue,'EdgeColor','none');
        patch([t_vec(idx)-hd+h/2-b6 t_vec(idx)-hd+h/2+b6 t_vec(idx)-hd-h/2+b6 t_vec(idx)-hd-h/2-b6],[z_des_Last_Filmtime(idx)+b2/2-4*r+0.4*curr_distance_wall_spring z_des_Last_Filmtime(idx)+b2/2-4*r+0.6*curr_distance_wall_spring z_des_Last_Filmtime(idx)+b2/2-4*r+0.6*curr_distance_wall_spring  z_des_Last_Filmtime(idx)+b2/2-4*r+0.4*curr_distance_wall_spring],TUMBlue,'EdgeColor','none');
        patch([t_vec(idx)-hd+h/2+b6 t_vec(idx)-hd+h/2-b6 t_vec(idx)-hd-h/2-b6 t_vec(idx)-hd-h/2+b6],[z_des_Last_Filmtime(idx)+b2/2-4*r+0.6*curr_distance_wall_spring z_des_Last_Filmtime(idx)+b2/2-4*r+0.8*curr_distance_wall_spring z_des_Last_Filmtime(idx)+b2/2-4*r+0.8*curr_distance_wall_spring  z_des_Last_Filmtime(idx)+b2/2-4*r+0.6*curr_distance_wall_spring],TUMBlue,'EdgeColor','none');
        patch([t_vec(idx)-hd+h/2-b6 t_vec(idx)-hd+h/2    t_vec(idx)-hd-h/2    t_vec(idx)-hd-h/2-b6],[z_des_Last_Filmtime(idx)+b2/2-4*r+0.8*curr_distance_wall_spring z_des_Last_Filmtime(idx)+b2/2-4*r+0.9*curr_distance_wall_spring z_des_Last_Filmtime(idx)+b2/2-4*r+0.9*curr_distance_wall_spring  z_des_Last_Filmtime(idx)+b2/2-4*r+0.8*curr_distance_wall_spring],TUMBlue,'EdgeColor','none');
        patch([t_vec(idx)-hd+h/2    t_vec(idx)-hd+h/2    t_vec(idx)-hd-h/2    t_vec(idx)-hd-h/2],   [z_des_Last_Filmtime(idx)+b2/2-4*r+0.9*curr_distance_wall_spring z_des_Last_Filmtime(idx)+b2/2-4*r+curr_distance_wall_spring     z_des_Last_Filmtime(idx)+b2/2-4*r+curr_distance_wall_spring      z_des_Last_Filmtime(idx)+b2/2-4*r+0.9*curr_distance_wall_spring],TUMBlue,'EdgeColor','none');
        
		transparency = 0.3;
        draw_circle(t_vec(idx),z_NUM_Ref_Filmtime(idx),r,TUMBlack,transparency);
        plot(t_vec(1:idx),z_NUM_Ref_Filmtime(1:idx)+r, '--', 'Color',TUMBlack)
        axis equal       
      
        %wall
        patch([t_vec(idx)+b/2 t_vec(idx)+b/2 t_vec(idx)-b/2 t_vec(idx)-b/2],[z_NUM_Ref_Filmtime_2(idx)-b2/2-r*4 z_NUM_Ref_Filmtime_2(idx)+b2/2-r*4 z_NUM_Ref_Filmtime_2(idx)+b2/2-r*4 z_NUM_Ref_Filmtime_2(idx)-b2/2-r*4],TUMBlack,'EdgeColor','none','FaceAlpha',transparency); 
        
        %wall diagonals
        patch([t_vec(idx)+b3/2-b4+0.1 t_vec(idx)+b3/2+0.1 t_vec(idx)-b3/2+0.1 t_vec(idx)-b3/2-b4+0.1],[z_NUM_Ref_Filmtime_2(idx)+b2/2-r*4-b4 z_NUM_Ref_Filmtime_2(idx)+b2/2+b4-r*4-b4 z_NUM_Ref_Filmtime_2(idx)+b2/2+b4-r*4-b4 z_NUM_Ref_Filmtime_2(idx)+b2/2-r*4-b4],TUMBlack,'EdgeColor','none','FaceAlpha',transparency);     
        patch([t_vec(idx)+b3/2+0.3*b-b4+0.1 t_vec(idx)+b3/2+0.3*b+0.1 t_vec(idx)-b3/2+0.3*b+0.1 t_vec(idx)-b3/2+0.3*b-b4+0.1],[z_NUM_Ref_Filmtime_2(idx)+b2/2-r*4-b4 z_NUM_Ref_Filmtime_2(idx)+b2/2+b4-r*4-b4 z_NUM_Ref_Filmtime_2(idx)+b2/2+b4-r*4-b4 z_NUM_Ref_Filmtime_2(idx)+b2/2-r*4-b4],TUMBlack,'EdgeColor','none','FaceAlpha',transparency);
        patch([t_vec(idx)+b3/2-0.3*b-b4+0.1 t_vec(idx)+b3/2-0.3*b+0.1 t_vec(idx)-b3/2-0.3*b+0.1 t_vec(idx)-b3/2-0.3*b-b4+0.1],[z_NUM_Ref_Filmtime_2(idx)+b2/2-r*4-b4 z_NUM_Ref_Filmtime_2(idx)+b2/2+b4-r*4-b4 z_NUM_Ref_Filmtime_2(idx)+b2/2+b4-r*4-b4 z_NUM_Ref_Filmtime_2(idx)+b2/2-r*4-b4],TUMBlack,'EdgeColor','none','FaceAlpha',transparency);
        
        %PD-Regler
        curr_distance_wall = abs(z_NUM_Ref_Filmtime_2(idx)+b2/2-r*4)-abs(z_NUM_Ref_Filmtime(idx)-r)-b2/2;
        curr_distance_wall_spring = curr_distance_wall + (r-r*cos(asin(hd/r))) + b2;
        
        % Damper
        patch([t_vec(idx)+hd-h/4 t_vec(idx)+hd+h/4 t_vec(idx)+hd+h/4 t_vec(idx)+hd-h/4],[z_NUM_Ref_Filmtime_2(idx)+b2/2-r*4 z_NUM_Ref_Filmtime_2(idx)+b2/2-r*4 z_NUM_Ref_Filmtime_2(idx)+b2/2-r*4+1/2*curr_distance_wall z_NUM_Ref_Filmtime_2(idx)+b2/2-r*4+1/2*curr_distance_wall],TUMBlack,'EdgeColor','none','FaceAlpha',transparency)
        damper_width = 7/8; %of hd
        inner_damper_width = 0.4;
        patch([t_vec(idx)+hd-hd*damper_width t_vec(idx)+hd+hd*damper_width t_vec(idx)+hd+hd*damper_width t_vec(idx)+hd-hd*damper_width],[z_NUM_Ref_Filmtime_2(idx)+b2/2-r*4+1/2*curr_distance_wall z_NUM_Ref_Filmtime_2(idx)+b2/2-r*4+1/2*curr_distance_wall z_NUM_Ref_Filmtime_2(idx)+b2/2-r*4+0.6*curr_distance_wall z_NUM_Ref_Filmtime_2(idx)+b2/2-r*4+0.6*curr_distance_wall],TUMBlack,'EdgeColor','none','FaceAlpha',transparency)
        patch([t_vec(idx)+hd-hd*damper_width t_vec(idx)+hd-hd*damper_width+h/2 t_vec(idx)+hd-hd*damper_width+h/2 t_vec(idx)+hd-hd*damper_width],[z_NUM_Ref_Filmtime_2(idx)+b2/2-r*4+0.6*curr_distance_wall z_NUM_Ref_Filmtime_2(idx)+b2/2-r*4+0.6*curr_distance_wall z_NUM_Ref_Filmtime_2(idx)+b2/2-r*4+3/4*curr_distance_wall z_NUM_Ref_Filmtime_2(idx)+b2/2-r*4+3/4*curr_distance_wall],TUMBlack,'EdgeColor','none','FaceAlpha',transparency)
        patch([t_vec(idx)+hd+hd*damper_width-h/2 t_vec(idx)+hd+hd*damper_width t_vec(idx)+hd+hd*damper_width t_vec(idx)+hd+hd*damper_width-h/2],[z_NUM_Ref_Filmtime_2(idx)+b2/2-r*4+0.6*curr_distance_wall z_NUM_Ref_Filmtime_2(idx)+b2/2-r*4+0.6*curr_distance_wall z_NUM_Ref_Filmtime_2(idx)+b2/2-r*4+3/4*curr_distance_wall z_NUM_Ref_Filmtime_2(idx)+b2/2-r*4+3/4*curr_distance_wall],TUMBlack,'EdgeColor','none','FaceAlpha',transparency)  
        patch([t_vec(idx)+hd-hd*inner_damper_width t_vec(idx)+hd+hd*inner_damper_width t_vec(idx)+hd+hd*inner_damper_width t_vec(idx)+hd-hd*inner_damper_width],[z_NUM_Ref_Filmtime_2(idx)+b2/2-r*4+2/3*curr_distance_wall z_NUM_Ref_Filmtime_2(idx)+b2/2-r*4+2/3*curr_distance_wall z_NUM_Ref_Filmtime_2(idx)+b2/2-r*4+3/4*curr_distance_wall z_NUM_Ref_Filmtime_2(idx)+b2/2-r*4+3/4*curr_distance_wall],TUMBlack,'EdgeColor','none','FaceAlpha',transparency)
        patch([t_vec(idx)+hd-h/4 t_vec(idx)+hd+h/4 t_vec(idx)+hd+h/4 t_vec(idx)+hd-h/4],[z_NUM_Ref_Filmtime_2(idx)+b2/2-r*4+3/4*curr_distance_wall z_NUM_Ref_Filmtime_2(idx)+b2/2-r*4+3/4*curr_distance_wall z_NUM_Ref_Filmtime(idx) z_NUM_Ref_Filmtime(idx)],TUMBlack,'EdgeColor','none','FaceAlpha',transparency)
        
        % Feder Kp
        patch([t_vec(idx)-hd-h/2 t_vec(idx)-hd+h/2 t_vec(idx)-hd+h/2 t_vec(idx)-hd-h/2],            [z_NUM_Ref_Filmtime_2(idx)+b2/2-4*r                               z_NUM_Ref_Filmtime_2(idx)+b2/2-4*r                                z_NUM_Ref_Filmtime_2(idx)+b2/2-4*r+0.1*curr_distance_wall_spring  z_NUM_Ref_Filmtime_2(idx)+b2/2-4*r+0.1*curr_distance_wall_spring],TUMBlack,'EdgeColor','none','FaceAlpha',transparency);
        patch([t_vec(idx)-hd+h/2 t_vec(idx)-hd+h/2+b6 t_vec(idx)-hd-h/2+b6 t_vec(idx)-hd-h/2],      [z_NUM_Ref_Filmtime_2(idx)+b2/2-4*r+0.1*curr_distance_wall_spring z_NUM_Ref_Filmtime_2(idx)+b2/2-4*r+0.2*curr_distance_wall_spring  z_NUM_Ref_Filmtime_2(idx)+b2/2-4*r+0.2*curr_distance_wall_spring  z_NUM_Ref_Filmtime_2(idx)+b2/2-4*r+0.1*curr_distance_wall_spring],TUMBlack,'EdgeColor','none','FaceAlpha',transparency);
        patch([t_vec(idx)-hd+h/2+b6 t_vec(idx)-hd+h/2-b6 t_vec(idx)-hd-h/2-b6 t_vec(idx)-hd-h/2+b6],[z_NUM_Ref_Filmtime_2(idx)+b2/2-4*r+0.2*curr_distance_wall_spring z_NUM_Ref_Filmtime_2(idx)+b2/2-4*r+0.4*curr_distance_wall_spring z_NUM_Ref_Filmtime_2(idx)+b2/2-4*r+0.4*curr_distance_wall_spring  z_NUM_Ref_Filmtime_2(idx)+b2/2-4*r+0.2*curr_distance_wall_spring],TUMBlack,'EdgeColor','none','FaceAlpha',transparency);
        patch([t_vec(idx)-hd+h/2-b6 t_vec(idx)-hd+h/2+b6 t_vec(idx)-hd-h/2+b6 t_vec(idx)-hd-h/2-b6],[z_NUM_Ref_Filmtime_2(idx)+b2/2-4*r+0.4*curr_distance_wall_spring z_NUM_Ref_Filmtime_2(idx)+b2/2-4*r+0.6*curr_distance_wall_spring z_NUM_Ref_Filmtime_2(idx)+b2/2-4*r+0.6*curr_distance_wall_spring  z_NUM_Ref_Filmtime_2(idx)+b2/2-4*r+0.4*curr_distance_wall_spring],TUMBlack,'EdgeColor','none','FaceAlpha',transparency);
        patch([t_vec(idx)-hd+h/2+b6 t_vec(idx)-hd+h/2-b6 t_vec(idx)-hd-h/2-b6 t_vec(idx)-hd-h/2+b6],[z_NUM_Ref_Filmtime_2(idx)+b2/2-4*r+0.6*curr_distance_wall_spring z_NUM_Ref_Filmtime_2(idx)+b2/2-4*r+0.8*curr_distance_wall_spring z_NUM_Ref_Filmtime_2(idx)+b2/2-4*r+0.8*curr_distance_wall_spring  z_NUM_Ref_Filmtime_2(idx)+b2/2-4*r+0.6*curr_distance_wall_spring],TUMBlack,'EdgeColor','none','FaceAlpha',transparency);
        patch([t_vec(idx)-hd+h/2-b6 t_vec(idx)-hd+h/2    t_vec(idx)-hd-h/2    t_vec(idx)-hd-h/2-b6],[z_NUM_Ref_Filmtime_2(idx)+b2/2-4*r+0.8*curr_distance_wall_spring z_NUM_Ref_Filmtime_2(idx)+b2/2-4*r+0.9*curr_distance_wall_spring z_NUM_Ref_Filmtime_2(idx)+b2/2-4*r+0.9*curr_distance_wall_spring  z_NUM_Ref_Filmtime_2(idx)+b2/2-4*r+0.8*curr_distance_wall_spring],TUMBlack,'EdgeColor','none','FaceAlpha',transparency);
        patch([t_vec(idx)-hd+h/2    t_vec(idx)-hd+h/2    t_vec(idx)-hd-h/2    t_vec(idx)-hd-h/2],   [z_NUM_Ref_Filmtime_2(idx)+b2/2-4*r+0.9*curr_distance_wall_spring z_NUM_Ref_Filmtime_2(idx)+b2/2-4*r+curr_distance_wall_spring     z_NUM_Ref_Filmtime_2(idx)+b2/2-4*r+curr_distance_wall_spring      z_NUM_Ref_Filmtime_2(idx)+b2/2-4*r+0.9*curr_distance_wall_spring],TUMBlack,'EdgeColor','none','FaceAlpha',transparency);

      
        xlabel('Time / s','Interpreter','Latex')
        ylabel('Displacement / mm','Interpreter','Latex')
        xticks([0 1 2 3 4])
        set(gca,'Fontsize',18)
        ylim([-10 -6])
        xlim([-1.5 5.5])
        title(lastIter)

        subplot(2,2,3);     %First Iteration comparison plot
        hold on
        syms x y

        plot(t_vec(1:idx),-z_NUM_First_Filmtime(1:idx)-r,'Color',TUMBlue);
        plot(t_vec(1:idx),-z_NUM_Ref_Filmtime(1:idx)-r, '--', 'Color',TUMBlack);
        xlabel('Time / s','Interpreter','Latex')
        ylabel('Displacement / mm','Interpreter','Latex')
        xticks([0 1 2 3 4])
        set(gca,'Fontsize',18)
        ylim([6.5 8.5])
        xlim([0 t_vec(end)])
        
        subplot(2,2,4);     %Last Iteration comparison plot
        hold on
        syms x y

        plot(t_vec(1:idx),-z_NUM_Last_Filmtime(1:idx)-r,'Color',TUMBlue);
        plot(t_vec(1:idx),-z_NUM_Ref_Filmtime(1:idx)-r, '--', 'Color',TUMBlack);
        xlabel('Time / s','Interpreter','Latex')
        ylabel('Displacement / mm','Interpreter','Latex')
        xticks([0 1 2 3 4])
        set(gca,'Fontsize',18)
        ylim([6.5 8.5])
        xlim([0 t_vec(end)])
        
        drawnow
        F=getframe(gcf);
        writeVideo(v,F);
     end
for k=1:60
    drawnow
    F=getframe(gcf);
    writeVideo(v,F);
end
    
close(v)

% Function to draw a circle
function [] = draw_circle(x,y,r,colorspec,transparency)
th = 0:pi/50:2*pi;
xunit = r*cos(th)+x;
yunit = r*sin(th)+y;
plot(xunit,yunit,'Color',colorspec);
fill(xunit,yunit,colorspec,'FaceAlpha',transparency);
end

