%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Init_Fcn_vRTHS_Contact.m
% - Version: November 24th 2021
% - Tutorial and files created by: Christina Insam, Mehroz Hussain, Arian 
%   Kist, Henri Schwalm and Daniel J. Rixen
% - This script is called automatically by the Simulink model
%   "vRTHS_1D_Contact.slx" at the beginning of a simulation. The variables/
%   parameters are described in the accompanying tutorial.
% - These Matlab/Simulink files implement the methods proposed in "Insam C.,
%   Kist, A., Schwalm, H. and Rixen, D. J. Robust and high fidelity real-
%   time hybrid substructuring. Mechanical Systems and Signal Processing, 
%   157, 2021".
% - These Matlab/Simulink files implement a virtual Real-Time Hybrid 
%   Substructuring experiment of a one-dimensional structure with contact. 
%   The main focus is put on actuator dynamics compensation. The actuator 
%   is a Stewart Platform (six electric motors), which is controlled by a 
%   cascaded feedback controller (position - P controller - and velocity - 
%   PI controller - control loop). The paper proposes Iterative Learning 
%   Control (ILC) as feedforward control scheme and Normalized Passivity 
%   Control (NPC) to monitor the test stability and interfere if stability 
%   is jeopardized.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

clear all; close all; clc

%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                           Parameters to vary                             %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

Iterations              = 15;
modus_FFW               = 3;        %1: Only feedback control: PPI cascade
                                    %2: PPI + Velocity Feedforward (VFF)
                                    %3: PPI + ILC
                                    %4: PPI + ILC + VFF
%Counter
enable_NPC              = 1;        %Switch on NPC: 0=OFF,1=ON
                                
%External Excitation                        
f_des                   = 0.25;     %Frequency of the wall's cosine trajectory [Hz]
max_def                 = 0.005;    %Max. displacement of the upper wall(Delta_z_max in the paper) [m]

%%Feedback controller
K_P_x                   = 20;       %position control loop, P gain [1/s]
K_P_v                   = 0.2;      %velocity control loop, P gain [As/m]	
K_I_v                   = 1;        %velocity control loop, I gain [A/m]

%Normalized Passivity Control
NPC_Gain                = 800;      %Damping scaling value [kg/s]
NPC_max                 = inf;       %Maximal force PC [N]
T_LPF1                  = 0.1;       %time constant of low-pass filter for P_tot [s]
T_LPF2                  = 0.1;       %time constant of low-pass filter for P_error [s]

% Iterative Learning Control
L_p                     = 16;       %P-Gain of ILC [1/s]
L_d                     = 0;        %D-Gain of ILC [-]
Q_Cutoff                = 2;        %Cutoff frequency for ZPF [Hz]

%Dynamical System
g                       = 9.81;     %Gravitational acceleration [m/s^2]
m_num                   = 9.82;     %Numerical mass [kg]
k_num                   = 10000;    %Numerical stiffness [N/m]
d_num                   = 50;       %Numerical Damping [kg/s]

m_exp                   = 0.38;     %Experimental mass [kg]
k_exp                   = 8650;     %Experimental stiffness [N/m]
l0                      = 0.071;    %Resting spring length [m]
h0                      = 0.01;     %Initial height of the experimental mass [m]

%Pause time after an oscillation to establish the starting position 
t_pause                 = 0.5;        %Pause time between two successive iterations [s]

%% 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                 Initialization - please do not change                    %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

SampleTime              = str2num(get_param('vRTHS_1D_Contact','FixedStep'));
q_start                 = [ 0 0 751; 0 0 0];
z_Contact               = 761;      %[mm]
Intervall_Counter       = 0;        %Counts the intervals
alpha_ILC               = 1;        %Scaling factor f_j
if modus_FFW == 3 || modus_FFW == 4
    SimTime             = (1/f_des+t_pause)*Iterations; %[s]
else
    Iterations          = 1;
    SimTime             = 1/f_des;                      %[s]
    
end
delaylength             = (1/f_des+t_pause)/SampleTime; 

%Geometry and Numerical Simulation
l_stat                  = (m_exp*g)/k_exp; %Static deformation of the spring (i.e. length when gravity load is applied) [m]

%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                   Stewart Platform - please do not change                    %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Transfer functions of the Stewart Platform's legs 1-6: Motor voltage -> Actual leg length 

G1_Num                  = 1.011056853766925e+03;                          
G1_Den                  = [1 1.228954058286247e+02 0]; 

G2_Num                  = 1.053149793891977e+03;                          
G2_Den                  = [1 55.462239946523620 0]; 

G3_Num                  = 1.123500146792880e+03;                          
G3_Den                  = [1 54.668586837249360 0]; 

G4_Num                  = 7.809038030450906e+02;                          
G4_Den                  = [1 55.324714146949944 0];

G5_Num                  = 9.980922947046973e+02;                          
G5_Den                  = [1 66.237465688104280 0];

G6_Num                  = 1.070921480346769e+03;                          
G6_Den                  = [1 58.950986714433460 0];

% Initialize Stewart Platform Geometry 
l_Leg_Start             = inversekinematics(q_start)*0.001;

function l_desired_Leg_2   = inversekinematics(q_desired_TCP)

leg_length              = 545.0;    %Leg length when retracted    

alpha_02_desired_R      = q_desired_TCP(2,1);
beta_02_desired_R       = q_desired_TCP(2,2);
gamma_02_desired_R      = q_desired_TCP(2,3);

% Vectors to the centers of the universal joints a_0 and e_1
a_0                     = [[-176.777,  -176.777,  76.0];
                          [ -64.705,  -241.4815, 76.0];
                          [ 241.4815,  -64.705,  76.0];
                          [ 241.4815,   64.705,  76.0];
                          [ -64.705,   241.4815, 76.0];
                          [-176.777,   176.777,  76.0]]';
                        
e_1                     = [[-144.889,	 -38.823,	-76.0]; 
                          [  38.832,	-144.889,	-76.0]; 
                          [ 106.066,	-106.066,	-76.0]; 
                          [ 106.066,	 106.066,	-76.0]; 
                          [  38.832,	 144.889,	-76.0]; 
                          [-144.889,	  38.823,	-76.0]]';
% Specification of the transformation matrices
T_02                    = [[cos(beta_02_desired_R)*cos(gamma_02_desired_R) 
                            -cos(beta_02_desired_R)*sin(gamma_02_desired_R) 
                            sin(beta_02_desired_R)]';
                           [cos(alpha_02_desired_R)*sin(gamma_02_desired_R)+sin(alpha_02_desired_R)*sin(beta_02_desired_R)*cos(gamma_02_desired_R)
                            cos(alpha_02_desired_R) * cos(gamma_02_desired_R) - sin(alpha_02_desired_R) * sin(beta_02_desired_R) * sin(gamma_02_desired_R)
                            -sin(alpha_02_desired_R) * cos(beta_02_desired_R)]';
                           [sin(alpha_02_desired_R) * sin(gamma_02_desired_R) - cos(alpha_02_desired_R) * sin(beta_02_desired_R) * cos(gamma_02_desired_R)
                            sin(alpha_02_desired_R) * cos(gamma_02_desired_R) + cos(alpha_02_desired_R) * sin(beta_02_desired_R) * sin(gamma_02_desired_R)
                            cos(alpha_02_desired_R) * cos(beta_02_desired_R)]'];
    
T_01                    = T_02;     %*T_21;

% Transformation from e_1 to e_0
e_0                     = T_01*e_1;

% Calculation of the leg vectors
b_0                     = [0 0 0 0 0 0; 0 0 0 0 0 0 ;0 0 0 0 0 0];

for i=1:1:6
b_0(:,i)                = q_desired_TCP(1,:)'-a_0(:,i)+e_0(:,i);%-Default_TCP_0_12 (1, :) '; % q (1, :) ... translational position of the TCP
end
b_1                     = T_01'*b_0;

% Sum of the vectors
l_desired_abs_Leg       = [0 0 0 0 0 0];
l_desired_abs_Leg_1     = [0 0 0 0 0 0];

for i=1:1:6
l_desired_abs_Leg(i)    = sqrt(b_0(1,i)^2 + b_0(2,i)^2 + b_0(3,i)^2);
l_desired_abs_Leg_1(i)  = sqrt(b_1(1,i)^2 + b_1(2,i)^2 + b_1(3,i)^2);
end

%Calculation of the spindle hub
l_desired_Leg           = l_desired_abs_Leg-leg_length;

 l_desired_Leg_2        = [l_desired_Leg(1);l_desired_Leg(2);l_desired_Leg(3);l_desired_Leg(4);l_desired_Leg(5);l_desired_Leg(6)];
end
