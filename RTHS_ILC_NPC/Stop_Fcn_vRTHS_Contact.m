%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Stop_Fcn_vRTHS_Contact.m
% - Version: November 24th 2021
% - Tutorial and files created by: Christina Insam, Mehroz Hussain, Arian 
%   Kist, Henri Schwalm and Daniel J. Rixen
% - This script is called automatically by the Simulink model
%   "vRTHS_1D_Contact.slx" when the simulation has terminated successfully.
%   The variables/parameters are described in the accompanying tutorial.
% - These Matlab/Simulink files implement the methods proposed in "Insam C.,
%   Kist, A., Schwalm, H. and Rixen, D. J. Robust and high fidelity real-
%   time hybrid substructuring. Mechanical Systems and Signal Processing, 
%   157, 2021".
% - This script evaluates the results of the virtual RTHS test 
%   (vRTHS_1D_Contact.slx). It creates the same plots as in the paper
%   "Robust and high fidelity real-time hybrid substructuring".
%   Furthermore, an animation of the results can be created. Please do not
%   modify this script.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                    Post processing variables set up                          %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
z0_mV                           =   (h0+l0+l_stat)*1000;                    %Initial upper wall movement[mm]  
e_tcp                           =   e_tcp_sim.Data(1,:);                    %Error in the actually achieved trajectory
z_tcp_actual                    =   z_tcp_actual_sim.Data(:,1)';            %Actual interface displacement z' measured in the reference coordinate system of the Stewart Platform [mm]
zd_tcp_actual                   =   zd_tcp_actual_sim.Data(:,1)';           %Actual velocity z_dot' [m/s]  measured in the reference coordinate system of the Stewart Platform [mm]
z_tcp_desired(1,:)              =   z_tcp_desired_sim.Data(1,1,:);          %Desired interface displacement (z) measured in the reference coordinate system of the Stewart Platform [mm] [mm]
zd_tcp_desired                  =   zd_tcp_desired_sim.Data(:,1)';          %Desired velocity (z_dot) [mm]  measured in the reference coordinate system of the Stewart Platform [mm]
z_NUM_desired_real              =   -(z_tcp_desired-z_tcp_desired(1))+z0_mV;%Desired interface displacement measured in the coordinate system of the numerical simulation [mm]
z_NUM_real                      =   -(z_tcp_actual-z_tcp_actual(1))+z0_mV;  %Actual interface displacement z'_NUM, measured from the ground [mm]
z_NUM_Ref                       =   (Ref_sim.Data(:,1)')*1000+z0_mV;        %Actual referece interface displacement z'NUM_ref, measured from the ground [mm]
e_Ref                           =   z_NUM_real-z_NUM_Ref;                   %Reference error [mm]
ed_tcp                          =   zd_tcp_desired-zd_tcp_actual;           %Velocity error [m/s]
P_error                         =   P_error_Fint.Data(:,1)';                %Error power introduced by the transfer system [W]
int_len                         =   (1/f_des)*1000;                         %Time period/Sampling time 
t_p                             =   t_pause*1000;                           %Pause time/Sampling time

%% 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                             Error calculations                               %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%Creating a separate column for each iteration, from the column matrix
for h=1:Iterations                                     
    u                           =   1+(h-1)*(int_len+t_p);
    o                           =   int_len+(h-1)*(int_len+t_p);
    e_tcp_int(h,:)              =   e_tcp(u:o);                             
    ed_tcp_int(h,:)             =   ed_tcp(u:o);                            
    z_desired_int(h,:)          =   z_tcp_desired(u:o);                     
    zd_desired_int(h,:)         =   zd_tcp_desired(u:o);                    
    e_Ref_int(h,:)              =   e_Ref(u:o);                             
    z_NUM_desired_real_int(h,:) =   z_NUM_desired_real(u:o);         
    z_NUM_real_int(h,:)         =   z_NUM_real(u:o);                        
    z_NUM_Ref_int(h,:)          =   z_NUM_Ref(u:o);                         
    P_error_int(h,:)            =   P_error(u:o);                           
end
e_rms_tcp                       =   sqrt(sum(e_tcp_int.^2,2)/int_len);      %Tracking root mean square displacement error
max_z_desired                   =   max(abs(-(z_desired_int-z_desired_int(:,1))+z0_mV),[],2); 
e_rms_tcp_rel                   =   e_rms_tcp ./ max_z_desired;             %Tracking relative root mean square displacement error

ed_rms_tcp                      =   sqrt(sum(ed_tcp_int.^2,2)/int_len);     %Root mean square velocity error
max_zd_desired                  =   max(abs(zd_desired_int),[],2); 
ed_rms_tcp_rel                  =   ed_rms_tcp ./ max_zd_desired;           %Relative root mean square velocity error

e_rms_Ref                       =   sqrt(sum(e_Ref_int.^2,2)/int_len);      %Reference root mean square displacement error
max_z_Ref                       =   max(abs(z_NUM_Ref),[],2);
e_rms_Ref_rel                   =   e_rms_Ref ./ max_z_Ref;                 %Reference relative root mean square displacement error

%% 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                   Plots                                      %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
lastIter                        =   sprintf('Iteration %i', Iterations);

if enable_NPC == 1
    NPC_str                     =   ' with NPC';
    NPC_str2                    =   'NPC+';
else
    NPC_str                     =   '';
    NPC_str2                     =   '';
end
if modus_FFW == 1
    FFW_str                     =   [NPC_str2 'Feedforward control'];
elseif modus_FFW == 2
    FFW_str                     =   [NPC_str2 'VFF'];
elseif modus_FFW == 3
    if L_d ~= 0
        FFW_str                 =   [NPC_str2 'PDILC'];
    else
        FFW_str                 =   [NPC_str2 'PILC'];
    end
elseif modus_FFW == 4
    if L_d ~= 0
        FFW_str                 =   [NPC_str2 'VFF+PDILC'];
    else
        FFW_str                 =   [NPC_str2 'VFF+PILC'];
    end
end

TUMBlue                         =   [0 0.3961 0.7412];
TUMOrange                       =   [0.8902 0.4471 0.133];
TUMGreen                        =   [162 173 0]/255;
TUMBlack                        =   [0 0 0]/255;

%% Plot convergence condition (Only when ILC is active)
if modus_FFW == 3 || modus_FFW == 4
    set(groot, 'defaultAxesTickLabelInterpreter','latex'); set(groot, 'defaultLegendInterpreter','latex');

    % System to be examined in the frequency domain
    s                               =   tf('s');

    % Transfer system (here Stewart platform's leg 2 as an example)

    %Controller and Plant
    C_PI                            =   pid(K_P_v,K_I_v,0);                     %PI velocity control loop
    Actuator                        =   tf(G2_Num,G2_Den);                      
    P                               =   feedback(C_PI*Actuator,s);              
    C                               =   K_P_x;                                  %P gain of position control loop [1/s]
    %Measuring system
    G_M                             =   tf(1);

    % Dynamic System
    G_NUM                           =   1/(m_num*s^2+d_num*s+k_num);            %transfer function (TF) numerical subsysem 
    G_EXP                           =  -( m_exp*s^2 + 0*s + k_exp);             %tf experimental subsystem
    G_MECH                          =   G_NUM*G_EXP;

    % Calculation
    % ILC
    L1                              =   pid(L_p,0,L_d);                         %learning function of ILC

    %ZPF
    fc1                             =   Q_Cutoff;                               %Cutoff frequency of Zero phase filter (ZPF) [Hz]
    fs                              =   1000;                                   
    [bb1,aa1]                       =   butter(1,fc1/(fs/2));
    myQ1                            =   tf(bb1,aa1,0.001)*tf(bb1,aa1,0.001);    %Double Filter because zpf filters twice

    % RTHS + ILC 
    %Error Propagation
    S_fin                           =   (G_M*G_MECH*P-P)/(1-G_M*G_MECH*C*P+C*P);
    ILC_err_prop                    =   (1+S_fin*L1);
    ILC_err_prop_filt1              =   d2c(myQ1,'matched')*ILC_err_prop;


    W                               =   2*pi*([0.1:0.1:10,11:1:110]);
    [a1,~]                          =   bode(ILC_err_prop_filt1,W);             %16-0-inf
    [a_0dB,~]                       =   bode(tf(1),W);                          %0dB

    e_infty                         =   1/(1-ILC_err_prop);

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % Plot result
    Mag_0dB                         =   reshape(a_0dB,length(a_0dB),1);         % 0 dB
    Mag_1                           =   reshape(a1,length(a1),1);               %16-0-inf

    W                               =   1/(2*pi)*W;

    % Convergence condition
    figure
        semilogx(W,20*log10(Mag_1),'Color',TUMBlue,'LineStyle','-');
        hold on

        set(findall(gcf,'type','line'),'linewidth',1)
        semilogx(W,20*log10(Mag_0dB),'k--','HandleVisibility','off','LineWidth',1)
        xlabel('Frequency / Hz','Interpreter','Latex') 
        ylabel('Magnitude / dB','Interpreter','Latex')
        grid on
        %ylim([-20 10])
        legentText = ['$f_\textup{Q,cut} = $ ' num2str(Q_Cutoff) ' $~\mathrm{Hz}$'];
        legend(legentText, 'interpreter','latex')
        sgtitle(append('Frequency response of the error propagation'),'Interpreter','latex')

end

%% Plot Relative RMS errors vs. Iteration (Only when ILC is active)
if modus_FFW == 3 || modus_FFW == 4
    figure
        % Plot Relative RMS tracking error vs. Iteration
        subplot(1,2,1)
            semilogy(1:Iterations,e_rms_tcp_rel,'.','Color', TUMBlue,'MarkerSize',12) %can be calculated using the variables in the coordinate system of the Stewart Platform (here) or the coordinate system of the numerial simulation
            grid on 
            hold on
            box off
            ylabel('Relative RMS tracking error','Interpreter','latex')
            xlabel('Iteration j','Interpreter','latex')
        % Plot Relative RMS reference error vs. Iteration
        subplot(1,2,2)
        semilogy(1:Iterations,e_rms_Ref_rel,'.','Color', TUMBlue,'MarkerSize',12) %can be calculated using the variables in the coordinate system of the Stewart Platform or the coordinate system of the numerial simulation (here)
        grid on 
        hold on
        box off
        ylabel('Relative RMS reference error','Interpreter','latex')
        xlabel('Iteration j','Interpreter','latex')

        sgtitle(append('Relative RMS errors', NPC_str),'Interpreter','latex')
end
%% Plot Relative reference error vs. Time (Only when ILC is active)
if modus_FFW == 3 || modus_FFW == 4
    figure
        plot(0.001:0.001:4,e_Ref_int(1,1:end),'Color', TUMBlue,'LineWidth',1.25)
        grid on 
        hold on
        box off
        if Iterations>=5
            plot(0.001:0.001:4,e_Ref_int(5,1:end),'Color', TUMOrange,'LineWidth',1.25)
        end
        plot(0.001:0.001:4,e_Ref_int(Iterations,1:end),'Color', TUMBlack,'LineWidth',1.25)

        ylabel('Reference error / mm','Interpreter','latex')
        xlabel('Time / s','Interpreter','latex')
        if Iterations>=5 
            legend('Iteration 1','Iteration 5',lastIter,'Location','Northeast')
        else
            legend('Iteration 1',lastIter,'Location','Northeast')
        end
        sgtitle(append('Reference error in time-domain',NPC_str),'Interpreter','latex')
end

%% Plot Relative RMS velocity error vs. Iterations (Only when ILC is active)
if modus_FFW == 3 || modus_FFW == 4   
    figure
        semilogy(1:Iterations,ed_rms_tcp_rel,'.','Color', TUMBlue,'MarkerSize',12)
        grid on 
        hold on
        box off
        ylabel('Relative RMS velocity error','Interpreter','latex')
        xlabel('Iteration j','Interpreter','latex')
        sgtitle('Convergence of the RMS velocity error','Interpreter','latex')
end

%% Plot Error power vs. Time  (Only when ILC is active)
if modus_FFW == 3 || modus_FFW == 4
    figure
        plot(0.001:0.001:4,P_error_int(1,1:end),'Color', TUMBlue,'LineWidth',1.25)
        grid on 
        hold on
        box off
        xlim([1.25 2.75])
        plot(0.001:0.001:4,P_error_int(Iterations,1:end),'Color', TUMBlack,'LineWidth',1.25)
        ylabel('Error power / W','Interpreter','latex')
        xlabel('Time / s','Interpreter','latex')
        legend('Iteration 1',lastIter,'Location','Northeast')
        sgtitle('Comparison of the error power between the first iteration and the last iteration','Interpreter','latex')
end

%% Plot z'Num vs. Time 
figure
    plot(0.001:0.001:4,z_NUM_real_int(1,1:end),'Color', TUMBlue,'LineWidth',1.25)
    hold on
    plot(0.001:0.001:4,z_NUM_real_int(Iterations,1:end),'Color', TUMBlack,'LineWidth',1.25)
    grid on 
    box off
    xlim([1.2 2.8])
    plot(0.001:0.001:4,z_NUM_Ref_int(1,1:end), '--','Color', TUMBlack,'LineWidth',1.25)
    ylabel('$z_\textup{NUM}^\prime$ / mm','Interpreter','latex')
    xlabel('Time / s','Interpreter','latex')
    legend('Iteration 1',lastIter,'Reference','Interpreter','latex','Location','Northeast')
    sgtitle('Trajectories of QoI $z_\textup{NUM}^\prime$ during the contact phase','Interpreter','latex')

%% Plot z'Num vs. Time
figure
if modus_FFW == 3 || modus_FFW == 4
    subplot(1,3,1)
    plot(0.001:0.001:4,z_NUM_real_int(1,1:end),'Color', TUMBlue,'LineWidth',1.25)
    grid on 
    hold on
    box off
    plot(0.001:0.001:4,z_NUM_Ref_int(1,1:end), '--','Color', TUMBlack,'LineWidth',1.25)
    ylabel('$z_\textup{NUM}^\prime$ / mm','Interpreter','latex')
    xlabel('Time / s','Interpreter','latex')
    legend(['Iteration ' num2str(1)],'Reference','Interpreter','latex','Location','Northeast')
    title('Iteration 1')

        if Iterations>=5    
        subplot(1,3,2)
        plot(0.001:0.001:4,z_NUM_real_int(5,1:end),'Color', TUMOrange,'LineWidth',1.25)
        grid on 
        hold on
        box off
        plot(0.001:0.001:4,z_NUM_Ref_int(5,1:end), '--','Color', TUMBlack,'LineWidth',1.25)
        ylabel('$z_\textup{NUM}^\prime$ / mm','Interpreter','latex')
        xlabel('Time / s','Interpreter','latex')
        legend(['Iteration ' num2str(5)],'Reference','Interpreter','latex','Location','Northeast')
        title('Iteration 5')
        end
    subplot(1,3,3)
    plot(0.001:0.001:4,z_NUM_real_int(Iterations,1:end),'Color', TUMBlack,'LineWidth',1.25)
    grid on 
    hold on
    box off
    plot(0.001:0.001:4,z_NUM_Ref_int(Iterations,1:end), '--','Color', TUMBlack,'LineWidth',1.25)
    ylabel('$z_\textup{NUM}^\prime$ / mm','Interpreter','latex')
    xlabel('Time / s','Interpreter','latex')
    legend(lastIter,'Reference','Interpreter','latex','Location','Northeast')
    title(lastIter)
    sgtitle(append('Interface displacement over time',NPC_str),'Interpreter','latex')
else 
    plot(0.001:0.001:4,z_NUM_real_int(1,1:end),'Color', TUMBlue,'LineWidth',1.25)
    grid on 
    hold on
    box off
    plot(0.001:0.001:4,z_NUM_Ref_int(1,1:end), '--','Color', TUMBlack,'LineWidth',1.25)
    ylabel('$z_\textup{NUM}^\prime$ / mm','Interpreter','latex')
    xlabel('Time / s','Interpreter','latex')
    legend('','Reference','Interpreter','latex','Location','Northeast')
    sgtitle(append('Interface displacement over time',NPC_str),'Interpreter','latex')
end

%% Animate the solution
if modus_FFW == 3 || modus_FFW == 4
    enable_animation = input('Do you want to animate the solution? y/n [n]','s')
    if isempty(enable_animation)
        enable_animation = 'n';
    end
    if strcmp(enable_animation,'y') 
        Create_Animation
    end
end
