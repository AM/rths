# RTHS
This project contains implementations for Real-Time Hybrid Substructuring (RTHS) at the Chair of Applied Mechanics, TUM. The folder "RTHS_ILC_NPC" contains a virtual RTHS simulation and presents the implementation of an actuator control scheme. In this implementation, Normalized Passivity Control (NPC) and Iterative Learning Control (ILC) are combined for robust and high fidelity RTHS. 

The folder "RTHS_FACE" contains a description and the implementation of the FACE method, which stands for Fidelity Assessment based on Convergence and Extrapolation. The application example is a virtual RTHS simulation of a linera RTHS system. 

The research was funded by the Deutsche Forschungsgemeinschaft (DFG, German Research Foundation) with project number 450801414.
